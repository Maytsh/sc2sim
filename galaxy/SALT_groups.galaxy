
// Our interface. How hard could it be?
void SALT_GROUP_init();
void SALT_GROUP_calibrate(int player, bool checkDefaultLayouts);
unitgroup SALT_GROUP_get(int player, int group);
void SALT_GROUP_clear(int player, int group);
void SALT_GROUP_add(int player, int group, unit u);

// How many control group update schemes we account for. This is e.g.
// "Shift=Add, Ctrl=Set, Alt=Take" or "Shift=Set, Ctrl=Add, Alt=Take".
// I am using the latter scheme personally, so I am very motivated
// to get this right :)
const int SALT_GROUP_SCHEMES = 2;

const int SALT_GROUP_SCHEME_SET = 1;
const int SALT_GROUP_SCHEME_ADD = 2;
const int SALT_GROUP_SCHEME_TAKE = 3;
const int SALT_GROUP_SCHEME_ADD_TAKE = 4;
int[SALT_GROUP_SCHEMES][8] SALT_GROUP_schemes;
string[SALT_GROUP_SCHEMES] SALT_GROUP_scheme_name;
trigger SALT_GROUP_destructive_train_trigger;

// How much confidence we need in a control scheme before we make it
// official and pull of the triggers on the control group hotkeys. To
// be concrete, this is how often we found the control scheme in
// question to be *better* than the next best explanation. As things
// are right now, this roughly requires the player to assign to a 
// non-empty control group this many times (in a way that actually
// make a difference)
const int SALT_GROUP_MINIMUM_CONFIDENCE = 3; // fairly conservative choice

struct SALT_GROUP_player_state
{
	int player;
	// bank saltBank; // Disabled until Blizzard fixes extension mods
	
	trigger sync_trigger;
	trigger dummy_timer;
	trigger select_trigger;
	trigger deselect_trigger;
	trigger key_trigger;
	trigger select_finish_trigger;
	int last_key;
	int last_control_group;
	fixed last_key_time;
	unitgroup current_selection;
	bool got_select;
	unitgroup[SALT_lAST_CONTROL_GROUP][SALT_GROUP_SCHEMES] control_groups;
	int[SALT_GROUP_SCHEMES] control_group_confidence;

	unit[SALT_lAST_CONTROL_GROUP] control_group_dummys;
	int[SALT_lAST_CONTROL_GROUP] control_group_keys;
};

SALT_GROUP_player_state[SALT_lAST_FORCE+1] SALT_GROUP_state;

void SALT_GROUP_init() {
	int i; string entry;
	
	// Set control group schemes. Right now we have only two, but it would be easy
	// to add more. The problem with adding more is that it becomes harder and harder
	// to tell them apart - with how rarely Take/Take+Add get used, for many players
	// we would never be able to identify them.
	SALT_GROUP_schemes[0][1<<c_keyShift] = SALT_GROUP_SCHEME_ADD;
	SALT_GROUP_schemes[0][1<<c_keyControl] = SALT_GROUP_SCHEME_SET;
	SALT_GROUP_schemes[0][1<<c_keyAlt] = SALT_GROUP_SCHEME_TAKE;
	SALT_GROUP_schemes[0][(1<<c_keyShift)+(1<<c_keyAlt)] = SALT_GROUP_SCHEME_ADD_TAKE;
	SALT_GROUP_scheme_name[0] = "default";
	
	SALT_GROUP_schemes[1][1<<c_keyShift] = SALT_GROUP_SCHEME_SET;
	SALT_GROUP_schemes[1][1<<c_keyControl] = SALT_GROUP_SCHEME_ADD;
	SALT_GROUP_schemes[1][1<<c_keyAlt] = SALT_GROUP_SCHEME_TAKE;
	SALT_GROUP_schemes[1][(1<<c_keyControl)+(1<<c_keyAlt)] = SALT_GROUP_SCHEME_ADD_TAKE;
	SALT_GROUP_scheme_name[0] = "flipped";
	
	// Add trigger that waits for train events that kill the training unit. SC2
	// silently replaces the training unit with the created unit for the purpose of
	// control groups. This might seem like a pretty obscure use case - in fact,
	// there's only three abilities as of LotV - but this set includes such rarely
	// used abilities such as LarvaTrain and MorphZerglingToBaneling, so it is
	// actually pretty damn apparent if we get this wrong.
	//
	// Fun fact: Morphs do *not* have this problem, because the units retain their
	// identity with those.
	SALT_GROUP_destructive_train_trigger = TriggerCreate("SALT_GROUP_destructive_train");
	for (i = 1; i < CatalogEntryCount(c_gameCatalogAbil); i+=1) {
		entry = CatalogEntryGet(c_gameCatalogAbil, i);
		if (!CatalogEntryIsValid(c_gameCatalogAbil, entry)) { continue; } 
		if (CatalogEntryClass(c_gameCatalogAbil, entry) != c_classIdCAbilTrain) { continue; }
		if (!CatalogFieldValueGetAsInt(c_gameCatalogAbil, entry, "Flags[KillOnFinish]", 0)) { continue; }
		TriggerAddEventUnitCreated(SALT_GROUP_destructive_train_trigger, null, entry, null);
	}	
}

bool SALT_GROUP_destructive_train(bool testConds, bool runActions) {
	structref<SALT_GROUP_player_state> th; int i; int j;
	if (!runActions) { return true; }

	// Find player
	th = SALT_GROUP_state[SALT_player_to_force(EventPlayer())];
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+= 1) {
		for (j = 0; j < SALT_GROUP_SCHEMES; j+= 1) {
			
			// Unit in control group? Add created unit. The old unit will vanish automatically.
			if (UnitGroupHasUnit(th.control_groups[i][j], EventUnit())) {
				UnitGroupAdd(th.control_groups[i][j], EventUnitCreatedUnit());
			}
		}
	}
	
	return true;
}

void SALT_GROUP_calibrate_stop(structref<SALT_GROUP_player_state> th);
void SALT_GROUP_calibrate_full(structref<SALT_GROUP_player_state> th);
void SALT_GROUP_calibrate_scheme(structref<SALT_GROUP_player_state> th);
void SALT_GROUP_calibrate_finish(structref<SALT_GROUP_player_state> th);

void SALT_GROUP_calibrate(int player, bool checkDefaultLayouts) {
	int i; int j; unit u; string profile;
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(player)];
	
	th.player = player;
	
	// Skip for non-human players
	if (PlayerType(player) != c_playerTypeUser) { return; }
	
	// Set up select trigger. Will stay on for the entire time,
	// as it seems more reliable than querying UnitGroupSelected().
	if (th.select_trigger == null) {
		th.select_trigger = TriggerCreate("SALT_GROUP_select");
		th.deselect_trigger = TriggerCreate("SALT_GROUP_deselect");
	}
	TriggerAddEventUnitSelected(th.select_trigger, null, player, true);
	TriggerAddEventUnitSelected(th.deselect_trigger, null, player, false);
	
	// Initialize control groups, reset confidence
	th.current_selection = UnitGroupEmpty();
	for (j = 0; j < SALT_GROUP_SCHEMES; j+=1) {
		for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
			th.control_groups[i][j] = UnitGroupEmpty();
		}
		th.control_group_confidence[j] = 0;
	}

	// Here's where the walkaround for loading banks from extension mods would go.
	// IF I HAD ONE.
	//BankLoad("MacroHero", th.player);
	//th.saltBank = BankLastCreated();

	if (checkDefaultLayouts) {
		// Check whether it is one of the default hotkey layouts
		profile = PlayerGetHotkeyProfile(th.player);
		if (profile == "0_Default" || profile == "2_GridLeftSide" || profile == "4_Classic") {
			th.control_group_keys[0] = c_key0;
			th.control_group_keys[1] = c_key1;
			th.control_group_keys[2] = c_key2;
			th.control_group_keys[3] = c_key3;
			th.control_group_keys[4] = c_key4;
			th.control_group_keys[5] = c_key5;
			th.control_group_keys[6] = c_key6;
			th.control_group_keys[7] = c_key7;
			th.control_group_keys[8] = c_key8;
			th.control_group_keys[9] = c_key9;
			th.control_group_confidence[0] = SALT_GROUP_MINIMUM_CONFIDENCE;
			th.control_group_confidence[1] = 0;
			SALT_GROUP_calibrate_finish(th);
			return;
		}
		if (profile == "1_NameRightSide" || profile == "3_GridRightSide") {
			th.control_group_keys[0] = c_keyEquals;
			th.control_group_keys[1] = c_key3;
			th.control_group_keys[2] = c_key4;
			th.control_group_keys[3] = c_key5;
			th.control_group_keys[4] = c_key6;
			th.control_group_keys[5] = c_key7;
			th.control_group_keys[6] = c_key8;
			th.control_group_keys[7] = c_key9;
			th.control_group_keys[8] = c_key0;
			th.control_group_keys[9] = c_keyMinus;
			th.control_group_confidence[0] = SALT_GROUP_MINIMUM_CONFIDENCE;
			th.control_group_confidence[1] = 0;
			SALT_GROUP_calibrate_finish(th);
			return;
		}
	}
	
	// Fall back to full calibration...
	SALT_GROUP_calibrate_full(th);

}

void SALT_GROUP_calibrate_stop(structref<SALT_GROUP_player_state> th) {
	int i;
	
	// Remove the triggers
	if (th.dummy_timer != null) {
		TriggerDestroy(th.dummy_timer);
		th.dummy_timer = null;
	}
	if (th.key_trigger != null) {
		TriggerDestroy(th.key_trigger);
		th.key_trigger = null;
	}

	// Remove any control group probes
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
		if (th.control_group_dummys[i] != null) {
			UnitRemove(th.control_group_dummys[i]);
			th.control_group_dummys[i] = null;
		}
	}

}

void SALT_GROUP_calibrate_full(structref<SALT_GROUP_player_state> th) {
	int i; int j; string msg;

	// Show message. Isn't it ironic that we can display the key in a message, but
	// can't reason about it in any other way?
	msg = "Unknown layout '" + PlayerGetHotkeyProfile(th.player) + "'. Please press";
	for (i = 1; i < 9; i += 1) {
		msg += "<k val=\"ControlGroupRecall" + IntToString(i) + "\" count=\"1\"/>";
	}
	msg += "<k val=\"ControlGroupRecall0\" count=\"1\"/>!"; // Zero is the last control group by default
	if (GameIsOnline()) {
		// Works less well with lag involved
		msg += " Not too fast though."
	}
	Hero_personal_message(th.player, msg);
	
	// Set up key trigger. 
	if (th.key_trigger == null) {
		th.key_trigger = TriggerCreate("SALT_GROUP_key");
	}
	TriggerAddEventKeyPressed(th.key_trigger, th.player,
		c_keyNone, true,
		c_keyModifierStateIgnore, c_keyModifierStateIgnore, c_keyModifierStateIgnore);

	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
		
		// Create dummy, if required
		if (th.control_group_dummys[i] == null) {
			
			// Create unit with trigger and custom value so we can
			// identify it later.
			th.control_group_dummys[i] = UnitGroupUnit(UnitCreate(1, "ControlGroupProbe", c_unitCreateIgnorePlacement, th.player, CameraGetTarget(th.player), 0), 1);
			UnitSetCustomValue(th.control_group_dummys[i], 0, IntToFixed(i));

		}
		
		// Reset key
		th.control_group_keys[i] = -2;
	}
	
	th.last_control_group = -1;
	
	// Start timer
	th.dummy_timer = TriggerCreate("SALT_GROUP_timer");
	TriggerAddEventTimePeriodic(th.dummy_timer, 1, c_timeGame);
}

void SALT_GROUP_calibrate_scheme(structref<SALT_GROUP_player_state> th) {
	int i;

	// We now use the key trigger to specifically watch the hotkeys.
	// We still register it to look for all modifier keys, as we need to figure
	// out the control group update scheme - and we can only do this by comparing
	// selection events with when control group hotkeys get pressed.
	th.key_trigger = TriggerCreate("SALT_GROUP_key");
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i += 1) {
		TriggerAddEventKeyPressed(th.key_trigger, th.player,
			th.control_group_keys[i], true,
			c_keyModifierStateIgnore, c_keyModifierStateIgnore, c_keyModifierStateIgnore);
	}

}


void SALT_GROUP_calibrate_finish(structref<SALT_GROUP_player_state> th) {
	int i;
	
	// Create our most specific trigger version: Only control group keys with
	// some sort of modifier key active. This should be fairly fairly non-intrusive.
	th.key_trigger = TriggerCreate("SALT_GROUP_key");
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i += 1) {
		TriggerAddEventKeyPressed(th.key_trigger, th.player,
			th.control_group_keys[i], true,
			c_keyModifierStateRequire, c_keyModifierStateIgnore, c_keyModifierStateIgnore);
		TriggerAddEventKeyPressed(th.key_trigger, th.player,
			th.control_group_keys[i], true,
			c_keyModifierStateIgnore, c_keyModifierStateRequire, c_keyModifierStateIgnore);
		TriggerAddEventKeyPressed(th.key_trigger, th.player,
			th.control_group_keys[i], true,
			c_keyModifierStateIgnore, c_keyModifierStateIgnore, c_keyModifierStateRequire);
	}
	
}

bool SALT_GROUP_timer(bool testConds, bool runActions) {
	unitgroup group; int i; int j;
	structref<SALT_GROUP_player_state> th;
	if (!runActions) { return true; }

	// Find player state corresponding to this timer trigger
	for (i = 0; i <= SALT_lAST_FORCE; i+=1) {
		if (SALT_GROUP_state[i].dummy_timer == TriggerGetCurrent()) {
			th = SALT_GROUP_state[i];
			break;
		}
	}
	// Could probably happen if the timer gets destroyed, but the trigger
	// call is still in the queue?
	if (i > SALT_lAST_FORCE) { return true; }
	
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {

		// Not set? Skip.
		if (th.control_group_dummys[i] == null) { continue; }

		// Make sure all the *other* control groups don't contain it.
		//
		// Disabled for now, as it seems fairly stable even without this.
		// After all, we will remove the probe right after it got selected,
		// so it's basically impossible to multi-assign. And even if the
		// player manages to do it, we'll probably catch the inconsistency.
		//group = UnitGroupEmpty();
		//for (j = 0; j <= 9; j+= 1) { if (i != j) {
		//	UnitGroupAdd(group, th.control_group_dummys[j]);
		//}}
		//UnitControlGroupRemoveUnits(th.player, i, group);
		
		// Add to control group
		UnitControlGroupAddUnit(th.player, i, th.control_group_dummys[i]);
		
		// For some reason, the above can cause selection trigger calls. It
		// definetely didn't happen when I first tested this, so until I have
		// a better theory I'm going with the tinfoil "something in SC2 changed"
		// hypothesis.

		// Whatever it is, it can actually cause *significant* lag. On the plus
		// side, it seems that the callbacks arrive in an order that does *not*
		// trigger erronous hotkey detection. So, yay?
	}
	return true;
}

bool SALT_GROUP_unit_group_equal(unitgroup group1, unitgroup group2) {
	int count = UnitGroupCount(group1, c_unitCountAlive); int i;
	
	// Count mismatch? Easy.
	if (count != UnitGroupCount(group2, c_unitCountAlive)) { return false; }
	
	// Check individual units
	for (i = 1; i <= count; i+=1) {
		if (!UnitGroupHasUnit(group2, UnitGroupUnit(group1, i))) {
			return false;
		}
	}
	return true;
}

void SALT_GROUP_selection(structref<SALT_GROUP_player_state> th, unitgroup units, int cgroup, bool possibly_empty)
{
	int count; int i;
	int best_scheme = -1; int best_confidence = 0; int snd_best_confidence = 0;

	// Check consistency with schemes
	for (i = 0; i < SALT_GROUP_SCHEMES; i += 1) {
		
		// Theoretically we could gain confidence even for empty control groups,
		// but it is probably fairly silly to extract information from... nothing?
		// Puns aside, I can't think of a situation where this would actually
		// allow us to decide for or against a control scheme.
		if (SALT_GROUP_unit_group_equal(units, th.control_groups[cgroup][i])) {
			th.control_group_confidence[i] += 1;
		}

		// Update best (& second best) scheme
		if (th.control_group_confidence[i] > best_confidence) {
			snd_best_confidence = best_confidence;
			best_confidence = th.control_group_confidence[i];
			best_scheme = i;
		} else if (th.control_group_confidence[i] > snd_best_confidence) {
			snd_best_confidence = th.control_group_confidence[i];
		}
		
		// Take over unit group. This is not guaranteed to be correct, but it is
		// more often than not, so better use this to gain some robustness.
		if (!possibly_empty || !UnitGroupCount(th.control_groups[cgroup][i], c_unitCountAlive) == 0) {
			UnitGroupClear(th.control_groups[cgroup][i]);
			UnitGroupAddUnitGroup(th.control_groups[cgroup][i], units);
		}
	}
	
	// Have we reached enough confidence on this control group layout? Disable checks then.
	if (best_confidence >= snd_best_confidence + SALT_GROUP_MINIMUM_CONFIDENCE) {
		
		// Alternate scheme? Congratulate the player on good taste.
		if (best_scheme == 1) {
			Hero_personal_message(th.player, "Alternate group assignment scheme detected. Good choice!");
		}
		
		SALT_GROUP_calibrate_stop(th);
		SALT_GROUP_calibrate_finish(th);
	}

}
void SALT_GROUP_update_selection(structref<SALT_GROUP_player_state> th, unitgroup units, int cgroup, int scheme, int update)
{
	int i; unitgroup group = th.control_groups[cgroup][scheme];
	string msg;

	// Do the update
	if (update == SALT_GROUP_SCHEME_SET) {
		msg = "Set " + IntToString(cgroup) + ": ";
		UnitGroupClear(group);
		UnitGroupAddUnitGroup(group, units);
	}
	if (update == SALT_GROUP_SCHEME_ADD) {
		msg = "Add " + IntToString(cgroup) + ": ";
		UnitGroupAddUnitGroup(group, units);
	}
	if (update == SALT_GROUP_SCHEME_TAKE) {
		UnitGroupClear(group);
		for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
			UnitGroupRemoveUnitGroup(th.control_groups[i][scheme], units);
		}
		UnitGroupAddUnitGroup(group, units);
	}
	if (update == SALT_GROUP_SCHEME_ADD_TAKE) {
		for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
			UnitGroupRemoveUnitGroup(th.control_groups[i][scheme], units);
		}
		UnitGroupAddUnitGroup(group, units);
	}
	
	// For debugging purposes
	//for (i = 1; i <= UnitGroupCount(group, c_unitCountAlive); i+=1) {
	//	msg += " " + UnitGetType(UnitGroupUnit(group, i));
	//}
	//StringDump(msg);
}

bool SALT_GROUP_key(bool testConds, bool runActions) {
	int i; int comb; structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(EventPlayer())];
	if (!runActions) { return true; }
	
	// Got a selection key before? Then callback
	if (th.last_control_group != -1) {
		SALT_GROUP_selection(th, th.current_selection, th.last_control_group, !th.got_select);
	}
	
	// Selection key?
	if (!EventKeyShift() && !EventKeyControl() && !EventKeyAlt()) {
		
		// Start a (very) short timer to catch the units
		// getting selected. We need this because the selection trigger will
		// get called for every unit, and only once the next frame has come around
		// we'll know which one was the last one.
		if (th.select_finish_trigger == null) {
			th.select_finish_trigger = TriggerCreate("SALT_GROUP_finish_select");
		}
		if (th.last_key_time != GameGetMissionTime()) {
			TriggerAddEventTimeElapsed(th.select_finish_trigger, 1.0/32.0, c_timeGame);
		}
		
		// Save key and time. Set variable so we can track whether
		// a selection event actually occured (so we can detect empty
		// control groups).
		th.last_key = EventKeyPressed();
		th.last_key_time = GameGetMissionTime();
		th.got_select = false;

	}
	
	// Check whether this is a known control group
	th.last_control_group = -1;
	for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
		if (th.control_group_keys[i] == EventKeyPressed()) {
			th.last_control_group = i;
		}
	}
	
	if (th.last_control_group != -1 && (EventKeyShift() || EventKeyControl() || EventKeyAlt())) {
		
		// Determine combination
		comb = 0;
		if (EventKeyShift()) { comb += 1 << c_keyShift; }
		if (EventKeyControl()) { comb += 1 << c_keyControl; }
		if (EventKeyAlt()) { comb += 1 << c_keyAlt; }
		
		// Now loop through schemes and update accordingly
		for (i = 0; i < SALT_GROUP_SCHEMES; i+=1) {
			if (SALT_GROUP_schemes[i][comb] != 0) {
				SALT_GROUP_update_selection(th, th.current_selection, th.last_control_group, i, SALT_GROUP_schemes[i][comb]);
			}
		}
		
		th.last_control_group = -1;
	}
	
	return true;
}

bool SALT_GROUP_select(bool testConds, bool runActions) {
	int cgroup; unit u = EventUnit(); int i; bool last;
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(EventPlayer())];
	if (!runActions) { return true; }
	
	// Got a selection event - if we had a key right before, this means
	// that the control group in question was not empty.
	th.got_select = true;
	
	if (UnitGetType(u) != "ControlGroupProbe") { 
		// Add to selection
		UnitGroupAdd(th.current_selection, u);
		return true;
	}

	// Deselect immediately. This is asynchronous, the player will therefore see
	// the unit for a short time. Small nuisance to the player. Theoretically, they 
	// could actually get back at us by using that fraction of a second to assign
	// the unit to a different control group. So we are going to do some extra
	// sanity checks later to prevent confusion.
	UnitSelect(u, th.player, false);
	
	// Same frame as key got pressed? It seems both key as well as select triggers
	// go through the network queue, and therefore our view is actually consistent
	// even if it lags behind the player's view for a split second.
	if (th.last_key_time != GameGetMissionTime()) {
		return true;
	}
	
	// Get control group in question
	cgroup = FixedToInt(UnitGetCustomValue(u, 0));
	if (th.control_group_keys[cgroup] != th.last_key) {

		// Same key as another control group? This should be impossible unless the
		// player changed control group layout mid-calibration or had fingers
		// nimble enough to mess up our control group set-up. In either case,
		// start over.
		last = true;
		for (i = 0; i < SALT_lAST_CONTROL_GROUP; i+=1) {
			if (th.control_group_keys[i] == th.last_key) {
				Hero_personal_message(th.player, "Uh-oh. That was a bit too fast? Sorry, lag confuses me. Let's start over...");
				SALT_GROUP_calibrate(th.player, false);
				return true;
			}
			// Check whether this is the last control group getting assigned,
			// while we're looping through them.
			if (i != cgroup && th.control_group_dummys[i] != null) {
				last = false;
			}
		}
		
		// Remove tester
		// Hero_personal_message(th.player, "Key " + IntToString(SALT_GROUP_last_key) + " is control group " + IntToString(cgroup));
		UnitRemove(u);
		th.control_group_dummys[cgroup] = null;
		
		// Last control group?
		if (last) {
			
			// Be polite. This is not actually the end of the calibration process, but the rest we can
			// manage in the background.
			Hero_personal_message(th.player, "... got it, thanks!");
			
			// Stop the timer and remove the catch-all key timer
			SALT_GROUP_calibrate_stop(th);
			SALT_GROUP_calibrate_scheme(th);
			
		}
	}

	th.last_control_group = cgroup;
	th.control_group_keys[cgroup] = th.last_key;
	return true;
}

bool SALT_GROUP_deselect(bool testConds, bool runActions) {
	unit u = EventUnit();
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(EventPlayer())];
	if (!runActions) { return true; }
	
	// Add to selection
	UnitGroupRemove(th.current_selection, u);
	th.got_select = true;
	
	return true;
}

bool SALT_GROUP_finish_select(bool testConds, bool runActions) {
	int i; structref<SALT_GROUP_player_state> th;
	if (!runActions) { return true; }

	// Find player state corresponding to this timer trigger
	for (i = 0; i <= SALT_lAST_FORCE; i+=1) {
		if (SALT_GROUP_state[i].select_finish_trigger == TriggerGetCurrent()) {
			th = SALT_GROUP_state[i];
			break;
		}
	}
	if (i > SALT_lAST_FORCE) { return true; }
	
	// Do selection callback
	if (th.last_control_group >= 0) {
		SALT_GROUP_selection(th, th.current_selection, th.last_control_group, !th.got_select);
	}
	
	// Reset last control group
	th.last_control_group = -1;
	return true;
}

unitgroup SALT_GROUP_get(int player, int group)
{
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(player)];

	// Find scheme with most confidence, with bias towards early schemes
	// (so until we have evidence, we assume the "default" scheme)
	int best_scheme = 0; int best_confidence = th.control_group_confidence[0]; int i;
	for (i = 1; i < SALT_GROUP_SCHEMES; i += 1) {
		if (th.control_group_confidence[i] > best_confidence) {
			best_scheme = i; best_confidence = th.control_group_confidence[i];
		}
	}
	
	// Return appropriate control group
	if (th.control_groups[group][best_scheme] == null) {
		return UnitGroupEmpty();
	} else {
		return UnitGroupCopy(th.control_groups[group][best_scheme]);
	}
}

void SALT_GROUP_clear(int player, int group)
{
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(player)]; int i;
	
	// Clear for all schemes & game
	for (i = 0; i < SALT_GROUP_SCHEMES; i += 1) {
		th.control_groups[group][i] = UnitGroupEmpty();
	}
	UnitControlGroupClear(player, group);
	
	// Possibly re-add control group probe. Not sure whether this can happen
	if (th.control_group_dummys[group] != null) {
		UnitControlGroupAddUnit(player, group, th.control_group_dummys[group]);
	}
}

void SALT_GROUP_add(int player, int group, unit u)
{
	structref<SALT_GROUP_player_state> th = SALT_GROUP_state[SALT_player_to_force(player)]; int i;
	
	// Add for all schemes & game
	for (i = 0; i < SALT_GROUP_SCHEMES; i += 1) {
		UnitGroupAdd(th.control_groups[group][i], u);
	}
	UnitControlGroupAddUnit(player, group, u);
}
