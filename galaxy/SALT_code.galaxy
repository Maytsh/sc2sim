

// Decoder for SALT format. References:
// * https://www.reddit.com/r/starcraft/comments/25losh/build_order_tool_salt_update/chjg05r/
// * The SALT mod itself (duh)

// Characters the SC2 engine allows us to work with
const string SALT_code = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
const int SALT_code_count = 95;

// Mapping of encodings to unit names
const int SALT_code_type_count = 4;
string[SALT_code_type_count][SALT_code_count] SALT_code_unit_names;

void SALT_code_init()
{
	
	// Structures (Type 0)
	SALT_code_unit_names[0][0] = "Armory";
	SALT_code_unit_names[0][1] = "Barracks";
	SALT_code_unit_names[0][2] = "Bunker";
	SALT_code_unit_names[0][3] = "Command Center";
	SALT_code_unit_names[0][4] = "Engineering Bay";
	SALT_code_unit_names[0][5] = "Factory";
	SALT_code_unit_names[0][6] = "Fusion Core";
	SALT_code_unit_names[0][7] = "Ghost Academy";
	SALT_code_unit_names[0][8] = "Missile Turret";
	SALT_code_unit_names[0][9] = "Reactor (Barracks)";
	SALT_code_unit_names[0][10] = "Reactor (Factory)";
	SALT_code_unit_names[0][11] = "Reactor (Starport)";
	SALT_code_unit_names[0][12] = "Refinery";
	SALT_code_unit_names[0][13] = "Sensor Tower";
	SALT_code_unit_names[0][14] = "Starport";
	SALT_code_unit_names[0][15] = "Supply Depot";
	SALT_code_unit_names[0][16] = "Tech Lab (Barracks)";
	SALT_code_unit_names[0][17] = "Tech Lab (Factory)";
	SALT_code_unit_names[0][18] = "Tech Lab (Starport)";
	SALT_code_unit_names[0][19] = "Assimilator";
	SALT_code_unit_names[0][20] = "Cybernetics Core";
	SALT_code_unit_names[0][21] = "Dark Shrine";
	SALT_code_unit_names[0][22] = "Fleet Beacon";
	SALT_code_unit_names[0][23] = "Forge";
	SALT_code_unit_names[0][24] = "Gateway";
	SALT_code_unit_names[0][25] = "Nexus";
	SALT_code_unit_names[0][26] = "Photon Canon";
	SALT_code_unit_names[0][27] = "Pylon";
	SALT_code_unit_names[0][28] = "Robotics Bay";
	SALT_code_unit_names[0][29] = "Robotics Facility";
	SALT_code_unit_names[0][30] = "Stargate";
	SALT_code_unit_names[0][31] = "Templar Archives";
	SALT_code_unit_names[0][32] = "Twilight Council";
	SALT_code_unit_names[0][33] = "Baneling Nest";
	SALT_code_unit_names[0][46] = "Creep Tumor";
	SALT_code_unit_names[0][34] = "Evolution Chamber";
	SALT_code_unit_names[0][35] = "Extractor";
	SALT_code_unit_names[0][36] = "Hatchery";
	SALT_code_unit_names[0][37] = "Hydralisk Den";
	SALT_code_unit_names[0][38] = "Infestation Pit";
	SALT_code_unit_names[0][39] = "Nydus Network";
	SALT_code_unit_names[0][40] = "Roach Warren";
	SALT_code_unit_names[0][41] = "Spawning Pool";
	SALT_code_unit_names[0][42] = "Spine Crawler";
	SALT_code_unit_names[0][43] = "Spire";
	SALT_code_unit_names[0][44] = "Spore Crawler";
	SALT_code_unit_names[0][45] = "Ultralisk Cavern";
	
	// Units (Type 1)
	SALT_code_unit_names[1][0] = "Banshee";
	SALT_code_unit_names[1][40] = "Battle Hellion";
	SALT_code_unit_names[1][1] = "Battlecruiser";
	SALT_code_unit_names[1][02] = "Ghost";
	SALT_code_unit_names[1][3] = "Hellion";
	SALT_code_unit_names[1][4] = "Marauder";
	SALT_code_unit_names[1][5] = "Marine";
	SALT_code_unit_names[1][6] = "Medivac";
	SALT_code_unit_names[1][7] = "Raven";
	SALT_code_unit_names[1][8] = "Reaper";
	SALT_code_unit_names[1][9] = "SCV";
	SALT_code_unit_names[1][10] = "Siege Tank";
	SALT_code_unit_names[1][11] = "Thor";
	SALT_code_unit_names[1][41] = "Warhound";
	SALT_code_unit_names[1][42] = "Widow Mine";
	SALT_code_unit_names[1][12] = "Viking";
	SALT_code_unit_names[1][13] = "Archon";
	SALT_code_unit_names[1][14] = "Carrier";
	SALT_code_unit_names[1][15] = "Colossus";
	SALT_code_unit_names[1][16] = "Dark Templar";
	SALT_code_unit_names[1][17] = "High Templar";
	SALT_code_unit_names[1][18] = "Immortal";
	SALT_code_unit_names[1][19] = "Mothership";
	SALT_code_unit_names[1][43] = "Mothership Core";
	SALT_code_unit_names[1][20] = "Observer";
	SALT_code_unit_names[1][44] = "Oracle";
	SALT_code_unit_names[1][21] = "Phoenix";
	SALT_code_unit_names[1][22] = "Probe";
	SALT_code_unit_names[1][23] = "Sentry";
	SALT_code_unit_names[1][24] = "Stalker";
	SALT_code_unit_names[1][45] = "Tempest";
	SALT_code_unit_names[1][25] = "Void Ray";
	SALT_code_unit_names[1][39] = "Warp Prism";
	SALT_code_unit_names[1][26] = "Zealot";
	SALT_code_unit_names[1][27] = "Corruptor";
	SALT_code_unit_names[1][28] = "Drone";
	SALT_code_unit_names[1][29] = "Hydralisk";
	SALT_code_unit_names[1][38] = "Infestor";
	SALT_code_unit_names[1][30] = "Mutalisk";
	SALT_code_unit_names[1][31] = "Overlord";
	SALT_code_unit_names[1][32] = "Queen";
	SALT_code_unit_names[1][33] = "Roach";
	SALT_code_unit_names[1][46] = "Swarm Host";
	SALT_code_unit_names[1][34] = "Ultralisk";
	SALT_code_unit_names[1][47] = "Viper";
	SALT_code_unit_names[1][35] = "Zergling";

	// Morphs (Type 2)
	SALT_code_unit_names[2][0] = "Orbital Command";
	SALT_code_unit_names[2][1] = "Planetary Fortress";
	SALT_code_unit_names[2][2] = "Warp Gate";
	SALT_code_unit_names[2][3] = "Lair";
	SALT_code_unit_names[2][4] = "Hive";
	SALT_code_unit_names[2][5] = "Greater Spire";
	SALT_code_unit_names[2][6] = "Brood Lord";
	SALT_code_unit_names[2][7] = "Baneling";
	SALT_code_unit_names[2][8] = "Overseer";
	SALT_code_unit_names[2][9] = "Ravager";
	SALT_code_unit_names[2][10] = "Lurker";
	SALT_code_unit_names[2][12] = "Lurker Den";

	// Upgrades (Type 3)
	SALT_code_unit_names[3][0] = "Terran Building Armor";
	SALT_code_unit_names[3][1] = "Terran Infantry Armor";
	SALT_code_unit_names[3][2] = "Terran Infantry Weapons";
	SALT_code_unit_names[3][3] = "Terran Ship Plating";
	SALT_code_unit_names[3][4] = "Terran Ship Weapons";
	SALT_code_unit_names[3][5] = "Terran Vehicle Plating";
	SALT_code_unit_names[3][6] = "Terran Vehicle Weapons";
	SALT_code_unit_names[3][7] = "250mm Strike Cannons";
	SALT_code_unit_names[3][8] = "Cloaking";
	SALT_code_unit_names[3][9] = "Cloaking";
	SALT_code_unit_names[3][10] = "Pre-igniter";
	SALT_code_unit_names[3][11] = "Stimpack";
	SALT_code_unit_names[3][12] = "Seeker Missiles";
	SALT_code_unit_names[3][13] = "Siege Tech";
	SALT_code_unit_names[3][46] = "Moebius Reactor";
	SALT_code_unit_names[3][14] = "Neosteel Frame";
	SALT_code_unit_names[3][15] = "Concussive Shells";
	SALT_code_unit_names[3][16] = "Combat Shields";
	SALT_code_unit_names[3][17] = "Reaper Speed";
	SALT_code_unit_names[3][51] = "Behemoth Reactor";
	SALT_code_unit_names[3][52] = "Weapon Refit";
	SALT_code_unit_names[3][53] = "Hi-Sec Auto Tracking";
	SALT_code_unit_names[3][54] = "Caduceus Reactor";
	SALT_code_unit_names[3][55] = "Corvid Reactor";
	SALT_code_unit_names[3][56] = "Durable Materials";
	SALT_code_unit_names[3][57] = "Transformation servos";
	SALT_code_unit_names[3][66] = "Drilling claws";
	SALT_code_unit_names[3][18] = "Protoss Ground Armor";
	SALT_code_unit_names[3][19] = "Protoss Ground Weapons";
	SALT_code_unit_names[3][20] = "Protoss Air Armor";
	SALT_code_unit_names[3][21] = "Protoss Air Weapons";
	SALT_code_unit_names[3][22] = "Protoss Shields";
	SALT_code_unit_names[3][23] = "Hallucination";
	SALT_code_unit_names[3][24] = "Psi Storm";
	SALT_code_unit_names[3][25] = "Blink";
	SALT_code_unit_names[3][26] = "Warp Gate Tech";
	SALT_code_unit_names[3][27] = "Charge";
	SALT_code_unit_names[3][47] = "Extended Thermal Lance";
	SALT_code_unit_names[3][48] = "Khaydarin Amulet";
	SALT_code_unit_names[3][58] = "Graviton Catapult";
	SALT_code_unit_names[3][59] = "Gravatic Boosters";
	SALT_code_unit_names[3][60] = "Gravatic Drive";
	SALT_code_unit_names[3][61] = "Bosonic Core";
	SALT_code_unit_names[3][62] = "Gravity Sling";
	SALT_code_unit_names[3][67] = "Anion Pulse-Crystals";
	SALT_code_unit_names[3][28] = "Zerg Ground Carapace";
	SALT_code_unit_names[3][29] = "Zerg Melee Weapons";
	SALT_code_unit_names[3][30] = "Zerg Flyer Carapace";
	SALT_code_unit_names[3][31] = "Zerg Flyer Weapons";
	SALT_code_unit_names[3][32] = "Zerg Missile Weapons";
	SALT_code_unit_names[3][33] = "Grooved Spines";
	SALT_code_unit_names[3][34] = "Pneumatized Carapace";
	SALT_code_unit_names[3][35] = "Ventral Sacs";
	SALT_code_unit_names[3][36] = "Glial Reconstitution";
	SALT_code_unit_names[3][37] = "Organic Carapace";
	SALT_code_unit_names[3][38] = "Tunneling Claws";
	SALT_code_unit_names[3][39] = "Anabolic Synthesis";
	SALT_code_unit_names[3][40] = "Chitinous Plating";
	SALT_code_unit_names[3][41] = "Adrenal Glands";
	SALT_code_unit_names[3][42] = "Metabolic Boost";
	SALT_code_unit_names[3][43] = "Obverse Incubation";
	SALT_code_unit_names[3][44] = "Burrow";
	SALT_code_unit_names[3][45] = "Centrifugal Hooks";
	SALT_code_unit_names[3][49] = "Neural Parasite";
	SALT_code_unit_names[3][50] = "Pathogen Gland";
	SALT_code_unit_names[3][63] = "Evolve Burrow Charge";
	SALT_code_unit_names[3][64] = "Evolve Enduring Locusts";
	SALT_code_unit_names[3][65] = "Muscular Augments";
}

int SALT_code_decode(string ch)
{
	// Simply given by position in encode string constant
	return StringFind(SALT_code, StringSub(ch, 1, 1), c_stringCase) - 1;
}

string SALT_code_encode(int v)
{
	// See above
	return StringSub(SALT_code, v+1, v+1);
}

string SALT_decode_title(string title)
{
	string out;
	int pos;

	// Got an author field? Theoretically this should be given by the
	// SALT encoding version, but people don't seem to feel like playing
	// along with that... Let's try to be as robust as we can.
	pos = StringFind(title, "|", c_stringCase);
	if (pos == c_stringNotFound) {
		return "Name:" + title;
	}
	out = "Name:" + StringSub(title, 1, pos - 1);
	title = StringSub(title, pos + 1, StringLength(title));
	
	// Also have a description field? We will put it into "Credit". Close enough.
	pos = StringFind(title, "|", c_stringCase);
	if (pos == c_stringNotFound) {
		return out + " | Author:" + title;
	}	
	out = out + " | Author:" + StringSub(title, 1, pos - 1);
	title = StringSub(title, pos + 1, StringLength(title));

	// Description field terminated?
	pos = StringFind(title, "|", c_stringCase);
	if (pos == c_stringNotFound) {
		return out + " | Credit:" + title;
	}
	// Ignore the rest
	return out + " | Credit:" + StringSub(title, 1, pos - 1);
}

string SALT_decode_build(string encoding)
{
	string out;
	int pos; int pos_off;
	
	int supply; int minutes; int seconds;
	int percentage; // or location. What does it mean? It's a mystery...
	int typ;
	int item;
	int repeats;
	
	// Expect it to start with a valid format ID
	int v = SALT_code_decode(encoding);
	StringDump(IntToString(v));
	
	// v3
	if (v < 3 || v > 6) {
		return ""; // Unsupported :()
	}
	
	// v3: [version]title|Author|description|~[supply][minutes][seconds][percentage][type][item id]...
	// v4: [version]title|Author|description|~[supply][minutes][seconds][type][item id]...
	// v5: [version]title~[supply][minutes][seconds][type][item id]...
	// v6: [version]title~[supply][minutes][seconds][location][type][item id][description];...
	
	// Find title end
	pos = StringFind(encoding, "~", c_stringCase);
	if (pos == c_stringNotFound) { return ""; }
	out = SALT_decode_title(StringSub(encoding, 2, pos - 1));
	
	// Now on to the fun part: Walk over title end, and start decoding the build order
	pos += 1;
	while (true) {
		
		// Decode the properties
		supply = SALT_code_decode(StringSub(encoding, pos, pos));
		pos += 1;
		minutes = SALT_code_decode(StringSub(encoding, pos, pos));
		pos += 1;
		seconds = SALT_code_decode(StringSub(encoding, pos, pos));
		pos += 1;
		if (v == 3 || v == 6) {
			percentage = SALT_code_decode(StringSub(encoding, pos, pos));
			pos += 1;
		} else {
			percentage = 0;
		}
		typ = SALT_code_decode(StringSub(encoding, pos, pos));
		pos += 1;
		item = SALT_code_decode(StringSub(encoding, pos, pos));
		pos += 1;
		
		// Looks legit?
		if (supply < 0 || minutes < 0 || seconds < 0 || percentage < 0 || typ < 0 || item < 0 ||
                typ >= SALT_code_type_count || item >= SALT_code_count) {
			break;
		}

		// Now decide how to re-frame the requirement in our format
		if (supply > 0) {
			out = out + ("| " + IntToString(4 + supply) + " - ");
		} else {
			out = out + ("| " + IntToString(minutes));
			if (seconds < 10) {
				 out = out + (":0" + IntToString(seconds) + " - ");
			} else {
				out = out + (":" + IntToString(seconds) + " - ");
			}
		}
		
		// Spy ahead: Is the order repeated? Then assume it's meant to be "repeated".
		for (repeats = 1; ; repeats += 1) {
			if (SALT_code_decode(StringSub(encoding, pos, pos)) != supply) { break; }
			if (SALT_code_decode(StringSub(encoding, pos+1, pos+1)) != minutes) { break; }
			if (SALT_code_decode(StringSub(encoding, pos+2, pos+2)) != seconds) { break; }
			if (v == 3 || v == 6) {	pos_off = 1; } else { pos_off = 0; }
			if (SALT_code_decode(StringSub(encoding, pos+3+pos_off, pos+3+pos_off)) != typ) { break; }
			if (SALT_code_decode(StringSub(encoding, pos+4+pos_off, pos+4+pos_off)) != item) { break; }
			pos += pos_off + 5;
		}
		
		// Warn if we don't know the encoding
		if (SALT_code_unit_names[typ][item] == "") {
			if (GameIsTestMap(false)) {
				StringDump("Unknown SALT unit code: " + IntToString(typ) + " / " + IntToString(item));
			}
		}

		// Then finally add what we are supposed to build
		if (repeats > 1) {
			out = out + IntToString(repeats) + " " + SALT_code_unit_names[typ][item];
		} else {
			out = out + SALT_code_unit_names[typ][item];			
		}
	}
	
	return out;
}