
#ifndef UNIT_H
#define UNIT_H

#include "galaxy.h"

// Games
MK_ENUM(Game,
	(G_HotS)
	(G_LotV)
)

// Races
MK_ENUM(Race, (R_Zerg))

// Setup for unit list. We want to create a nice specialised
// copy procedure along with it, which takes a bit of extra effort.
#define MK_UNIT_TYPES(list) \
	MK_ENUM(UnitType, list (UT_Count)) \
	typedef ARR(int, UT_Count, UnitTypeList); \
	INLINE void UnitType_copyArray(AREF(int,UnitTypeList) to, AREF(int,UnitTypeList) from) { \
		BOOST_PP_SEQ_FOR_EACH_I(_MK_UNIT_TYPES_UNROLL, _, list) \
	}
#define _MK_UNIT_TYPES_UNROLL(r, name, i, elem) to[i] = from[i];

// Units. In our case, this is pretty much anything that we can have
// something of - be it an actual unit, a building, enough energy or
// an idle research slot.
MK_UNIT_TYPES(

	// * Zerg

	// Buildings
	(UT_Hatchery)         // (building it implies taking a new base)
	(UT_Lair)             // (doesn't consume hatchery when built)
	(UT_Hive)             // (doesn't consume lair when built)
	(UT_MacroHatchery)
	(UT_Extractor)
	(UT_SpawningPool)
	(UT_EvolutionChamber)
	(UT_BanelingNest)
	(UT_RoachWarren)
	(UT_HydraliskDen)
	(UT_Spire)
	(UT_InfestationPit)
	(UT_UltraliskCavern)
	(UT_NydusNetwork)
	(UT_NydusCanal)
	(UT_SpineCrawler)
	(UT_SporeCrawler)

	// Units
	(UT_Larva)
	(UT_Overlord)
	(UT_Overseer)
	(UT_OverlordTransport)
	(UT_Drone)
	(UT_Queen)
	(UT_Zergling)
	(UT_Baneling)
	(UT_Roach)
	(UT_Ravager)
	(UT_Hydralisk)
	(UT_Lurker)
	(UT_Mutalisk)
	(UT_Corruptor)
	(UT_Broodlord)
	(UT_Ultralisk)
	(UT_Viper)
	(UT_Infestor)
	// Why no swarm host, you ask? But why would you ask that question?

	// Research
	(UT_Burrow)
	(UT_Melee1)
	(UT_Melee2)
	(UT_Melee3)
	(UT_Missile1)
	(UT_Missile2)
	(UT_Missile3)
	(UT_Carapace1)
	(UT_Carapace2)
	(UT_Carapace3)
	(UT_MetabolicBoost)      // aka Ling Speed
	(UT_GlialReconstitution) // aka Roach Speed
	(UT_TunnelingClaws)      // aka Roach Covert Ops
	(UT_GroovedSpines)       // aka Hydra Range
	(UT_MuscularAugments)    // aka Hydra Speed
	(UT_CentrifugalHooks)    // aka Bane Speed
	(UT_PneumatizedCarapace) // aka Overlord Speed
	(UT_VentralSacs)         // aka Overlord Drop
	(UT_AdrenalGlands)       // aka the best thing since sliced bread
	(UT_PathogenGlands)      // aka infestor tax
	(UT_LurkerDen)           // aka something that should be a morph, but is easier this way
	(UT_GreaterSpire)        // aka ditto
	(UT_FlyerAttack1)
	(UT_FlyerAttack2)
	(UT_FlyerAttack3)
	(UT_FlyerCarapace1)
	(UT_FlyerCarapace2)
	(UT_FlyerCarapace3)

	// Available mineral patches and gasses
	(UTA_Patch)
	(UTA_Geysir)
	(UTA_Gas) // Means gasses *taken*

	// Worker units = by occupation. Note that the sum doesn't have to
	// match up = as we don't count transferring workers
	// (see the Worker actions;
	(UTA_MinWorker)
	(UTA_GasWorker)

	// Hatchery has two independent functions that can be blocked - build/research/morph and inject
	// We model it as three separate units that always spawn together.
	(UTA_HatcheryBuild)
	(UTA_HatcheryInject)

	// Queen energy - number of casts available
	(UTA_QueenEnergy)

	// Available research slots
	(UTA_SpawningPoolResearch)
	(UTA_BanelingNestResearch)
	(UTA_EvolutionChamberResearch)
	(UTA_RoachWarrenResearch)
	(UTA_HydraliskDenResearch)
	(UTA_SpireResearch)
	(UTA_InfestationPitResearch)

)
const ENUM_INT(UnitType) UT_None = CAST(UnitType, -1);
const int UT_ResearchStart = UT_Burrow;
const int UTA_First = UTA_Patch;

// Unit class. This mainly describes how the unit gets created in the first place.
MK_ENUM(UnitClass,
	(UC_None)

	(UC_MorphLarva) // morph from larvae (zerg units)
	(UC_MorphDrone) // morph from drone (zerg buildings)
	(UC_MorphLing) // morph from zergling (bling)
	(UC_MorphRoach) // morph from roach (ravager)
	(UC_MorphHydralisk) // morph from hydralisk (lurker)
	(UC_MorphOverlord) // morph from overlord (overseer, transport overlord)
	(UC_MorphCorruptor) // morph from corruptor (broodlord)

	(UC_BuildHatchery) // build at hatchery

	(UC_ResearchSpawningPool)
	(UC_ResearchBanelingNest)
	(UC_ResearchEvolutionChamber)
	(UC_ResearchRoachWarren)
	(UC_ResearchHydraliskDen)
	(UC_ResearchSpire)
	(UC_ResearchInfestationPit)

	(UC_Count)
)
const int UC_FirstBuild = UC_BuildHatchery; // Give builder back after this point (= no morph)
const int UC_FirstResearch = UC_ResearchSpawningPool; // First research unit class

void Unit_staticInit(Game g);

#ifndef GALAXY
extern string unitName[UT_Count];
extern string unitAbbrevs[UT_Count];
extern int unitMins[UT_Count];
extern int unitGas[UT_Count];
extern int unitSupply[UT_Count];
extern int unitCap[UT_Count];
extern int unitClass[UT_Count];
extern int unitTime[UT_Count];
extern UnitType unitTech[UT_Count];
extern UnitType unitTech2[UT_Count];
extern int unitMult[UT_Count];

extern UnitType unitClassBuilder[UC_Count];
#endif

#endif
