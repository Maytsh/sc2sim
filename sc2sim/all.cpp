
// Master file to generate Galaxy script...
#include "galaxy.h"

#include "unit.h"
#include "action.h"
#include "state.h"
#include "build.h"
#include "parse.h"

#include "unit.cpp"
#include "action.cpp"
#include "state.cpp"
#include "build.cpp"
#include "parse.cpp"
