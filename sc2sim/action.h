
#ifndef ACTION_H
#define ACTION_H

#include "galaxy.h"
#include "unit.h"
#ifndef GALAXY
struct State;
#endif

const int MAX_SUPPLY = 200;

MK_ENUM(ActionType,

	// External actions
	(AT_Build) // data = what to build
	(AT_MinWorkerDelay)
	(AT_MinWorker)
	(AT_GasWorker)
	(AT_Inject)
	(AT_Noop)
	
	// Automatic actions
	(AT_SpawnLarvaNatural)
	(AT_QueenEnergy)
	(AT_MinExhausted)

	(AT_Count)
)

struct Action {
	ActionType type;
	UnitType unit;
	int activation;
};

void Action_staticInit(Game g);

#ifndef INLINE_BY_HAND
void Action_init0(REF(Action) th, ActionType t);
void Action_init(REF(Action) th, ActionType t, UnitType u);
void Action_copy(REF(Action) to, REF(Action) from);
#else
// Manually inlined. We also skip initialization of "activation".
#define Action_init0(to, t) \
	to.type = t; \
	to.unit = UT_None;
#define Action_init(to, t, u) \
	to.type = t; \
	to.unit = u;
#define Action_copy(to, from) \
	to.type = from.type; \
	to.unit = from.unit; \
	to.activation = from.activation;
#endif

// Result of attempting to start an action
MK_ENUM(ActionCheck,

	// Two "OK" check results: Either immediately = or needs to be queued for completion
	(AC_Done)
	(AC_Delay)

	// Needs more minerals before activation = recheck
	(AC_MoreMins)
	(AC_MoreGas)
	(AC_MoreCap)

	// All results from here mean that we need more of a certain unit type - recheck
	// when one of this unit type gets created.
	(AC_MoreUnit)

	// Impossible (e.g. going over max supply)
	(AC_Impossible)
)

struct ActionCheckResult {
	UnitType unitType;
	int reqMins;
	int reqGas;
	int reqUnit;
	int reqSupply;
	int reqLarvae;
};

// Mode in which to attempt action execution
MK_ENUM(ActionMode,
	(AM_Do) // check, then apply effects
	(AM_Check) // check, but don't apply effects
);

#ifdef GALAXY
#include "state.h"
#endif

// Actions (can have consequences when they start and when they finish)
void ActionCheckResult_init(REF(ActionCheckResult) th);
ActionCheck Action_start(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res);
void Action_finish(REF(Action) th, REF(State) s);

string Action_show(REF(Action) a);
string Action_toSc2Planner(REF(Action) a);

#endif
