
#ifndef STRING_H
#define STRING_H

#include "galaxy.h"

#ifndef GALAXY
#include <string>
#include <sstream>
#include <iostream>

// We replace Galaxy's strings by STL strings.
typedef std::string string;
#endif

// * Constants
const string c_noString = "";
#ifndef GALAXY
const bool c_stringCase = true;
const bool c_stringNoCase = false;
const int c_stringAnywhere = 0;
#endif

// * Function definitions
#ifndef GALAXY
inline int StringLength(string s) {
	return s.length();
}
inline string FormatDuration(int secs) {
	// Get hours and minutes
	//int secs = ticks / 32; ticks %= 32;
	int mins = secs / 60; secs %= 60;
	int hours = mins / 60; mins %= 60;
	// Format
	std::ostringstream sstr;
	sstr.fill('0');
	if (hours > 0) { sstr << hours << ':'; sstr.width(2); }
	sstr << mins << ':'; sstr.width(2);
	sstr << secs;
	// sstr << '.';
	// sstr.width(2);
	// sstr << ticks * 100 / 32;
	return sstr.str();
}

inline string IntToString(int i)
{
	std::ostringstream str;
	str << i;
	return str.str();
}

inline int StringToInt(string s)
{
	std::istringstream str(s);
	int i;
	str >> i;
	return i;
}

extern FILE *log_file;

inline void StringDump(string s)
{
	if (log_file) {
		fputs(s.c_str(), log_file);
		fputc('\n', log_file);
	}
	puts(s.c_str());
}

// Some more string functions that get used for parsing
inline string StringReplaceAll(string s, string fs, string rs)
{
	string work = s;
	size_t pos = 0;
	while((pos = work.find(fs, pos)) != std::string::npos) {
		work.replace(pos, fs.length(), rs);
		pos += rs.length();
	}
	return work;
}

inline string StringSub(string s, int start, int end) {
	return s.substr(start-1, end-start+1);
}

inline string StringWord(string s, int w) {
	// Find first word
	size_t start = 0;
	while (start < s.length() && isspace(s[start]))
		start++;
	// Find the right word end
	for (size_t pos = start; pos < s.length(); ) {
		// 
		if (!isspace(s[pos])) {
			pos++; continue;
		}
		// Word complete?
		if (w == 1) {
			return s.substr(start, pos - start);
		}
		// Go over space
		do { pos++; } while(isspace(s[pos]));
		start = pos;
		w--;
	}
	// Return word
	if (w == 1)
		return s.substr(start, std::string::npos);
	else
		return c_noString;
}

inline bool StringEqual(string s1, string s2, bool caseSens) {
	if (caseSens) {
		return s1 == s2;
	}
	// Sigh. There's really nothing standard for this?
	std::string::iterator i = s1.begin(), j = s2.begin();
	for (; i != s1.end() && j != s2.end(); i++, j++) {
		if (toupper(*i) != toupper(*j)) {
			return false;
		}
	}
	return i == s1.end() && j == s2.end();
}

inline string StringCase(string s, bool upper) {
	if (upper) {
		std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	} else {
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	}
	return s;
}

inline bool StringContains(string s1, string s2, int loc, bool caseSens) {
	assert(caseSens == c_stringCase);
	assert(loc == c_stringAnywhere);
	return s1.find(s2) != std::string::npos;
}

const int c_stringNotFound = -1;

inline int StringFind(string s1, string s2, bool caseSens) {
	assert(caseSens == c_stringCase);
	size_t r = s1.find(s2);
	if (r == std::string::npos) return c_stringNotFound;
	return r + 1;
}

#else

// Some of the above actually don't exist in vanilla Galaxy...
void TextDump(text s) { TriggerDebugOutput(1, s, true); }
void StringDump(string s) { TextDump(StringToText(s)); }
#define StringReplaceAll(s, fs, rs) StringReplaceWord(s, fs, rs, c_stringReplaceAll, c_stringCase)

#endif

INLINE bool StringIsInt(string w) {
	string work = w;
	if (StringLength(w) == 0) { return false; }
	// This is probably the most efficient way
	// that doesn't involve runtime errors. Kill me now.
	work = StringReplaceAll(work, "0", "");
	work = StringReplaceAll(work, "1", "");
	work = StringReplaceAll(work, "2", "");
	work = StringReplaceAll(work, "3", "");
	work = StringReplaceAll(work, "4", "");
	work = StringReplaceAll(work, "5", "");
	work = StringReplaceAll(work, "6", "");
	work = StringReplaceAll(work, "7", "");
	work = StringReplaceAll(work, "8", "");
	work = StringReplaceAll(work, "9", "");
	return StringLength(work) == 0;
}

INLINE string StringTrimLeft(string s, string t) {
	int i; int tl = StringLength(t);
	while (true) {
		for (i = 1; i <= tl; i += 1) {
			if (StringSub(s, 1, 1) == StringSub(t, i, i)) {
				s = StringSub(s, 2, StringLength(s));
				break;
			}
		}
		if (i > tl) { return s; }
	}
	return s;
}

INLINE string StringAlign(int align, string str) {
	int len = StringLength(str);
	string res = "";
	while (len < align) { res = res + " "; len += 1; }
	return res + str;
}

// Format an engine tick number as human-readable duration.
INLINE string FormatTickSec(int g, int ticks) {
	
	// For HotS, the time multiplier used to be nice and round - 32 ticks per
	// Blizzard second. With LotV, those pansies at Blizzard caved and changed
	// it to "real" seconds, which is apparently exactly 44.8 ticks. What the hell.
	// We multiply by 5 in order to keep the math half-way sane.
	int mult = 160; int time; int mins; int hours;
	string out;
	if (g > 0) { mult = 224; }
	time = (ticks * 50) / mult / 10;

	// Get hours and minutes
	mins = time / 60; time %= 60;
	hours = time / 60; mins %= 60;

	// Format hour
	if (hours > 0) {
		out = IntToString(hours) + ":";
	}
	// Format minutes
	if (mins < 10 && hours > 0) {
		out += "0";
	}
	out += IntToString(mins) + ":";
	// Format seconds
	if (time < 10) {
		out += "0";
	}
	return out + IntToString(time);
}
INLINE string FormatTick(int g, int ticks)
{
	int mult = 160; int time;
	if (g > 0) { mult = 224; }
	time = (ticks * 50) / mult; // 1/10 s
	return FormatTickSec(g, ticks) + "." + IntToString(time % 10);
}

#endif
