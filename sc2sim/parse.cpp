
#include "parse.h"

string unitNameLookup;
int unitNameMaxLength;

void Build_registerUnitName(string unitName, int unitId) {
	int j;
	unitName = StringTrimLeft(unitName, " \t");
	for (j = 2; j < 128; j += 1) {
		if (StringEqual(StringWord(unitName, j), c_noString, c_stringNoCase)) {
			break;
		}
	}
	unitNameLookup = unitNameLookup + "|" + StringCase(unitName, false) + "|" + IntToString(unitId);
	unitNameMaxLength = MaxI(unitNameMaxLength, j - 1);
}

void Build_staticInitParse(Game g) {
	int i; int pos; string unitAbbrev;
	Build_staticInit(g);

	// These are things we will ask for relatively often, so
	// let's cache them.
	unitNameLookup = ""; unitNameMaxLength = 0;
	for (i = 0; i < UTA_First; i+=1) {
		Build_registerUnitName(unitName[i], i);
		unitAbbrev = unitAbbrevs[i];
		while (StringLength(unitAbbrev) > 0) {
			pos = StringFind(unitAbbrev, ",", c_stringCase);
			if (pos == c_stringNotFound) {
				Build_registerUnitName(unitAbbrev, i);
				break;
			} else {
				Build_registerUnitName(StringSub(unitAbbrev, 1, pos - 1), i);
				unitAbbrev = StringSub(unitAbbrev, pos + 1, StringLength(unitAbbrev));
			}
		}
	}
	unitNameLookup = unitNameLookup + "---";
}

int Build_parseUnit(string s, int w, string word, REF(Action) a) {
	int i; int j; string all = StringCase(word, false); string found;
	// Find the unit in string
	for (i = 1; i <= unitNameMaxLength; i += 1) {
		// Full name appears in lookup string? Possibly with -s or -es suffix?
		found = all;
		j = StringFind(unitNameLookup, "|" + found + "|", c_stringCase);
		if (j == c_stringNotFound) {
			if (StringSub(all, StringLength(all), StringLength(all)) == "s") {
				found = StringSub(all, 1, StringLength(all) - 1);
				j = StringFind(unitNameLookup, "|" + found + "|", c_stringCase);
			}
		}
		if (j == c_stringNotFound) {
			if (StringLength(all) > 1 && StringSub(all, StringLength(all)-1, StringLength(all)) == "es") {
				found = StringSub(all, 1, StringLength(all) - 2);
				j = StringFind(unitNameLookup, "|" + found + "|", c_stringCase);
			}
		}
		if (j != c_stringNotFound) {
			// And there's no other name that we could choose by extending?
			// E.g. "Baneling Nest" for "Baneling"
			if (StringWord(s, w + i) == c_noString ||
				StringFind(unitNameLookup, "|" + found + " " + StringCase(StringWord(s, w + i), false), c_stringCase)
			     == c_stringNotFound) {
				// Get ID
				found = StringSub(unitNameLookup, j + StringLength(found) + 2, j + StringLength(found) + 4);
				found = StringSub(found, 1, StringFind(found, "|", c_stringCase) - 1);
				// Should always be an integer now, encoding the unit number
				if (!StringIsInt(found)) { return 0; }
				a.unit = CAST(UnitType, StringToInt(found));
				return i;
			}
		}
		// At least a prefix of something in the lookup string? If not,
		// then we can bail out right away, it's not there.
		if (StringFind(unitNameLookup, "|" + all, c_stringCase) == c_stringNotFound) {
			return 0;
		}
		// Otherwise keep adding extra words until we hopefully find a match.
		all = all + (" " + StringCase(StringWord(s, w + i), false));
	}
	// Nothing found...
	return 0;
}

int Build_parseAction(string s, int w, string word, REF(Action) a) {
	int i;

	// Check verbs
	if (StringEqual(word, "start", c_stringNoCase) ||
		StringEqual(word, "build", c_stringNoCase) ||
		StringEqual(word, "morph", c_stringNoCase) ||
		StringEqual(word, "research", c_stringNoCase)) {

		// We'd expect the next one to be a unit name
		i = Build_parseUnit(s, w+1, StringWord(s, w+1), a);
		if (i == 0) { return 0; }

		// Okay, done
		a.type = AT_Build;
		return i+1;
	}
	if (StringEqual(word, "gas", c_stringNoCase) ||
		StringEqual(word, "mineral", c_stringNoCase)) {

		if (StringEqual(word, "gas", c_stringNoCase)) {
			a.type = AT_GasWorker;
		} else {
			a.type = AT_MinWorker;
		}

		// Next one might be "drone" or "worker"
		if (StringEqual(StringWord(s, w + 1), "drone", c_stringNoCase) ||
			StringEqual(StringWord(s, w + 1), "worker", c_stringNoCase)) {
			return 3;
		}
		return 2;
	}

	// Check naked unit name
	i = Build_parseUnit(s, w, word, a);
	if (i > 0) {
		// Interpret it as a build command
		a.type = AT_Build;
		return i+1;
	}

	return 0;
}

int Build_parseTo(string s, int w, REF(Step) step) {
	string word = StringWord(s, w);

	if (StringEqual(word, "to", c_stringNoCase) &&
		StringIsInt(StringWord(s, w+1))) {
	
		step.upTo = StringToInt(StringWord(s, w+1));
		return 2;
	}

	if (StringEqual(StringSub(word, 1, 1), "x", c_stringNoCase) &&
		StringIsInt(StringSub(word, 2, StringLength(word)))) {

		step.repeat = StringToInt(StringSub(word, 2, StringLength(word)));
		return 1;
	}

	return 0;
}

int Build_parse(REF(Build) th, string s, Game g)
{
	// Defaults
	Step step;
	int w;
	string word;
	int i;
	int j;
	string str;
	int gas_offset = 0;
	int errors = 0;
	string error_words;

	step.supply = 0;
	step.time = 0;
	step.gas = 0;

	// Prepare string. We unify seperators and
	// make sure that every token will get its own word.
	// Poor man's tokenization.
	s = StringReplaceAll(s, ",", " , ");
	s = StringReplaceAll(s, "--", " , ");
	s = StringReplaceAll(s, "-", " , ");
	s = StringReplaceAll(s, "|", " | ");
	s = StringReplaceAll(s, "\n", " | ");
	s = StringReplaceAll(s, "\"", " \" ");
	s = StringReplaceAll(s, "  ", " ");

	// Go through words
	for (w = 1; ; w+=1) {
		word = StringWord(s, w);

		if (word == ",") { continue; }

		step.repeat = 1;
		step.upTo = 0;
		step.action.type = AT_Build;
		step.action.unit = UT_None;
		step.label = "";

		// End of string?
		if (word == c_noString) {
			break;
		}

		// New line?
		if (word == "|") {
			step.supply = 0;
			step.time = 0;
			step.gas = 0;
			continue;
		}

		// Begins with "@"?
		if (StringEqual(StringSub(word, 1, 1), "@", c_stringCase)) {

			// Just "@"? Go to next word
			if (StringLength(word) == 1) {
				w += 1;
				word = StringWord(s, w);
			} else {
				word = StringSub(word, 2, StringLength(word));
			}

			// Ends with "%"?
			str = StringSub(word, StringLength(word), StringLength(word));
			if (str == "%") {

				// TODO: @100% Spawning Pool
				assert(false);

			// Ends with "g"?
			} else if (str == "g" && StringIsInt(StringSub(word, 1, StringLength(word)-1))) {

				// @100g
				step.gas = gas_offset + StringToInt(StringSub(word, 1, StringLength(word) - 1));
				continue;

			} else if (StringIsInt(word) && (StringEqual(StringWord(s, w+1), "gas", c_stringNoCase) || StringEqual(StringWord(s, w + 1), "g", c_stringNoCase))) {

				// @100 gas
				step.gas = gas_offset + StringToInt(word);
				w += 1;
				continue;

			}
		}

		// Label?
		if (StringEqual(StringSub(word, 1, 1), "\"", c_stringCase)) {
			
			// Build string contained in quotes. Laughably inefficiently.
			str = "";
			for (j = w+1; StringWord(s, j) != "\"" && StringWord(s, j) != c_noString; j += 1) {
				if (j > w+1) { str = str + " "; }
				str = str + StringWord(s, j);
			}
			w = j;

			step.action.type = AT_Noop;
			step.label = str;
			Build_addStepStruct(th, step);
			
			continue;
		}

		// Has a ":"?
		if (StringContains(word, ":", c_stringAnywhere, c_stringCase)) {
			i = StringFind(word, ":", c_stringCase);

			// String before ":" is a number?
			str = StringSub(word, 1, i-1);
			if (StringIsInt(str)) {
				j = StringToInt(str);

				// String after ":" is a number as well? Then it's a time!
				str = StringSub(word, i+1, StringLength(word));
				if (StringIsInt(str)) {
					i = StringToInt(str);

					if (g == G_HotS) {
						step.time = 32 * (j * 60 + i);
					} else if (g == G_LotV) {
						step.time = (224 * (j * 60 + i)) / 5;
					}
					continue;
				}
			}
		}

		// Number?
		if (StringIsInt(word)) {
			i = StringToInt(word);

			// Naked?
			word = StringWord(s, w+1);
			if (StringEqual(word, c_noString, c_stringCase) || 
				StringEqual(word, ",", c_stringCase)) {
				// Supply
				step.supply = MaxI(step.supply, i);
				continue;

			// Otherwise assume action
			} else {
				step.repeat = i;
				w += 1;
				word = StringWord(s, w);
			}
		}

		// "Mass" or "Pump"?
		if (StringEqual(word, "mass", c_stringNoCase) || StringEqual(word, "pump", c_stringNoCase)) {
			word = StringWord(s, w+1);
			j = Build_parseAction(s, w+1, word, step.action);
			if (j == 0) { break; }
			if (j > 0) {
				w += j - 1;
				step.repeat = -1;
				Build_addStepStruct(th, step);
				continue;
			}
		}

		// Action?
		j = Build_parseAction(s, w, word, step.action);
		if (j > 0) {
			w += j - 1;
			w += Build_parseTo(s, w, step);
			if (step.action.type == AT_MinWorker) {
				// *Always* add the gas offset to "Worker to minerals" orders.
				// Otherwise we risk priority inversion preventing rules from
				// ever firing.
				step.gas = MaxI(step.gas, gas_offset);
			}
			Build_addStepStruct(th, step);
			if (step.action.type == AT_Build) {
				// Reduce supply for rules following in the same line. E.g. the following two
				// should be equivalent:
				//   44 - Roach Warren, Evolution Chamber 
				//   44 - Roach Warren | 43 - Evolution Chamber
				// This prevents spurious drones from appearing in the build order. However note
				// that this does not apply to supply getting consumed, so the following two are
				// also equivalent:
				//   44 - Queen, Evolution Chamber
				//   44 - Queen | 44 - Evolution Chamber
				if (step.supply > 0 && unitClass[step.action.unit] == UC_MorphDrone) {
					step.supply -= step.repeat;
				}
				gas_offset += unitGas[step.action.unit];
			}
			continue;
		}

		// Otherwise, we've got an error...
		errors += 1;
		error_words += " " + word;
	}
	if (errors > 0) {
		StringDump("Unrecognized words:" + error_words);
	}

	return errors;
}

bool BuildState_asGoodAs(REF(Build) b, REF(BuildState) bs1, REF(BuildState) bs2)
{
	int i;
	// End time should be at least as good
	if (bs1.state.time > bs2.state.time) {
		return false;
	}
	// And especially every unit should be done at least as early
	for (i = 0; i < UTA_First; i += 1) {
		if (i != UT_Overlord && i != UT_Drone &&
			bs1.unitFinish[i] > bs2.unitFinish[i] && bs1.unitCounter[i] <= bs2.unitCounter[i]) {
			return false;
		}
	}
	// Then it's okay!
	return true;
}

struct Simplify
{
	Race race; Game game;
	int time_limit;
	// Build to optimise
	Build build;
	// Current "reference" build execution
	ActionLog log;
	BuildState state;
	// Log of simplifcations
	string description; // For this pass
	int count; // Since last refresh
	int count_pass;
	int count_total;
};

void Simplify_refresh(REF(Simplify) simp)
{
	if (simp.count > 0) {
		BuildState_exec0(simp.state, simp.build, simp.log, simp.race, simp.game, simp.time_limit);
		simp.count = 0;
	}
}
	
void Simplify_log(REF(Simplify) simp, int step, string desc)
{
	if (simp.count_pass > 0) {
		simp.description += ", ";
	}
	simp.description += "[" + Action_show(simp.build.steps[step].action) + "]" + desc;
	simp.count += 1;
	simp.count_pass += 1;
	simp.count_total += 1;
}

bool Simplify_test(REF(Simplify) simp, REF(Build) b2, bool strictlyBetter, int step, string desc)
{
	// Test new build by executing it
	BuildState bs; ActionLog al2;
	BuildState_exec0(bs, b2, al2, simp.race, simp.game, simp.state.state.time + 10*32);
	
	// Check that build state is at least as good as reference state
	if (!BuildState_asGoodAs(simp.build, bs, simp.state)) {
		return false;
	}
	if (strictlyBetter) {
		// Also do reverse check
		if (BuildState_asGoodAs(simp.build, simp.state, bs)) {
			return false;
		}
	}

	// Accept!
	Build_copy(simp.build, b2);
	//Build_dump(simp.build, simp.game);
	Simplify_log(simp, step, desc);
	return true;
}

void Simplify_finishPass(REF(Simplify) simp, string pass)
{
	if (simp.count_pass > 0) {
		StringDump(pass + ": " + simp.description);
		simp.description = "";
		simp.count_pass = 0;
	}
}

// Not actually a simplification: Reduce supply where a rule was not applied close enough to
// the given time window. This is useful for build orders where we are pretty sure that the
// time stamps are correct, but dubious about supply counts (looking at you, Spawning Tool).
bool Simplify_overrideSupply(REF(Simplify) simp, int window)
{
	int step; int j;  REF(Build) build = simp.build;
	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {

		if (build.steps[step].repeat != 0 && build.steps[step].time > 0 && build.steps[step].supply > 0) {
			j = Build_findLog(simp.log, step);
			if (simp.log.log[j].supply == build.steps[step].supply &&
				simp.log.log[j].action.activation > build.steps[step].time + window) {
				build.steps[step].supply -= 1;
				Simplify_log(simp, step, "-1s");
				// Needs refresh after every change
				return true;
			}
		}
	}
	return false;
}

// Annotate the supply where the rule matched.
void Simplify_addSupply(REF(Simplify) simp, bool onlyWhereMissing)
{
	int step; int j;  REF(Build) build = simp.build; REF(ActionLog) al = simp.log;
	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {
		if (build.steps[step].repeat != 0 && (!onlyWhereMissing || build.steps[step].supply == 0)) {
			j = Build_findLog(al, step);
			if (j != -1 && build.steps[step].supply != al.log[j].supply) {
				build.steps[step].supply = al.log[j].supply;
				Simplify_log(simp, step, "="+IntToString(al.log[j].supply)+"s");
			}
		}
	}
}

// Try reduce the time of rules that are blocked on it
bool Simplify_reduceTime(REF(Simplify) simp, int amount, bool onlyBlocked)
{
	int step; int log_pos;  REF(Build) build = simp.build; REF(ActionLog) al = simp.log;
	Build b2; bool improve = false; string desc;

	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {

		// Attempt to reduce time
		if (build.steps[step].repeat != 0 && build.steps[step].time > 0 && build.steps[step].action.type != AT_Noop) {

			// Make sure step is actually limited by time
			log_pos = Build_findLog(al, step);
			if (log_pos >= 0 && (!onlyBlocked || al.log[log_pos].action.activation <= build.steps[step].time)) {

				// Then test time reduction
				Build_copy(b2, simp.build);
				b2.steps[step].time = MaxI(0, b2.steps[step].time - amount);
				if (b2.steps[step].time > 0) {
					desc = "-" + IntToString(amount) + "t";
				} else {
					desc = "=0t";
				}
				if (Simplify_test(simp, b2, false, step, desc)) {
					improve = true;
				}
			}
		}
	}
	return improve;
}

// Try reduce the supply of rules that are blocked on it
bool Simplify_reduceSupply(REF(Simplify) simp, bool strictlyBetter)
{
	int step; int log_pos;  REF(Build) build = simp.build; REF(ActionLog) al = simp.log;
	Build b2; bool improve = false; string desc;

	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {
		if (build.steps[step].repeat != 0 && build.steps[step].supply > 0) {
			log_pos = Build_findLog(al, step);
			if (log_pos < 0 || al.log[log_pos].supply != b2.steps[step].supply) {
				continue;
			}

			// Try reducing supply (carefully!)
			Build_copy(b2, simp.build);
			b2.steps[step].supply -= 1;

			if (Simplify_test(simp, b2, strictlyBetter, step, desc)) {
				improve = true;
			}
		}

	}
	return improve;
}

// Try to remove repeats of the given unit type. This makes sense for units
// build execution builds automatically (drones and overlords).
bool Simplify_removeReps(REF(Simplify) simp, UnitType ut)
{
	int step; REF(Build) build = simp.build; REF(ActionLog) al = simp.log;
	Build b2; bool improve = false; string desc;

	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {

		// Drone and Overlord rules: Attempt to remove
		while (build.steps[step].repeat > 0 && build.steps[step].action.type == AT_Build && build.steps[step].action.unit == ut) {

			// Test repeat reduction
			Build_copy(b2, simp.build);
			b2.steps[step].repeat -= 1;
			if (Simplify_test(simp, b2, false, step, "-1rep")) {
				improve = true;
			} else {
				break;
			}

		}

	}
	return improve;
}

// Try to move repeats of rules to earlier rules building the same unit type
// This is basically about merging rules
bool Simplify_moveReps(REF(Simplify) simp)
{
	int step; int step2;
	REF(Build) build = simp.build; REF(ActionLog) al = simp.log;
	Build b2; bool improve = false; string desc;

	for (step = MAX_AUTO_STEPS; step < MAX_AUTO_STEPS + build.stepCount; step += 1) {

		// Drone and Overlord rules: Attempt to remove
		if (build.steps[step].repeat > 0 && build.steps[step].action.type == AT_Build) {

			// Find next matching action
			for (step2 = step + 1; step2 < MAX_AUTO_STEPS + build.stepCount; step2 += 1) {
				if (build.steps[step2].action.type == AT_Build &&
					build.steps[step2].action.unit == build.steps[step].action.unit) {

					// Test moving repeats over
					Build_copy(b2, simp.build);
					while (b2.steps[step2].repeat > 0) {
						b2.steps[step2].repeat -= 1;
						b2.steps[step].repeat += 1;
						if (Simplify_test(simp, b2, false, step, "+1rep(m)")) {
							improve = true;
						}
						else {
							break;
						}
					}

				}
			}

		}

	}
	return improve;
}


int Build_simplify(REF(Build) build, Race r, Game g, int iterations, int limit, int override_supply)
{

	int i;
	int iteration;

	// Initialise simplification
	Simplify simp;
	Build_copy(simp.build, build);
	simp.race = r;
	simp.game = g;
	if (limit == 0) limit = 32 * 60 * 60; // 1h
	simp.time_limit = limit;
	simp.count = 0;
	simp.count_pass = 0;
	simp.count_total = 0;
	BuildState_exec0(simp.state, simp.build, simp.log, simp.race, simp.game, simp.time_limit);

	// Override supply on rules with times that are too late?
	if (override_supply > 0) {
		// Do a number of iterations, refreshing after each - this is so that
		// early errors don't impact later timings
		for (iteration = 0; iteration <= 20; iteration += 1) {
			if (Simplify_overrideSupply(simp, override_supply) == 0) {
				break;
			}
			Simplify_refresh(simp);
		}
		Simplify_finishPass(simp, "Supply override");
	}

	// First annotate an extra supply requirement on everything lacking at it
	// (so we can convert from time requirements to supply requirements)
	Simplify_addSupply(simp, true);
	Simplify_finishPass(simp, "Add supply");

	// Now try to simplify
	int imp = 0; bool final = false;
	for (iteration = 0; iteration < iterations; iteration += 1) {

		// Attempt to reduce the time on rules that are blocked on it
		for (i = (2 << (5 - iteration)) * 32; i >= 16; i /= 2) {
			Simplify_reduceTime(simp, i, true);
			Simplify_refresh(simp);
		}
		
		// Finally attempt to remove the time requirement entirely
		Simplify_reduceTime(simp, simp.time_limit, false);
		Simplify_refresh(simp);

		// Attempt to remove repeats of drone and overlord rules
		Simplify_removeReps(simp, UT_Drone);
		Simplify_removeReps(simp, UT_Overlord);
		Simplify_refresh(simp);

		// Finally try to remove supply. We need to be more careful with that,
		// as this might easily mangle the build order!
		Simplify_moveReps(simp);
		Simplify_reduceSupply(simp, true);
		Simplify_refresh(simp);

		if (simp.count_pass == 0) {
			if (final) {
				break;
			}

			// Finally tighten up supply limits for anything that is *not* limited on supply
			Simplify_addSupply(simp, false);
			final = true;
		}
		if (final) {
			Simplify_finishPass(simp, "Final Iteration " + IntToString(iteration));
		} else {
			Simplify_finishPass(simp, "Iteration " + IntToString(iteration));
		}
	}

	// Write build back
	Build_copy(build, simp.build);
	return simp.count_total;
}


