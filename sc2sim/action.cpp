
#include "galaxy.h"
#include "action.h"
#include "state.h"

#ifndef INLINE_BY_HAND
void Action_init0(REF(Action) th, ActionType t) {
	th.type = t;
	th.unit = UT_None;
	th.activation = 0;
}

void Action_init(REF(Action) th, ActionType t, UnitType u) {
	th.type = t;
	th.unit = u;
	th.activation = 0;
}

void Action_copy(REF(Action) to, REF(Action) from) {
	to.type = from.type;
	to.unit = from.unit;
	to.activation = from.activation;
}
#endif

string Action_show(REF(Action) a)
{
	string prefix;
	if (a.type == AT_Build) {
		if (unitClass[a.unit] == UC_MorphLarva || unitClass[a.unit] == UC_MorphDrone ||
			unitClass[a.unit] == UC_MorphLing || unitClass[a.unit] == UC_MorphOverlord || 
			unitClass[a.unit] == UC_MorphRoach || unitClass[a.unit] == UC_MorphHydralisk ||
			unitClass[a.unit] == UC_MorphCorruptor) {
			prefix = "Morph ";
		} else if (unitClass[a.unit] == UC_BuildHatchery) {
			prefix = "Build ";
		} else {
			prefix = "Research ";
		}
		return prefix + unitName[a.unit];
	}
	if (a.type == AT_GasWorker) {
		return "Worker to Gas";
	}
	if (a.type == AT_MinWorker || a.type == AT_MinWorkerDelay) {
		return "Worker to Minerals";
	}
	if (a.type == AT_Inject) {
		return "Inject";
	}
	if (a.type == AT_SpawnLarvaNatural) {
		return "Larva Spawn";
	}
	if (a.type == AT_QueenEnergy) {
		return "Queen Energy";
	}
	if (a.type == AT_Noop) {
		return "No Op";
	}
	assert(false);
	return "Mysterious Action";
}

string Action_toSc2Planner(REF(Action) a) {
	if (a.type == AT_Build) {
		if (a.unit == UT_Hatchery) { return "oA"; }
		else if (a.unit == UT_Lair) { return "oB"; }
		else if (a.unit == UT_Hive) { return "oC"; }
		else if (a.unit == UT_MacroHatchery) { return "oS"; }
		else if (a.unit == UT_Extractor) { return "oD"; }
		else if (a.unit == UT_SpawningPool) { return "oF"; }
		else if (a.unit == UT_BanelingNest) { return "oL"; }
		else if (a.unit == UT_RoachWarren) { return "oH"; }
		else if (a.unit == UT_EvolutionChamber) { return "oG"; }
		else if (a.unit == UT_HydraliskDen) { return "oI"; }
		else if (a.unit == UT_Spire) { return "oN"; }
		else if (a.unit == UT_InfestationPit) { return "oM"; }
		else if (a.unit == UT_UltraliskCavern) { return "oO"; } // fitting
		else if (a.unit == UT_Overlord) { return "j"; }
		else if (a.unit == UT_Drone) { return "a"; }
		else if (a.unit == UT_Queen) { return "h"; }
		else if (a.unit == UT_Zergling) { return "k"; }
		else if (a.unit == UT_Baneling) { return "t"; }
		else if (a.unit == UT_Roach) { return "l"; }
		else if (a.unit == UT_Hydralisk) { return "m"; }
		else if (a.unit == UT_Mutalisk) { return "r"; }
		else if (a.unit == UT_Ultralisk) { return "v"; }
		else if (a.unit == UT_MetabolicBoost) { return "uD"; }
		else if (a.unit == UT_CentrifugalHooks) { return "uH"; }
		else if (a.unit == UT_GlialReconstitution) { return "uG"; }
		else if (a.unit == UT_GroovedSpines) { return "uI"; }
		else if (a.unit == UT_MuscularAugments) { return "iF"; }
		else if (a.unit == UT_AdrenalGlands) { return "uE"; }
		else if (a.unit == UT_Melee1) { return "uM"; }
		else if (a.unit == UT_Melee2) { return "uN"; }
		else if (a.unit == UT_Melee3) { return "uO"; }
		else if (a.unit == UT_Missile1) { return "uP"; }
		else if (a.unit == UT_Missile2) { return "uQ"; }
		else if (a.unit == UT_Missile3) { return "uR"; }
		else if (a.unit == UT_Carapace1) { return "uS"; }
		else if (a.unit == UT_Carapace2) { return "uT"; }
		else if (a.unit == UT_Carapace3) { return "uU"; }
		else if (a.unit == UT_Burrow) { return "uA"; }
		else if (a.unit == UT_PneumatizedCarapace) { return "uB"; }
		else if (a.unit == UT_FlyerAttack1) { return "uV"; }
		else if (a.unit == UT_FlyerAttack2) { return "uW"; }
		else if (a.unit == UT_FlyerCarapace1) { return "uY"; }
		else if (a.unit == UT_FlyerCarapace2) { return "uZ"; }
		else { assert(false); }

	} else if (a.type == AT_GasWorker) {
		return "iD";
	} else if (a.type == AT_MinWorker) {
		return "iB";
	} else if (a.type == AT_Inject) {
		return "f";
	}
	return "";
}


// We don't have a switch statement in Galaxy, so we'll have to make do
// with function lookup tables. These are the default ones that get
// called if we forget to set them for a certain Action (which should
// be avoided, obviously).
ActionCheck Action_startError(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res) {
	assert(false);
	return AC_Done;
}
ActionCheck Action_startNop(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res) {
	return AC_Done;
}
void Action_finishError(REF(Action) th, REF(State) s) {
	assert(false);
}
void Action_finishNop(REF(Action) th, REF(State) s) {
}
void Action_finishBuildNop(REF(Action) th, REF(State) s) {
}
typedef FREF(Action_startError) StartType;
typedef FREF(Action_finishError) FinishType;
typedef FREF(Action_finishBuildNop) FinishBuildType;
ARR(StartType, AT_Count, Action_startMap);
ARR(FinishType, AT_Count, Action_finishMap);
ARR(FinishBuildType, UT_Count, Action_finishBuildMap);

ActionCheck Action_start(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	assert(th.type >= 0 && th.type < AT_Count);
	return Action_startMap[th.type](th, s, mode, res);
}

void Action_finish(REF(Action) th, REF(State) s)
{
	assert(th.type >= 0 && th.type < AT_Count);
	Action_finishMap[th.type](th, s);
}

void ActionCheckResult_init(REF(ActionCheckResult) th)
{
	th.unitType = UT_Drone; // doesn't matter
	th.reqMins = 0;
	th.reqGas = 0;
	th.reqSupply = 0;
	th.reqUnit = UT_None;
	th.reqLarvae = 0;
}

// Canned checks
#define CHECK_UNIT(u) if (s.units[u] <= 0) { res.unitType = u; res.reqUnit = 1; return AC_MoreUnit; }
#define END_CHECK if (mode == AM_Check) { return AC_Done; }

ActionCheck Action_startBuild(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{ 

	// Check resources
	res.reqMins = unitMins[th.unit];
	res.reqGas = unitGas[th.unit];
	res.reqLarvae = 0;
	if (unitClass[th.unit] == UC_MorphLarva) { res.reqLarvae = 1; }
	if (res.reqMins > 0 && s.mins < res.reqMins) { return AC_MoreMins; }
	if (res.reqGas > 0 && s.gas < res.reqGas) { return AC_MoreGas; }

	// Check tech
	if (unitTech[th.unit] != UT_None) {
		CHECK_UNIT(unitTech[th.unit])
	}

	// Check supply last, so we don't get supply blocks
	// until the resources are there and the tech is present
	res.reqSupply = MaxI(0, unitSupply[th.unit] - unitSupply[unitClassBuilder[unitClass[th.unit]]]);
	if (res.reqSupply > 0) {
		if (MAX_SUPPLY < s.supply + res.reqSupply) {
			return AC_Impossible;
		}
		if (s.cap < s.supply + res.reqSupply) { return AC_MoreCap; }
	}

	// Occupy something with actually building it
	assert(unitClassBuilder[unitClass[th.unit]] > UT_None);
	CHECK_UNIT(unitClassBuilder[unitClass[th.unit]])
	if (unitClass[th.unit] == UC_MorphDrone) {
		CHECK_UNIT(UTA_MinWorker)
		END_CHECK
		s.units[UTA_MinWorker]-=1;
	} else {
		END_CHECK
	}
	s.supply -= unitSupply[unitClassBuilder[unitClass[th.unit]]];
	s.units[unitClassBuilder[unitClass[th.unit]]] -= 1;

	// Take cost
	s.mins -= unitMins[th.unit];
	s.gas -= unitGas[th.unit];
	s.supply += unitSupply[th.unit];
	th.activation = s.time + unitTime[th.unit];

	return AC_Delay;
}

void Action_finishBuild(REF(Action) th, REF(State) s)
{

	// Unit is available and starts giving supply
	s.units[th.unit] += unitMult[th.unit];
	s.cap += unitCap[th.unit] - unitCap[unitClassBuilder[unitClass[th.unit]]];

	// Special cases
	Action_finishBuildMap[th.unit](th, s);

	// Finish production - give back the builder (not for morph builds!)
	if (unitClass[th.unit] >= UC_FirstBuild) {
		s.units[unitClassBuilder[unitClass[th.unit]]]+=1;
	}
}

void Action_finishBuildHatchery(REF(Action) th, REF(State) s)
{
	int i;
	// Generate pseudo units
	s.patches+=8;
	s.geysirs+=2;
	s.gasses = MinI(s.geysirs, s.units[UT_Extractor]);
	s.units[UTA_HatcheryBuild]+=1;
	s.units[UTA_HatcheryInject]+=1;
	if (s.game == G_LotV) {
		
	}
	// Transfer drones
	for (i = MinI(s.units[UTA_MinWorker], State_hatchTransfer); i > 0; i-=1) {
		s.units[UTA_MinWorker] -= 1;
		State_scheduleNew(s, AT_MinWorkerDelay, UT_None);
	}
	assert(s.units[UTA_MinWorker] >= 0);
	// Start our natural larvae spawning cycle
	s.units[UT_Larva]+=1;
	State_scheduleNew(s, AT_SpawnLarvaNatural, UT_None);
}
void Action_finishBuildMacroHatchery(REF(Action) th, REF(State) s)
{
	// Generate somewhat fewer pseudo units
	s.units[UTA_HatcheryBuild]+=1;
	s.units[UTA_HatcheryInject]+=1;
	// Start our natural larvae spawning cycle
	s.units[UT_Larva]+=1;
	State_scheduleNew(s, AT_SpawnLarvaNatural, UT_None);
}
void Action_finishBuildExtractor(REF(Action) th, REF(State) s)
{
	s.gasses = MinI(s.geysirs, s.units[UT_Extractor]);
}
void Action_finishBuildDrone(REF(Action) th, REF(State) s)
{
	// add worker to minerals with delay
	// (we only count it once it has finished the first trip)
	State_scheduleNew(s, AT_MinWorkerDelay, UT_None);
}
void Action_finishBuildQueen(REF(Action) th, REF(State) s)
{
	// Start energy generation
	s.units[UTA_QueenEnergy]+=1;
	State_scheduleNew(s, AT_QueenEnergy, UT_None);
}

// Research capabilities
void Action_finishBuildSpawningPool(REF(Action) th, REF(State) s) { s.units[UTA_SpawningPoolResearch] += 1; }
void Action_finishBuildBanelingNest(REF(Action) th, REF(State) s) { s.units[UTA_BanelingNestResearch] += 1; }
void Action_finishBuildEvolutionChamber(REF(Action) th, REF(State) s) { s.units[UTA_EvolutionChamberResearch] += 1; }
void Action_finishBuildRoachWarren(REF(Action) th, REF(State) s) { s.units[UTA_RoachWarrenResearch] += 1; }
void Action_finishBuildHydraliskDen(REF(Action) th, REF(State) s) { s.units[UTA_HydraliskDenResearch] += 1; }
void Action_finishBuildSpire(REF(Action) th, REF(State) s) { s.units[UTA_SpireResearch] += 1; }
void Action_finishBuildInfestationPit(REF(Action) th, REF(State) s) { s.units[UTA_InfestationPitResearch] += 1; }

ActionCheck Action_startMinWorker(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	CHECK_UNIT(UTA_GasWorker)
	END_CHECK
	s.units[UTA_GasWorker]-=1;
	th.activation = s.time + 12 * 32;
	return AC_Delay;
}
void Action_finishMinWorker(REF(Action) th, REF(State) s)
{
	s.mins+=5;
	s.minWorkers+=1;
}


ActionCheck Action_startMinWorkerDelay(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	END_CHECK
	// We just see workers transferring as not doing anything for 12 full seconds.
	// This takes care of transfer inefficiencies (finding the right patch...) as
	// well as compensating for the fact that income through workers is more "spiky"
	// than we assume, which means that a real game will always be just a tiny 
	// bit behind.
	//
	// We also try to compensate for worker transfer: We assume that every new
	// base will be around 10 seconds travel distance from the last one, which
	// means that we build drones from all hatcheries at the same time each base
	// adds around 5 seconds average travel time. Obviously this is again very
	// flawed reasoning, as fresh bases have next to no larvae on them and bases
	// generally aren't aligned in a linear fashion. There's also the issue
	// that the mineral income calculation assumes the close-by patches at a
	// fresh base are saturated right when the base finishes. All in all a huge
	// number of things we can under- or overestimate here...
	th.activation = s.time + 16 * State_workerDelayBase + 
		MaxI(16 * State_workerDelayMin,
			 2 * State_workerDelayAdd * s.units[UT_Drone] +
			 2 * State_workerDelayAdd2 * s.units[UTA_Patch]);
	return AC_Delay;
}
void Action_finishMinWorkerDelay(REF(Action) th, REF(State) s)
{
	s.mins+=5;
	s.minWorkers+=1;
}

ActionCheck Action_startGasWorker(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	CHECK_UNIT(UTA_MinWorker)
	END_CHECK
	s.units[UTA_MinWorker]-=1;
	th.activation = s.time + 12 * 32;
	return AC_Delay;
}
void Action_finishGasWorker(REF(Action) th, REF(State) s)
{
	s.gas+=4;
	s.gasWorkers+=1;
}

ActionCheck Action_startInject(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	CHECK_UNIT(UTA_QueenEnergy)
	CHECK_UNIT(UTA_HatcheryInject)
	END_CHECK
	s.units[UTA_QueenEnergy]-=1;
	s.units[UTA_HatcheryInject]-=1;
	th.activation = s.time + 40 * 32;
	return AC_Delay;
}
void Action_finishInject(REF(Action) th, REF(State) s)
{
	if (s.game == G_LotV) {
		s.units[UT_Larva] += 3;
	} else {
		s.units[UT_Larva] += 4;
	}
	s.units[UTA_HatcheryInject]+=1;
}

ActionCheck Action_startSpawnLarvaNatural(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	th.activation = s.time + 15 * 32;
	return AC_Delay;
}

void Action_finishSpawnLarvaNatural(REF(Action) th, REF(State) s)
{
	// Stop larvae production if we have 3 larvae per hatchery. This
	// is very much a simplification - with injects we could easily
	// end up with, say, 6 larvae on one hatchery, but a second one
	// that would stil spawns larvae naturally. On the other hand, we
	// can also easily see situations where misaligned injects would
	// lead to hatcheries stopping natural spawning while the total
	// number of larvae never reaches this limit.
	if (s.units[UT_Larva] < 3 * s.units[UT_Hatchery]) {
		s.units[UT_Larva] += 1;
	}

	// Simplification number two: The natural spawn timer is
	// supposed to reset every time larvae count goes to 3 on a
	// hatchery, whereas we only check the larvae count now and then.
	// This is especially noteworthy because injects reset the
	// natural larvae respawn cycle every time they pop. Theoretically
	// this should mean that for a perfectly injected hatcery we only
	// get 2 natural larvae per inject cycle (as 40s < 44.4s < 3*15s).
	State_scheduleNew(s, AT_SpawnLarvaNatural, UT_None);
}

ActionCheck Action_startQueenEnergy(REF(Action) th, REF(State) s, ActionMode mode, REF(ActionCheckResult) res)
{
	th.activation = s.time + 45 * 32;
	return AC_Delay;
}

void Action_finishQueenEnergy(REF(Action) th, REF(State) s)
{
	s.units[UTA_QueenEnergy]+=1;
	State_scheduleNew(s, AT_QueenEnergy, UT_None);
}

void Action_finishMinExhausted(REF(Action) th, REF(State) s)
{
	s.units[UTA_Patch] = MaxI(0, s.units[UTA_Patch] - th.unit);
}

void Action_staticInit(Game g) {
	int i;

	// Call through to static initialization of units
	// (probably ugly to do it this way, but hey...)
	Unit_staticInit(g);

	for (i = 0; i < AT_Count; i+=1) {
		Action_startMap[i] = Action_startError;
		Action_finishMap[i] = Action_finishError;
	}
	for (i = 0; i < UT_Count; i+=1) {
		Action_finishBuildMap[i] = Action_finishBuildNop;
	}
	Action_startMap[AT_Build] = Action_startBuild;
	Action_finishMap[AT_Build] = Action_finishBuild;
	Action_finishBuildMap[UT_Hatchery] = Action_finishBuildHatchery;
	Action_finishBuildMap[UT_MacroHatchery] = Action_finishBuildMacroHatchery;
	Action_finishBuildMap[UT_Extractor] = Action_finishBuildExtractor;
	Action_finishBuildMap[UT_Drone] = Action_finishBuildDrone;
	Action_finishBuildMap[UT_Queen] = Action_finishBuildQueen;
	Action_finishBuildMap[UT_SpawningPool] = Action_finishBuildSpawningPool;
	Action_finishBuildMap[UT_BanelingNest] = Action_finishBuildBanelingNest;
	Action_finishBuildMap[UT_EvolutionChamber] = Action_finishBuildEvolutionChamber;
	Action_finishBuildMap[UT_RoachWarren] = Action_finishBuildRoachWarren;
	Action_finishBuildMap[UT_HydraliskDen] = Action_finishBuildHydraliskDen;
	Action_finishBuildMap[UT_Spire] = Action_finishBuildSpire;
	Action_finishBuildMap[UT_InfestationPit] = Action_finishBuildInfestationPit;
	Action_startMap[AT_MinWorker] = Action_startMinWorker;
	Action_finishMap[AT_MinWorker] = Action_finishMinWorker;
	Action_startMap[AT_MinWorkerDelay] = Action_startMinWorkerDelay;
	Action_finishMap[AT_MinWorkerDelay] = Action_finishMinWorkerDelay;
	Action_startMap[AT_GasWorker] = Action_startGasWorker;
	Action_finishMap[AT_GasWorker] = Action_finishGasWorker;
	Action_startMap[AT_Inject] = Action_startInject;
	Action_finishMap[AT_Inject] = Action_finishInject;
	Action_startMap[AT_SpawnLarvaNatural] = Action_startSpawnLarvaNatural;
	Action_finishMap[AT_SpawnLarvaNatural] = Action_finishSpawnLarvaNatural;
	Action_startMap[AT_QueenEnergy] = Action_startQueenEnergy;
	Action_finishMap[AT_QueenEnergy] = Action_finishQueenEnergy;
	Action_finishMap[AT_MinExhausted] = Action_finishMinExhausted;
	Action_startMap[AT_Noop] = Action_startNop;
	Action_finishMap[AT_Noop] = Action_finishNop;
}
