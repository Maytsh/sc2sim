
#include "state.h"
#include "action.h"

#ifndef GALAXY
#define TUNABLE(name, v) int name = v;
TUNABLE(State_minHighEff, DEFAULT_MIN_HIGH_EFF);
TUNABLE(State_minLowEff, DEFAULT_MIN_LOW_EFF);
TUNABLE(State_minSatEff, DEFAULT_MIN_SAT_EFF);
TUNABLE(State_hatchTransfer, DEFAULT_HATCH_TRANSFER);
TUNABLE(State_workerDelayBase, WORKER_DELAY_BASE);
TUNABLE(State_workerDelayAdd, WORKER_DELAY_ADD);
TUNABLE(State_workerDelayAdd2, WORKER_DELAY_ADD2);
TUNABLE(State_workerDelayMin, WORKER_DELAY_MIN);
#undef TUNABLE
#endif

// Tunables definition, see state.h

void State_init0(REF(State) th, Race r, Game g)
{
	int i;

	th.race = r;
	th.game = g;
	th.time = 0;
	th.mins = 50;
	th.gas = 0;
	th.supply = 0;
	th.cap = 0;

	th.heapSize = 0;
#ifdef STATE_ROLLBACK
	th.newActions = 0;
	th.oldActions = 0;
	th.earliest = 0;
#endif

	for (i = 0; i < UT_Count; i+=1) {
		th.units[i] = 0;
	}
}

void State_init(REF(State) th, Race r, Game g) {
	Action a; int i;

	State_init0(th, r, g);

	if (r == R_Zerg) {
		Action_init(a, AT_Build, UT_Hatchery); Action_finish(a, th);
		Action_init(a, AT_Build, UT_Overlord); Action_finish(a, th);
		Action_init(a, AT_Build, UT_Drone);
		if (g == G_HotS) {
			for (i = 0; i < 6; i+=1) {
				Action_finish(a, th);
			}
			th.units[UT_Larva] += 2;
			th.supply += 6;
		} else if (g == G_LotV) {
			for (i = 0; i < 12; i+=1) {
				Action_finish(a, th);
			}
			th.units[UT_Larva] += 3;
			th.supply += 12;
		} else {
			assert(false);
		}
	}

#ifdef STATE_ROLLBACK
	State_commit(th);
#endif
}

void State_copy(REF(State) to, REF(State) from)
{
	int i;
	to.race = from.race;
	to.time = from.time;
	to.mins = from.mins;
	to.gas = from.gas;
	to.supply = from.supply;
	to.cap = from.cap;
	UnitType_copyArray(to.units, from.units);
	//for (i = 0; i < UT_Count; i+=1) {
	//	to.units[i] = from.units[i];
	//}
	to.heapSize = from.heapSize;
#ifdef STATE_ROLLBACK
	assert(from.oldActions == 0);
	assert(from.newActions == 0);
	to.oldActions = from.oldActions;
	to.newActions = from.newActions;
	to.earliest = from.earliest;
#endif
	for (i = 0; i < to.heapSize + NEW_ACTIONS(to); i+=1) {
		Action_copy(to.actions[i], from.actions[i]);
	}
}

int State_minIncome(REF(State) th) {
	// Let f be the income function. Efficiency is gradually going down
	// from highEff to lowEff until we have 2 worker per patch:
	//   f'(n) = highEff - n * (highEff - lowEff) / 2p
	// Integration gives us with f(0) = 0:
	//   f(n) = n * highEff - n^2 * (highEff - lowEff) / 2p
	int p = MaxI(th.patches, 8);
	int n1 = MinI(th.minWorkers, 2 * p);
	int inc = n1 * State_minHighEff - n1 * n1 * (State_minHighEff - State_minLowEff) / p / 4;
	int n2;
	if (th.minWorkers > n1) {
		// After we have 2 workers per patch, an additional worker can only squeeze
		// up to satEff out of every mineral field. Naturally we start with the
		// low-efficiency patches: 
		//   g'(n) = (satEff - 2 * lowEff) - 2 * n * (highEff - lowEff) / p
		// With g(2p) = f(2p) we get:
		n2 = MinI(th.minWorkers - 2*p, p/2); // see note below
		inc += n2 * (State_minSatEff - 2* State_minLowEff) - n2 * n2 * (State_minHighEff - State_minLowEff) / p;
		// Note: We limit this to 4 extra workers per base - in my testing I found
		// it impossible to hold a third worker on closeby patches. Note that this
		// means that far mineral patches actually mine faster with over-saturation.
	}
	// Nice test: set satEff = 3 * lowEff and highEff = satEff / 2 - the curve should be smooth at these point
	return inc / 3;
}

// Normal and saturated gas mining efficiency (again Liquidpedia)
const int gasNormalEff = 42; // changed - shouldn't it be higher?
const int gasSatEff = 114;

int State_gasIncome(REF(State) th) {
	int satGasses;
	// A lot easier: Just check whether we are oversaturated or not
	if (th.gasWorkers >= 3 * th.gasses) {
		return gasSatEff * th.gasses; // Likely to be the default case
	} else if (th.gasWorkers > 2 * th.gasses) {
		satGasses = th.gasWorkers - 2 * th.gasses;
		return satGasses * gasSatEff + gasNormalEff * (th.gasWorkers - 3 * satGasses);
	} else {
		return gasNormalEff * th.gasWorkers;
	}
}

int State_exec(REF(State) th, REF(Action) a, int limit, REF(ActionCheckResult) resData) {
	int res; int newlimit;
	// Can we execute the action?
	if (th.mins >= resData.reqMins &&
		th.gas >= resData.reqGas &&
		th.cap >= th.supply + resData.reqSupply &&
		th.units[resData.unitType] >= resData.reqUnit) {

		// Let's give it a whirl
		res = Action_start(a, th, AM_Do, resData);

		// Marvellous. Let's do it then.
		if (res == AC_Done) {
			Action_finish(a, th);
			return 0;
		}
		// Almost as perfect - let's wait a bit
		if (res == AC_Delay) {
			State_schedule(th, a);
			return 0;
		}
	}
	// Check limit
	if (th.time >= limit) { return -1; }
	// Calculate step time
	newlimit = 0;

	if (th.mins < resData.reqMins && th.minWorkers > 0) {
		newlimit = MaxI(newlimit, th.time + State_timeToMins(th, resData.reqMins));
	}
	if (th.gas < resData.reqGas && th.gasWorkers > 0) {
		newlimit = MaxI(newlimit, th.time + State_timeToGas(th, resData.reqGas));
	}
	if (newlimit == 0) {
		newlimit = NEVER;
	}
	newlimit = MinI(State_getNextActivation(th), newlimit);
	if (newlimit > limit) {
		return -1;
	}
	return MaxI(MaxI(th.time, 1), newlimit);
}

void State_advance(REF(State) th, int dt);
void State_step(REF(State) th, int limit) {
	Action a;
	// Do we have action that requires attention before that point?
	int nextAct = State_getNextActivation(th);
	//assert(limit >= th.time);
	if (nextAct <= limit) {

		// At least one new action should have matured. Finish it.
		State_advance(th, MaxI(0, nextAct - th.time));
		while (th.time >= State_getNextActivation(th)) {
			Action_copy(a, th.actions[EARLIEST(th)]);
			State_deschedule(th);
			Action_finish(a, th);
		}

	} else {

		// Just plain clock turning
		State_advance(th, limit - th.time);

	}
}

void State_heapifyUp(REF(State) th, int i) {
	// We have inserted a new leaf, and want to restore the heap property.
	int ip; Action a; Action_copy(a, th.actions[i]);

	// Insert into heap by tracing path from leaf - O(log n)
	while(i > 0) {

		// Which fires sooner - parent or current action?
		ip = (i-1) / 2;
		if (th.actions[ip].activation <= a.activation) {
			// Put action, done
			Action_copy(th.actions[i], a);
			return;
		}
		Action_copy(th.actions[i], th.actions[ip]);

		// Advance in tree
		i = ip;
	}
	
	// If we arrive here, we have to put the action as the root
	Action_copy(th.actions[0], a);
}

void State_heapifyDown(REF(State) th, REF(Action) ar, int i, int size)
{
	int i2;
	REFP(Action) a0; REFP(Action) a1; REFP(Action) a2;
	
	// The action that we want to insert at the given position
	Action a; Action_copy(a, ar);

	// Move actions upwards until heap invariant is re-established
	SET_REFP(a0, th.actions[i]);
	while (true) {
		i2 = i*2+2;
		if (i2 >= size) {
			break;
		}
		SET_REFP(a1, th.actions[i2-1]);
		SET_REFP(a2, th.actions[i2]);

		if (a.activation <= GET_REFP(a1).activation) {
			if (a.activation <= GET_REFP(a2).activation) {
				// Insert it here, we are done
				Action_copy(GET_REFP(a0), a);
				return;
			} else {
				Action_copy(GET_REFP(a0), GET_REFP(a2));
				i = i2; a0 = a2;
			}
		} else {
			if (a.activation <= GET_REFP(a2).activation) {
				Action_copy(GET_REFP(a0), GET_REFP(a1));
				i = i2-1; a0 = a1;
			} else if (GET_REFP(a1).activation <= GET_REFP(a2).activation) {
				Action_copy(GET_REFP(a0), GET_REFP(a1));
				i = i2-1; a0 = a1;
			} else {
				Action_copy(GET_REFP(a0), GET_REFP(a2));
				i = i2; a0 = a2;
			}
		}

	}

	// One check left?
	if (i*2+2 == size && th.actions[i*2+1].activation < a.activation) {
		Action_copy(th.actions[i], th.actions[i*2+1]);
		Action_copy(th.actions[i*2+1], a);
	} else {
		Action_copy(th.actions[i], a);
	}
}


void State_scheduleNew(REF(State) th, ActionType type, UnitType ut)
{
	// Create action
	Action a; ActionCheckResult r; ActionCheck res;
	a.type = type;
	a.unit = ut;
	// Caller should have made sure that this action needs to be scheduled
	res = Action_start(a, th, AM_Do, r);
	assert(res == AC_Delay);
	State_schedule(th, a);
}

void State_schedule(REF(State) th, REF(Action) ar) {
	assert (ar.activation >= th.time);
	assert (th.heapSize + NEW_ACTIONS(th) < MAX_ACTIONS);

	// Simply push back until commit
	Action_copy(th.actions[th.heapSize + NEW_ACTIONS(th)], ar);

#ifdef STATE_ROLLBACK

	// Check whether action is the new earliest
	if (ar.activation < th.actions[th.earliest].activation) {
		th.earliest = th.heapSize + th.newActions;
	}

	// Done
	th.newActions += 1;
#else

	// Insert into heap
	th.heapSize += 1;
	State_heapifyUp(th, th.heapSize-1);

#endif
}

#ifdef STATE_ROLLBACK
void State_commit(REF(State) th) {
	int i; int newHeapSize;

	// Save stats
	th.old_time = th.time;
	th.old_mins = th.mins;
	th.old_gas = th.gas;
	th.old_supply = th.supply;
	th.old_cap = th.cap;

	// Overwrite old actions by new ones, while building the new heap.
	// This is a cross between the standard heap building and the
	// delete-max heap algo - should be pretty close to optimal
	// (O(n), that is).
	newHeapSize = th.heapSize + th.newActions - th.oldActions;
	for (i = MinI(newHeapSize, th.oldActions) - 1; i >= 0; i -= 1) {

		// We do heapification at the same time - this will overwrite
		// th.actions[i] with something appropriate. As we go through
		// backwards, we will have a valid heap at the end of this loop.
		State_heapifyDown(th, th.actions[th.heapSize + th.newActions - i - 1], i, MinI(newHeapSize, th.heapSize));

	}

	// If we have extra actions left over, we insert them using the
	// standard bubble-up algorithm
	for (i = th.oldActions; i < th.newActions; i+=1) {
		State_heapifyUp(th, th.heapSize + i - th.oldActions);
	}

	for (i = 0; i < UT_Count; i+=1) {
		th.oldUnits[i] = th.units[i];
	}

	// Done!
	th.heapSize = newHeapSize;
	th.oldActions = 0;
	th.newActions = 0;
	th.earliest = 0; // now it's guaranteed to be the root
}

void State_rollback(REF(State) th)
{
	int i;

	// Roll back stats
	th.time = th.old_time;
	th.mins = th.old_mins;
	th.gas = th.old_gas;
	th.supply = th.old_supply;
	th.cap = th.old_cap;
	for (i = 0; i < UT_Count; i+=1) {
		th.units[i] = th.oldUnits[i];
	}

	// Restore heap to old state (easy!)
	th.newActions = 0;
	th.oldActions = 0;
	th.earliest = 0;
}
#endif // STATE_ROLLBACK

void State_deschedule(REF(State) th)
{
#ifdef STATE_ROLLBACK
	Action a; int i; int act;
	// Deschedule "th.earliest"
	
	// Descheduling a "new" action? Unlikely, but whatever...
	if (th.earliest >= th.heapSize) {
		// We can simply forget about it - it was added after
		// the last commit.
		if (th.earliest + 1 < th.heapSize + th.newActions) {
			Action_copy(th.actions[th.earliest], th.actions[th.heapSize + th.newActions - 1]);
		}
		th.newActions -= 1;
	
	} else {

		// Move the action in question to the front, if needed
		if (th.earliest != th.oldActions) {
			Action_copy(a, th.actions[th.oldActions]);
			Action_copy(th.actions[th.oldActions], th.actions[th.earliest]);

			// This move might have violated the heap property at
			// "th.earliest", as we just swapped in a later action.
			// So we need to correct the heap by bubbling down.
			State_heapifyDown(th, a, th.earliest, th.heapSize);
		}
		th.oldActions += 1;

	}

	// Now find new earliest action
	th.earliest = th.oldActions;
	act = th.actions[th.earliest].activation;
	for (i = th.oldActions + 1; i < MinI(th.heapSize, 2 * th.oldActions+1); i+=1) {
		if (th.actions[i].activation < act) {
			th.earliest = i;
			act = th.actions[i].activation;
		}
	}

	// Check "new" actions, too. Again unlikely, but let's not produce bugs here...
	for (i = 0; i < th.newActions; i+=1) {
		if (th.actions[th.heapSize+i].activation < act) {
			th.earliest = th.heapSize+i;
			act = th.actions[th.heapSize+i].activation;
		}
	}

	// Whew, finished.
#else

	// Move last heap element to first position, restore heap
	th.heapSize -= 1;
	if (th.heapSize == 0) {
		th.actions[0].activation = NEVER; // Make sure State_getNextActivation returns NEVER
	} else {
		State_heapifyDown(th, th.actions[th.heapSize], 0, th.heapSize);
	}

#endif
}

void State_advance(REF(State) th, int dt)
{
	// Calculate new minerals and new gas
	int minInc = State_minIncome(th) / 5;
	int gasInc = State_gasIncome(th) / 4;
	int t0 = th.time % (32 * 60);
	int t1 = t0 + dt;
	assert(dt >= 0);
	th.mins += 5 * ((t1 * minInc) / 32 / 60 - (t0 * minInc) / 32 / 60);
	th.gas += 4 * ((t1 * gasInc) / 32 / 60 - (t0 * gasInc) / 32 / 60);
	th.time += dt;
}

// Copy of the above for use for outside... awkward.
int State_minIncomeUntil(REF(State) th, int t1)
{
	int minInc = State_minIncome(th) / 5;
	int t0 = th.time % (32 * 60);
	return 5 * (((t1 % (32 * 60)) * minInc) / 32 / 60 - (t0 * minInc) / 32 / 60);
}
int State_gasIncomeUntil(REF(State) th, int t1)
{
	int gasInc = State_gasIncome(th) / 4;
	int t0 = th.time % (32 * 60);
	return 4 * (((t1 % (32 * 60)) * gasInc) / 32 / 60 - (t0 * gasInc) / 32 / 60);
}

int State_timeToMins(REF(State) th, int mins2)
{
	int minInc = State_minIncome(th) / 5;
	int t0 = th.time % (32 * 60);
	int min_ticks = (mins2 - th.mins + 4) / 5 + (t0 * minInc) / 32 / 60;
	if (minInc <= 0) { return NEVER/10; }
	return (min_ticks * 32 * 60 + minInc - 1) / minInc - t0;
}

int State_timeToGas(REF(State) th, int gas2)
{
	int gasInc = State_gasIncome(th) / 4;
	int t0 = th.time % (32 * 60);
	int gas_ticks = (gas2 - th.gas + 3) / 4 + (t0 * gasInc) / 32 / 60;
	if (gasInc <= 0) { return NEVER / 10; }
	return (gas_ticks * 32 * 60 + gasInc - 1) / gasInc - t0;
}

void State_dump(REF(State) th, int iVerbose)
{
	int i;
	StringDump(State_showOverview(th, 3));
	for (i = UT_None+1; i < UT_Count; i+=1) {
		if (iVerbose == 0 && i >= UTA_First) {
			break;
		}
		if (th.units[i] > 0) {
			StringDump(StringAlign(4, IntToString(th.units[i])) + " x " + unitName[i]);
		}
	}
	if (iVerbose > 1) {
		for(i = 0; i < th.heapSize; i+=1) {
			StringDump(FormatTick(th.game, th.actions[i].activation) + ": " + Action_show(th.actions[i]));
		}
	}
}

string State_dumpCompact(REF(State) th)
{
	int i; string dump = "";
	dump += IntToString(th.race) + " ";
	dump += IntToString(th.time) + " ";
	dump += IntToString(th.mins) + " ";
	dump += IntToString(th.gas) + " ";
	dump += IntToString(th.supply) + " ";
	dump += IntToString(th.cap) + " / ";
	for (i = UT_None + 1; i < UT_Count; i += 1) {
		dump += IntToString(th.units[i]) + " ";
	}
	dump += "/ ";
	dump += IntToString(th.heapSize) + " ";
	for (i = 0; i < th.heapSize + NEW_ACTIONS(th); i += 1) {
		dump += IntToString(th.actions[i].type) + " ";
		dump += IntToString(th.actions[i].unit) + " ";
		dump += IntToString(th.actions[i].activation) + " ";
	}
	return dump;
}

void State_readCompact(REF(State) th, string dump)
{
	int i = 0; int w = 1;
	th.race = CAST(Race, StringToInt(StringWord(dump, w))); w += 1;
	th.time = StringToInt(StringWord(dump, w)); w += 1;
	th.mins = StringToInt(StringWord(dump, w)); w += 1;
	th.gas = StringToInt(StringWord(dump, w)); w += 1;
	th.supply = StringToInt(StringWord(dump, w)); w += 1;
	th.cap = StringToInt(StringWord(dump, w)); w += 1;
	assert(StringWord(dump, w) == "/"); w += 1;
	for (i = UT_None + 1; i < UT_Count; i += 1) {
		th.units[i] = StringToInt(StringWord(dump, w)); w += 1;
	}
	assert(StringWord(dump, w) == "/"); w += 1;
	th.heapSize = StringToInt(StringWord(dump, w)); w += 1;
	for (i = 0; i < th.heapSize; i += 1) {
		th.actions[i].type = CAST(ActionType, StringToInt(StringWord(dump, w))); w += 1;
		th.actions[i].unit = CAST(UnitType, StringToInt(StringWord(dump, w))); w += 1;
		th.actions[i].activation = StringToInt(StringWord(dump, w)); w += 1;
	}
}

string State_showOverview(REF(State) th, int iAlign)
{
	int iTimeMult = 32;
	if (th.game == G_LotV) { iTimeMult = 45; } // More like 44.7 (?), but whatever
	return StringAlign(iAlign, IntToString(th.mins)) + "m " +
		   StringAlign(iAlign, IntToString(th.gas)) + "g " +
		   StringAlign(iAlign, IntToString(th.supply)) + "/" +
		   StringAlign(iAlign, IntToString(th.cap)) + " (" +
		   FormatTick(th.game, th.time) + ")";
}
