
#ifndef STATE_H
#define STATE_H

#include "unit.h"
#include "action.h"
#include "galaxy.h"

const int MAX_ACTIONS = 128;

// Tunables - such as low and high and saturated mineral mining efficiency.
// Not exactly the values found on TL Wiki - these were optimised for optimal
// fit to actual game data, see runMineralFit in test.cpp to see why.
// Note that the values inline them for Galaxy, as minIncome needs to be fast.
#define DEFAULT_MIN_HIGH_EFF 134 // 45 minerals / minute
#define DEFAULT_MIN_LOW_EFF 108 // 39.3 minerals / minute
#define DEFAULT_MIN_SAT_EFF 304 // 102 minerals / minute

// Number of drones that are assumed to transfer for every hatchery. This is
// where we catch all the inefficiencies that occur when adding a new base.
#define DEFAULT_HATCH_TRANSFER 0

// The worker delay is added whenever a worker is born or gets transferred
// (see above). The idea is that this catches both travel time to the
// destination mineral patch as well as the time it takes until it stops
// bouncing around and actually does some proper work. Getting this right
// is basically impossible - that's why we fit it instead.
#define WORKER_DELAY_BASE 13 // 6.5 s (base delay)
#define WORKER_DELAY_ADD  -8 // -0.5 s (delay per drone)
#define WORKER_DELAY_ADD2 24 // 1.5 s (delay per patch)
#define WORKER_DELAY_MIN  12 // 6 s (minimum delay)

// Make them into constants. We want them as global (optimsable) global 
// variables for C, hence yet more preprocessor magic.
#ifdef GALAXY
#define TUNABLE(name, v) const int name = v;
#else
#define TUNABLE(name, v) extern int name;
#endif
TUNABLE(State_minHighEff, DEFAULT_MIN_HIGH_EFF);
TUNABLE(State_minLowEff, DEFAULT_MIN_LOW_EFF);
TUNABLE(State_minSatEff, DEFAULT_MIN_SAT_EFF);
TUNABLE(State_hatchTransfer, DEFAULT_HATCH_TRANSFER);
TUNABLE(State_workerDelayBase, WORKER_DELAY_BASE);
TUNABLE(State_workerDelayAdd, WORKER_DELAY_ADD);
TUNABLE(State_workerDelayAdd2, WORKER_DELAY_ADD2);
TUNABLE(State_workerDelayMin, WORKER_DELAY_MIN);
#undef TUNABLE

// Our representation of game state. On a basic level, this is
// just the game stats, unit statistics and the action heap.

// However, we want to change the state back and forth really
// quickly: We have a State_rollback() and State_commit() pair
// which allow us to quickly go back to one old state. This is
// only guaranteed to be fast as long as we don't make too
// many changes between the two points (see below).
// (Note: Nvm, it's actually slower :( )
//#define STATE_ROLLBACK

struct State {

	Game game;
	Race race; // Our race (Zerg only for now)

	// Stats
	int time; // in ticks (32 ticks = 1 Blizzard second = 1 Blizzard release microsecond)
	int mins; int gas; int supply; int cap; // basic game state

	// Old stats at last commit
	int old_time;
#ifdef STATE_ROLLBACK
	int old_mins; int old_gas; int old_supply; int old_cap;
#endif

	// Current and old unit stats
	ARR(int, UT_Count, units);
#ifdef STATE_ROLLBACK
	ARR(int, UT_Count, oldUnits);
#endif

	ARR(Action, MAX_ACTIONS, actions); // heapified
	int heapSize;

#ifdef STATE_ROLLBACK
	// Here's the plan: The array forms a binary heap up to
	// "heapSize" (ordered by activation / finish time). However
	// the first "oldActions" entries are considered dead
	// and removed. Furthermore, there might be "newActions"
	// additional entries at the back, which were added since
	// the last commit, in no particular order.
	int newActions;
	int oldActions;
	int earliest;
#endif
};

#define minWorkers units[UTA_MinWorker]
#define gasWorkers units[UTA_GasWorker]
#define patches units[UTA_Patch]
#define geysirs units[UTA_Geysir]
#define gasses units[UTA_Gas]

void State_init0(REF(State) th, Race r, Game g);
void State_init(REF(State) th, Race r, Game g);
void State_copy(REF(State) to, REF(State) from);

int State_minIncome(REF(State) th);
int State_gasIncome(REF(State) th);

#ifdef STATE_ROLLBACK
void State_commit(REF(State) th);
void State_rollback(REF(State) th);
#endif

INLINE bool State_callbackNone(REF(State) state, REF(Action) a, int dt, REF(ActionCheckResult) acr) { return true; }
typedef FREF(State_callbackNone) StateCallback;

typedef ARR(StateCallback, AT_Count, CallbackArray);

// -1: could not execute
//  0: action executed/scheduled
// >0: wait given time, retry
int State_exec(REF(State) th, REF(Action) a, int limit, REF(ActionCheckResult) acr);

void State_step(REF(State) th, int limit);

void State_dump(REF(State) th, int iVerbose);
string State_showOverview(REF(State) th, int iAlign);
string State_dumpCompact(REF(State) th);
void State_readCompact(REF(State) th, string dump);

const int NEVER = 2147483647; // 0x7fffffff

#ifdef STATE_ROLLBACK
#define NEW_ACTIONS(th) th.newActions
#define OLD_ACTIONS(th) th.newActions
#define EARLIEST(th) th.earliest
INLINE int State_getNextActivation(REF(State) th) {
	if (th.heapSize + NEW_ACTIONS(th) > OLD_ACTIONS(th)) {
		return th.actions[EARLIEST(th)].activation;
	} else {
		return NEVER; 
	}
}
#else
#define NEW_ACTIONS(th) 0
#define OLD_ACTIONS(th) 0
#define EARLIEST(th) 0
#define State_getNextActivation(th) th.actions[0].activation
#endif

void State_scheduleNew(REF(State), ActionType type, UnitType ut);
void State_schedule(REF(State), REF(Action) a);
void State_deschedule(REF(State));

int State_minIncomeUntil(REF(State), int dt);
int State_gasIncomeUntil(REF(State), int dt);
int State_timeToMins(REF(State), int mins2);
int State_timeToGas(REF(State), int gas2);

#endif // STATE_H