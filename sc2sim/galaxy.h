
// Preprocessor definitions for Galaxy script. Used to convert C to Galaxy
// script. Worst idea in the history of anything ever? Possible, please
// queue here for scheduled public pointing and laughing!

#ifndef GALAXY_H
#define GALAXY_H

#define HERO_VERSION "0.0.1"

// For stuff that we declare inside the header
#ifdef GALAXY
#define INLINE
#else
#define INLINE inline
#endif

// No enumeration support in Galaxy... We have to use constants.
// At least we can abuse preprocessor magic so we don't have to
// generate the numbers ourself.
#include <boost/preprocessor/seq/for_each_i.hpp>
#ifdef GALAXY
#define MK_ENUM(t, seq) \
	typedef int t; \
	BOOST_PP_SEQ_FOR_EACH_I(_MK_ENUM, t, seq)
#define _MK_ENUM(r, name, i, elem) const int elem = i;
#define CAST(t,v) v
#define ENUM_INT(e) int
#else
#define MK_ENUM(t, seq) \
	enum t { \
	BOOST_PP_SEQ_FOR_EACH_I(_MK_ENUM, _, seq) \
	};
#define _MK_ENUM(r, name, i, elem) elem = i,
#define CAST(t,v) static_cast<t>(v)
#define ENUM_INT(e) e
#endif

// We also have this particular reference format.
#ifdef GALAXY
#define REF(t) structref<t>
#define FREF(t) funcref<t>
#define AREF(t0,t) arrayref<t>
#define REFP(t) structref<t>
#define SET_REFP(p,v) p = (v)
#define GET_REFP(p) p
#else
#define REF(t) t &
#define FREF(t) decltype(t) *
#define AREF(t0,t) t0 *
#define REFP(t) t *
#define SET_REFP(p,v) p = &(v)
#define GET_REFP(p) (*(p))
#endif

// Array declaration syntax has different order...
#ifdef GALAXY
#define ARR(t, c, n) t[c] n
#else
#define ARR(t, c, n) t n[c]
#endif

// Minimum function is called something different
#ifndef GALAXY
#include <algorithm>
#define MinI std::min<int>
#define MaxI std::max<int>
#endif

// Name is reserved in Galaxy - we simply l33tsp34k it
#ifdef GALAXY
#define unit un1t
#endif

// Might be a good idea to do in-lining manually?
#ifdef GALAXY
#define INLINE_BY_HAND
#endif

// Galaxy doesn't have assertions... So we emulate them (if DEBUG is defined)
#ifdef GALAXY
#ifdef DEBUG
#define ESC(x) #x
#define assert(x) if (!(x)) { \
	TriggerDebugOutput(1, StringToText("Assert '" + #x + "' from " + __FILE__ + " line " + IntToString(__LINE__) + " failed!"), true); \
}
#else
#define assert(x)
#endif
#else
#include <assert.h>
#endif

// String support (see string.h)
#include "string.h"

#endif
