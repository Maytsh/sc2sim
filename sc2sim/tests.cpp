
// Collection of build orders we should be able to execute correctly

#include "build.h"
#include "parse.h"

#include <time.h>

// Dump actions taken?
#define DUMP_ACTION_LOG

// Output SC2Planner representation of executed builds? Website seems defunct though :(
//#define DUMP_SC2PLANNER

// Should we adjust the mineral fit? See below.
//#define ADJUST_MINERAL_FIT

int tests, totalTests, failures, totalFailures, totalUnits;
bool quiet = false;

void runBuild(Game g, const char *name, const char **build, int buildLen, REF(BuildState) bs) {
	Build b; ActionLog log; int i;
	if (!quiet) { StringDump("*** Testing " + std::string(name) + "..."); }

	// Initialize
	Unit_staticInit(g);
	Build_init(b);
	
	// Parse the build
	for (i = 0; i < buildLen; i+=1) {
		if (Build_parse(b, build[i], g) > 0) {
			assert(false);
		}
	}

	State s;
	State_init(s, R_Zerg, g);

	// Execute the build
	Build_initLog(log);
	BuildState_init(bs, b, R_Zerg, g);
	BuildState_setState(bs, b, s);
	BuildState_exec(bs, b, log, 20 * 60 * 32);
	
	if (!quiet) {
#ifdef DUMP_ACTION_LOG
		Build_dumpActionLog(log);
#endif
		StringDump(" ... end state " + State_showOverview(bs.state, 0));
#ifdef DUMP_SC2PLANNER
		Build_dumpActionLogSc2Planner(log);
#endif
		// State_dump(bs.state, 1);
	}

	// Count units - just for fun
	for (i = 0; i < UT_ResearchStart; i++) { totalUnits += bs.unitCounter[i]; }

	tests = failures = 0;

	// Validate action log
	State_init(s, R_Zerg, g);
	if (!Build_testActionLog(log, s)) {
		StringDump("!!! Action log mismatch !!!");
		failures += 1;
	}
}

void check(REF(BuildState) bs, int count, UnitType unit, int min, int sec) {
	totalTests += 1; tests += 1;
	// Check count
	if (count > bs.unitCounter[unit]) {
		StringDump(" !!! " + IntToString(bs.unitCounter[unit]) + "x " + unitName[unit] +
					" (expected=" + IntToString(count) + 
					", d=" + IntToString(count - bs.unitCounter[unit]) +
					") !!!");
		failures++; totalFailures++;
		return;
	}
	// Check timing
	int ticks = (min * 60) + sec;
	if (bs.state.game == G_HotS) {
		ticks *= 32;
	} else {
		ticks = (ticks * 224) / 5;
	}
	if (ticks < bs.unitFinish[unit]) {
		StringDump(" !!! " + unitName[unit] + " @ " + FormatTick(bs.state.game, bs.unitFinish[unit]) +
					" (limit=" + FormatTick(bs.state.game, ticks) +
					", d=" + FormatTick(bs.state.game, bs.unitFinish[unit] - ticks) +
					") !!!");
		failures++; totalFailures++;
		return;
	}
}

void overview() {
	StringDump(" ... " + IntToString(tests) + " checks, " + IntToString(failures) + " failures");
}

void runCheeses() {
	BuildState bs;

	// 8 burrowed roach rush (by Predator_SC, optimised)
	// http://www.reddit.com/r/allthingszerg/comments/1x0fe7/predator_7rr_against_terran_and_5min_burrow_roach/
	//  ... end state 0m 77g 33/34 (5:28.8)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaoDaaiDiDiDoFaaaoHhjjuAfllllllllfoAj
	const char *build[] =
	{
		"9 - Overlord",
		"14 - Extractor",
		"15 - Spawning Pool",
		"17 - Roach Warren",
		"16 - Queen",
		"18 - 2 Overlord",
		"18 - Burrow",
		"18 - 8 Roach",
		"34 - Hatchery",
		"33 - Overlord",
	};
	runBuild(G_HotS, "predator8brr", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_SpawningPool, 3, 20);
	check(bs, 1, UT_Queen, 4, 10);
	check(bs, 1, UT_RoachWarren, 4, 15);
	check(bs, 8, UT_Roach, 5, 20);
	check(bs, 1, UT_Burrow, 5, 50);
	overview();
}

void runMutas() {
	BuildState bs;

	// 6 Muta Troll Build
	// Fastest possible mutas from a 15 Pool (according to SCFusion)
	// ... end state 0m 75g 52 / 52 (7:01.5)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAoDaaiDiDiDoFaaajoBhaaaaaoDoDaaaafiDiDiDaaiDiDiDoNjafaajaaaaaafaaaajrrrrrfrj
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"14 - Extractor",
		"15 - Spawning Pool",
		"16 - Overlord",
		"16 - Queen",
		"16 - Lair",
		"25 - 2 Extractor",
		"28 - Spire",
		"27 - Overlord",
		"40 - 1 Overlord",
		"40 - 6 Mutalisk",
		"50 - 1 Overlord",
	};

	runBuild(G_HotS, "troll6muta", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_SpawningPool, 3, 55);
	check(bs, 1, UT_Lair, 5, 15);
	check(bs, 1, UT_Spire, 6, 55);
	check(bs, 6, UT_Mutalisk, 7, 46);
	
	overview();

}

void runTefel() {
	BuildState bs;

	// Tefel's 2/2 3 Base Roach Hydra ZvT Timing (by Fenn3r)
	// http://www.reddit.com/r/allthingszerg/comments/1r4lsg/tefels_22_3_base_roach_hydra_zvt_timing_a_video/
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"26 - Overlord",
		"28 - 2 Queen",
		"32 - 2 Overlord",
		"46 - Roach Warren",
		"46 - 3 Extractor",
		"43 - Hatchery",
		"48 - 2 Evolution Chamber, 2 Drone, 3 Roach",
		"60 - 2 Extractor",
		"Missile Attacks Level 1",
		"Ground Carapace Level 1",
		"55 - Lair",
		"60 - Drone to 61",
		"Hydralisk Den",
		"Glial Reconstitution",
		"Grooved Spines",
		"Missile Attacks Level 2",
		"Ground Carapace Level 2",
		"60 - 10 Hydralisk, Mass Roach",
    };
	runBuild(G_HotS, "tefel22RoachHydra", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 4, UT_Queen, 5, 30);
	check(bs, 1, UT_RoachWarren, 6, 35);
	check(bs, 2, UT_EvolutionChamber, 7, 10);
	check(bs, 1, UT_Missile1, 9, 45);
	check(bs, 1, UT_Carapace1, 10, 00);
	check(bs, 10, UT_Hydralisk, 11, 05);
	check(bs, 54, UT_Roach, 12, 55);
	check(bs, 1, UT_GlialReconstitution, 10, 50);
	check(bs, 1, UT_GroovedSpines, 11, 00);
	check(bs, 1, UT_Missile2, 12, 50);
	check(bs, 1, UT_Carapace2, 13, 10);
	overview();
}

void runTrollMax() {
	BuildState bs;

	// 5 base roach max (probably theoretical fastest)
	//  ... end state 0m 69g 198/202 (9:59.2)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAaaaaoAaoFajaaaajahaaaaahaoAjafaahaafjoAaaafaaaajafhafajhaaaaafaaaaaaaaafjjafafaaaafaafaaaaaajaaoDoDoDoDoDoDaafjaaaaaafafaaajaaaaaaiDiDiDiDiDiDiDiDiDiDiDiDiDiDiDiDiDiDafaafaaaaaaaaaaafoHaffffjjjjjjjjfjffjllllllllllllllllllllllllljllllflllfllllllllllllfl
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"18 - Hatchery",
		"18 - Spawning Pool",
		"18 - Overlord",
		"22 - Queen, Overlord",
		"30 - Queen",
		"33 - Hatchery",
		"35 - Queen",
		"39 - Hatchery",
		"46 - Queen",
		"50 - Queen",
		"8:00 - 6 Extractor",
		"Drone to 96",
		"90 - Roach Warren, 5 Overlord",
		"Roach to 46",
	};

	runBuild(G_HotS, "troll5BaseMax", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 5, UT_Hatchery, 7, 35);
	check(bs, 1, UT_RoachWarren, 9, 30);
	check(bs, 46, UT_Roach, 10, 55);
	overview();
}

void runRoachBane() {
	BuildState bs;

	// 44 Supply Macro Roach/Bane Timing (by Stark)
	// http://imbabuilds.com/hots-zerg/hots-zvt/zvt-roach-bane-all-in/
	// The third hatch is timely, feels unfair to call it an all-in.

	// Also moved the first gas to a later point - getting ling speed
	// before a good number of lings is out is a bit wasteful.

	//  ... end state 0m 36g 84/86 (7:53.1)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAaaoFaajaahkkhaaaoDoDjaaffaiDiDiDiDiDiDjahaaaaaaafuDfaaaaaoAfafoHajjjjjfoLaflllllllllflkkfkkkkkfkkktttttttttt
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"28 - 2 Extractor",
		"29 - Metabolic Boost",
		"30 - Queen",
		"44 - Hatchery, Roach Warren, 5 Overlord, Baneling Nest",
		"41 - 10 Roach",
		"44 - 10 Zergling",
		"44 - 10 Baneling",
	};

	runBuild(G_HotS, "roachBane44", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 3, UT_Hatchery, 7, 50);
	check(bs, 1, UT_RoachWarren, 7, 10);
	check(bs, 1, UT_BanelingNest, 7, 50);
	check(bs, 10, UT_Roach, 7, 55);
	check(bs, 10, UT_Baneling, 8, 25);
	overview();
}

void runVortix2BaseRoach() {
	BuildState bs;

	// ZvZ VortiX�s 2 Base Roach Speedling Timing (by Stark)
	// http://imbabuilds.com/hots-zerg/hots-zvz/zvz-vortixs-2-base-roach-speedling-timing/
	// ... end state 0m 141g 90/92 (7:48.9)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAaaoFaajaahkkhajaaoDoDaahffajiDiDiDiDiDiDaaaaaaaaafafaaauDoHajfjfjjflllllllllfjllllljllflfkkkkkkkkkkkk

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"27 - Queen",
		"28 - 2 Extractor",
		"44 - Metabolic Boost, Roach Warren",
		"44 - 3 Overlord, 17 Roach, 12 Zergling",
		//"80 - Lair, Evolution Chamber, Missile Attacks Level 1",
	};

	runBuild(G_HotS, "vortix2BaseRoach", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 3, UT_Queen, 5, 30);
	check(bs, 1, UT_Extractor, 4, 50);
	check(bs, 1, UT_RoachWarren, 6, 55);
	check(bs, 1, UT_Roach, 8, 05);
	check(bs, 28, UT_Zergling, 8, 30);
	overview();
}

void runScarlettZvT() {
	BuildState bs;

	// ZvT Scarlett�s 3 Hatchery Macro Opener (by imbabuilds (NoseKnowsAll?))
	// http://imbabuilds.com/hots-zerg/hots-zvt/zvt-scarletts-3-hatchery-macro-opener/
	//  ... end state 0m 108g 160/160 (11:38.0)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAaaoFaajaahkkhajaaaahfhfajaoDoDjaaaaaaafafaaaiDiDiDiDiDiDaaaoAfaoDfajuDaaoGoGaaiDiDiDkkfkkfkjuMuSaaaaafoBfkkkjkkfkoLjaaaaafaafaaaaaaaafuBaaaoSfjfjaoNauHfoDoDoDaaakkkfjfiDiDiDiDiDiDiDiDiDjkkkjkkkfuNajuTttttktajkkfkkfkkkkkkkkkkffkkkkkkkruVrfrfjrrrffjkkrkkkrkkkkfrkfkkrkkkkkfkfk

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"29 - 2 Queen",
		"36 - 2 Extractor",
		"48 - Hatchery, Extractor, Metabolic Boost",
		"50 - 2 Evolution Chamber, 5 Zergling",
		"54 - Melee Attacks Level 1, Ground Carapace Level 1",
		"60 - Lair, 6 Zergling",
		"65 - Baneling Nest",
		"80 - Pneumatized Carapace",
		"83 - Macro Hatchery",
		"83 - Spire, Centrifugal Hooks, 3 Extractor",
		"83 - Melee Attacks Level 2, Ground Carapace Level 2",
		"83 - 5 Baneling, 10 Zergling",
		"100 - Flyer Attacks Level 1, Pump Mutalisk, Pump Zergling",
	};
	runBuild(G_HotS, "scarlettZvT", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 4, UT_Queen, 5, 30);
	check(bs, 2, UT_EvolutionChamber, 7, 05);
	check(bs, 3, UT_Hatchery, 7, 40);
	check(bs, 1, UT_MetabolicBoost, 8, 10);
	check(bs, 1, UT_Lair, 8, 40);
	check(bs, 1, UT_BanelingNest, 8, 45);
	check(bs, 1, UT_PneumatizedCarapace, 9, 20);
	check(bs, 1, UT_Melee1, 9, 45);
	check(bs, 1, UT_Carapace1, 9, 45);
	check(bs, 10, UT_Mutalisk, 12, 10);
	check(bs, 5, UT_Baneling, 10, 20);
	check(bs, 121, UT_Zergling, 12, 05);
	check(bs, 1, UT_Melee2, 13, 05);
	check(bs, 1, UT_Carapace2, 13, 05);
	check(bs, 1, UT_FlyerAttack1, 13, 10);
	overview();
}

void runHyunRoachKing() {
	BuildState bs;

	// Roccat HyuN's ZvT Roach King 1/1 Timing (by Fenn3r)
	// https://www.youtube.com/watch?v=usOXU7asQGY
	// (One of the few that actually gets the timings right! Cheers!)
	//  ... end state 0m 66g 150/150 (9:53.6)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoAaaoFaajaahkkhajaaaahfhfajaoDoDjaaaaaaafafaaiDiDiDiDiDiDjaaaaaaafaafaaaaoAaoBoGfoGafaoHaaaaoDoDaauPuSjfjfjiDiDiDiDiDiDjllllllauGaafaaffjaaaaajllljllflffjllllljlljllllllfjflfllllllllllllll
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"28 - 2 Queen",
		"36 - 2 Extractor",
		"57 - Hatchery, Lair, 2 Evolution Chamber, Roach Warren",
		"60 - 2 Extractor",
		"60 - Missile Attacks Level 1, Ground Carapace Level 1",
		"60 - 4 Overlord, 6 Roach",
		"60 - Glial Reconstitution",
		"65 - Drone to 60",
		"80 - Roach to 40"
	};

	runBuild(G_HotS, "hyunRoachKing", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 4, UT_Queen, 5,	30);
	check(bs, 2, UT_EvolutionChamber, 7, 25);
	check(bs, 1, UT_Lair, 8, 00);
	check(bs, 3, UT_Hatchery, 8, 10);
	check(bs, 1, UT_RoachWarren, 7, 55);

	check(bs, 1, UT_Missile1, 10, 00);
	check(bs, 1, UT_Carapace1, 10, 05);
	check(bs, 40, UT_Roach, 10, 40);

	overview();
}

void runPrebs3Roach() {
	BuildState bs;

	// Seed Preb's 3 roach ZvT opening
	// http://www.reddit.com/r/allthingszerg/comments/20wraa/fun_3_roach_opening_in_zvt/
	// Built specifically to counter reaper expand. Needs some kind of transition.

	//  ... end state 0m 54g 26/28 (4:30.5)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoFaaoAaoDaoHaiDiDiDhjlllfhj
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Hatchery",
		"16 - Extractor",
		"16 - Roach Warren",
		"16 - Queen",
		"18 - Overlord",
		"18 - 3 Roach, Queen",
		"26 - Overlord",
	};

	runBuild(G_HotS, "prebs3Roach", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_SpawningPool, 3, 05);
	check(bs, 2, UT_Hatchery, 4, 25);
	check(bs, 3, UT_Roach, 4, 35);
	check(bs, 2, UT_Queen, 5, 15);

	overview();
}

void runLife13Banes() {
	BuildState bs;

	// Life's 13/12 Baneling build (by Fenn3r)
	// http://www.youtube.com/watch?v=Y_f9ufVROS8
	// Artosis-cursed!
	//  ... end state 5m 155g 36/42 (5:50.4)
	// http://sc2planner.com/?v=hots#ZaaajaaaoDaoFiDiDiDjaaakkkhuDjoLafkkkkkkkfkkkkjkftttat
	const char *build[] =
	{
		"9 - Overlord",
		"1:37 - Extractor",
		"12 - Spawning Pool",
		"14 - 3 Zergling, Queen",
		"19 - Metabolic Boost",
		"19 - Overlord",
		"19 - Baneling Nest",
		"19 - 12 Zergling, 4 Baneling",
	};

	runBuild(G_HotS, "life13Banes", build, sizeof(build) / sizeof(*build), bs);
	overview();
}

void runLeenockUltra() {
	BuildState bs;

	// Leenock's 3-3 Ultra (by thekinkistream)
	// http://www.teamliquid.net/forum/sc2-strategy/410169-zvp-leenock-3-3-ultras
	//  ... end state 0m 939g 200/202 (13:47.6)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaoFaaaoAjahkkaoDahfiDiDiDjaahaaaafafajuDaaaafjfaaaaaaaoDaaaoGfoGafakkkiDiDiDjakkuMffakkkuSoAjkoBfhkkfjoDoDaaaakkkkkkkkfiDiDiDiDiDiDkafaaajoSaoMffafjaaaauNaaaaaaafffuTaaaaoCoDjaoDaaaaaffjfaafiDiDiDoAjaiDiDiDkkkkkkkkkkkkffkkkfkfjkkkkffuEoOafffjjjffjffjuOjvvvvvvfvffjkkkkfkkkkkfkjkkkkkkkkjkkfaaaafafaafaaaaaaaaaafaaaa
	const char *build[] =
	{
		"9 - Overlord",
		"14 - Spawning Pool",
		"16 - Hatchery",
		"15 - Overlord",
		"16 - Queen",
		"18 - 2 Zergling",
		"21 - Extractor",
		"20 - Queen",
		"25 - Queen",
		"32 - Metabolic Boost",
		"44 - Extractor",
		"46 - 2 Evolution Chamber",
		"50 - Melee Attacks Level 1, Ground Carapace Level 1",
		"56 - Hatchery, Lair, Queen",
		"60 - 2 Extractor",
		"75 - Macro Hatchery, Infestation Pit, Melee Attacks Level 2, Ground Carapace Level 2",
		"75 - Drone to 60",
		"85 - Hive, 2 Extractor, Drone to 66",
		"90 - Hatchery, Ultralisk Cavern, Adrenal Glands, Melee Attacks Level 3, Ground Carapace Level 3",
		"90 - Drone to 80",
		"46 - 10 Zergling",
		"56 - 10 Zergling",
		"76 - 10 Zergling",
		"86 - 10 Zergling",
		"100 - 7 Ultralisk, 20 Zergling",
	};

	runBuild(G_HotS, "leenockUltra", build, sizeof(build) / sizeof(*build), bs);
	overview();

}

void runFennerBronze()
{
	BuildState bs;

	// Fenner's Fundamentals Bronze Build
	// http://www.danielfenner.com/fundamentals/bronze/bronzebuild.php
	// Believe it or not, this simple build actually caused a bug.
	//  ... end state 20m 1g 60/66 (7:19.3)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoFaaoDjiDiDiDhkkaoHjjfllllllflljllfllljlflljlfllal
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Extractor",
		"15 - Overlord",
		"15 - Queen",
		"17 - 2 Zergling",
		"20 - Roach Warren",
		"19 - 2 Overlord",
		"19 - Roach to 20" // after first inject of roaches pop move out and kill them!
	};

	runBuild(G_HotS, "fennerBronze", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 05);
	check(bs, 1, UT_RoachWarren, 4, 15);
	check(bs, 20, UT_Roach, 7, 55);

	overview();
}

void runFennerSilver()
{
	BuildState bs;

	// Fenner's Fundamentals Silver Build
	// http://www.danielfenner.com/fundamentals/silver/silverbuild.php
	//  ... end state 0m 25g 100/100 (8:55.9)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoFaaaoAoDjhaiDiDiDaaahfaauDaaaoHafjfjfllllaalfljjlklkkjflfkkklkkjlklfkkfjllkkkkjlfkflkkkjlklkkkflkfklkkkkk
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"17 - Hatchery",
		"16 - Extractor",
		"15 - Overlord",
		"15 - Queen",
		"21 - Queen, Metabolic Boost",
		"28 - Roach Warren",
		"28 - 2 Overlord",
		"28 - 6 Roach",
		"40 - 2 Overlord",
		"40 - Roach to 20, Zergling to 60" // Injects, A-Moving, Attack etc!
	};

	runBuild(G_HotS, "fennerSilver", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 05);
	check(bs, 1, UT_RoachWarren, 5, 40);
	check(bs, 1, UT_MetabolicBoost, 6, 10);
	check(bs, 20, UT_Roach, 9, 20);
	check(bs, 60, UT_Zergling, 9, 35);

	overview();
}

void runFennerGold()
{
	BuildState bs;

	// Fenner's Fundamentals Gold Build
	// http://www.danielfenner.com/fundamentals/gold/goldbuild.php
	//  ... end state 0m 3g 167/172 (11:13.8)
	// http://sc2planner.com/?v=hots#ZaaajaaaaaaoFaaoAjhoDaaaaiDiDiDahfaaaaoHaajffjjfllllllflljllhafafaaaaaaajoBaoDffoDjaaaaaajaiDiDiDiDiDiDaafllfjllljlluGlffjllllljllllfjflllljlllljflfllljlllljlflfjlllllljllflfllllll
	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Hatchery",
		"15 - Overlord",
		"15 - Queen",
		"17 - Extractor",
		"21 - Queen",
		"27 - Roach Warren",
		"28 - 3 Overlord",
		"28 - 10 Roach",
		"48 - Queen",
		"50 - Lair",
		"60 - 2 Extractor",
		"50 - Drone to 41",
		"60 - Glial Reconstitution",
		"60 - Roach to 60",
	};

	runBuild(G_HotS, "fennerGold", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 05);
	check(bs, 1, UT_RoachWarren, 5, 35);
	check(bs, 20, UT_Roach, 11, 50);

	overview();
}


void runAgahamDefRoach()
{
	BuildState bs;

	// aGaham's defensive roach opener
	// http://www.teamliquid.net/forum/sc2-strategy/450110-zvt-defensive-roach-opening
	// Converted into shopping list form!

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"15 - Spawning Pool",
		"17 - Overlord",
		"19 - 2 Queen, 2 Zergling",
		"28 - Queen",
		"33 - Hatchery",
		"44 - 2 Extractor, Roach Warren",
		"54 - 4 Roach",
		"70 - Lair, 2 Extractor",
		"75 - 2 Evolution Chamber, Melee Attacks Level 1, Ground Carapace Level 1"
	};

	runBuild(G_HotS, "aGahamDefRoach", build, sizeof(build) / sizeof(*build), bs);

	overview();
}

void runLucadoZergHydra()
{
	BuildState bs;

	// Lucado's zerg hydra timing (vs Protoss forge first)
	// https://www.youtube.com/watch?v=LMdUix_GDVg

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Hatchery",
		"15 - Overlord",
		"15 - Queen",
		"20 - Zergling",
		"21 - Hatchery",
		"20 - Queen",
		"24 - Queen",
		"42 - Queen",
		"6:00 - 2 Extractor",
		"7:00 - Metabolic Boost",
		"7:30 - Lair, Evolution Chamber",
		"8:00 - Macro Hatchery, 2 Extractor, Missile Attacks Level 1",
		"8:50 - Hydralisk Den, 2 Extractor",
		"Drone to 66, Mass Hydralisk, Mass Zergling",
	};

	runBuild(G_HotS, "lucadoZergHydra", build, sizeof(build) / sizeof(*build), bs);

	overview();
}

void runLifeRoachHydra()
{
	BuildState bs;

	// Life's greedy roach/hydra timing (vs Protoss gateway first)
	// https://www.youtube.com/watch?v=W1wWeWCwl2g

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Hatchery",
		"15 - Overlord",
		"15 - Queen",
		"20 - Zergling",
		"21 - Hatchery",
		"20 - Queen",
		"24 - Queen",
		"42 - Queen",
		"6:00 - 2 Extractor",
		"7:00 - Metabolic Boost",
		"7:30 - Lair, Evolution Chamber",
		"8:00 - Macro Hatchery, 2 Extractor, Missile Attacks Level 1",
		"8:50 - Hydralisk Den, 2 Extractor",
	};

	runBuild(G_HotS, "lifeRoachHydra", build, sizeof(build) / sizeof(*build), bs);

	overview();
}

void runSpeedlingFlood()
{
	BuildState bs;

	const char *build[] =
	{
		"9 - Overlord",
		"15 - Hatchery",
		"16 - Extractor",
		"15 - Spawning Pool",
		"17 - Overlord",
		"17 - 2 Queen",
		"@100 gas - Metabolic Boost, 3 Mineral Drone",		
		"21 - Mass Zergling",
	};

	runBuild(G_HotS, "speedlingFlood", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 1, UT_MetabolicBoost, 5, 55);
	check(bs, 358, UT_Zergling, 19, 35);

	overview();
}

void runLingQueenFlood()
{
	BuildState bs;

	const char *build[] =
	{
		"13 - Overlord",
		"17 - Pool, Drone, Hatchery, Mass Queen, Mass Ling"
	};

	runBuild(G_LotV, "lingQueenFlood", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 1, 33);
	check(bs, 1, UT_Hatchery, 2, 20);
	check(bs, 186, UT_Zergling, 14, 35);
	check(bs, 43, UT_Queen, 15, 00);

	overview();
}

void runBaneBust()
{
	BuildState bs;

	const char *build[] =
	{
		"9 - Overlord",
		"17 - Hatchery, Drone, Pool, Extractor",
		"19 - Overlord, 2 Queen, 3 Ling, Metabolic Boost",
		"30 - Queen, Overlord Speed",
		"44 - Hatchery, Extractor, Drone to 38, Baneling Nest, Ling to 40",
		"60 - 16 Baneling, Mass Baneling, Mass Ling"
	};

	runBuild(G_HotS, "baneBust", build, sizeof(build) / sizeof(*build), bs);
	check(bs, 1, UT_SpawningPool, 3, 50);
	check(bs, 1, UT_MetabolicBoost, 6, 15);
	check(bs, 188, UT_Zergling, 15, 35);
	check(bs, 62, UT_Baneling, 15, 35);

	overview();
}

void runStephanoMaxLotV()
{
	BuildState bs;

	// Buildings' conversion of Stephano's classic build
	// http://www.teamliquid.net/forum/legacy-of-the-void/496993-lets-discuss-recreating-the-asap-roach-max
	//  ... end state 10m 3g 200/202 (7:05.5)

	const char *build[] =
	{
		"13 - Overlord",
		"17 - Hatchery",
		"19 - Hatchery, Spawning Pool",
		"23 - 2 Queen, 2 Zergling",
		"30 - 1 Queen",
		"2:55 - 3 Extractor",
		"@50 gas - Evolution Chamber",
		"@100 gas - Lair, Roach Warren",
		"@100 gas - Missile Attacks Level 1",
		"@100 gas - Metabolic Boost",
		"@100 gas - Roach Speed",
		"67 - Macro Hatchery, Mass Roach, Mass Zergling",
	};

	runBuild(G_LotV, "stephanoLotV", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_SpawningPool, 2, 20);
	check(bs, 1, UT_Lair, 4, 35);
	check(bs, 1, UT_Missile1, 5, 50);
	check(bs, 1, UT_MetabolicBoost, 5, 20);
	check(bs, 1, UT_GlialReconstitution, 5, 55);
	check(bs, 56, UT_Roach, 7, 32);
	check(bs, 38, UT_Zergling, 7, 30);

	overview();
}

void runBug()
{
	BuildState bs;

	const char *build[] =
	{
		"13 - Overlord",
		"17 - Hatchery",
		"17 - Spawning Pool",
		"19 - Overlord, Extractor, \"test",
		"22 - 2 Queen, 2 Zergling",
		"32 - 10 Zergling",
	};

	runBuild(G_LotV, "bug", build, sizeof(build) / sizeof(*build), bs);

	overview();
}

void runBug2()
{
	BuildState bs;

	// The intention here is that "mineral workers" happens before
	// "ling speed", but makes sure that enough gas is mined anyway.
	const char *build[] =
	{
		"13 - Overlord",
		"17 - Hatchery",
		"18 - Extractor, Spawning Pool",
		"19 - Overlord",
		"20 - 2 Queen, 2 Ling, Ling Speed, 3 Mineral Worker",
		"24 - Macro Hatchery, Queen, 40 Ling",
	};

	runBuild(G_LotV, "bug2", build, sizeof(build) / sizeof(*build), bs);

	check(bs, 1, UT_MetabolicBoost, 4, 34);
	check(bs, 84, UT_Zergling, 7, 20);

	overview();

}

int diffTicks(int ticks, int m, int s) {
	// Returns difference of tick number to a mm:ss in LotV time, in 1/10th seconds.
	return (50 * ticks) / 224 - m * 600 - s * 10 - 5;
}

int diffTicksHotS(int ticks, int m, int s) {
	// Returns difference of tick number to a mm:ss in HotS time, in 1/10th seconds.
	return (5 * ticks) / 16 - m * 600 - s * 10 - 5;
}

int getToMineral(BuildState &bs, int &acc, int mins, int m, int s) {

	// Execute until we have the appropriate amount of minerals
	while (bs.state.mins < mins) {
		if (bs.state.time < 1000) {
			State_step(bs.state, 1000);
		} else {
			State_step(bs.state, bs.state.time + State_timeToMins(bs.state, mins));
		}
	}

	// Compare time with reference time (in LotV seconds)
	int diff = diffTicks(bs.state.time, m, s) - acc;
	acc += diff; // accumulate differences, so we don't double-count them
	return abs(diff);
}

const int fitsCount = 60;
static int fitsSum[fitsCount];

int testMineralFit()
{
	BuildState bs;
	int fit = 0; int acc = 0; int fits[fitsCount]; int i;

	for (i = 0; i < fitsCount; i++) fits[i] = 0;
	i = 0;

	// All test runs played on Dusk Towers, LotV. Procedure is to play
	// the build order as efficiently as possible, then note down exact
	// timings we pass 500/1000/... minerals. In combination, this means
	// we can test whether we get both the initial build order as well
	// as (most importantly) the long-term scaling of different drone
	// saturations right.

	// 9 Drones
	const char *build0[] = { "12 - Spawning Pool" , "11 - 2 Extractor" };
	runBuild(G_LotV, "mineralFit9", build0, sizeof(build0) / sizeof(*build0), bs);
	fit += fits[i++] = abs(diffTicks(bs.unitFinish[UT_SpawningPool] - unitTime[UT_SpawningPool], 0, 17));
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 500,  1,17);
	fit += fits[i++] = getToMineral(bs, acc, 1000, 2,15);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 3,11);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 4,07);

	// 14 Drones
	const char *build2[] = { "Drone to 14" };
	runBuild(G_LotV, "mineralFit14", build2, sizeof(build2) / sizeof(*build2), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1000, 1, 24);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 1, 59);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 2, 37);
	
	// 16 Drones
	const char *build[] = { "13 - Overlord, Drone to 16" };
	runBuild(G_LotV, "mineralFit16", build, sizeof(build) / sizeof(*build), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1000, 1, 32);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 4);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 2, 37);
	
	// 20 Drones, 1 base
	const char *build2a[] = { "13 - Overlord", "Drone to 20" };
	runBuild(G_LotV, "mineralFit20", build2a, sizeof(build2a) / sizeof(*build2a), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1000, 1, 42);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 10);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 2, 37);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 3, 06);

	// 22 Drones, 1 base
	const char *build2aa[] = { "13 - Overlord", "Drone to 22" };
	runBuild(G_LotV, "mineralFit20", build2aa, sizeof(build2aa) / sizeof(*build2aa), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 15);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 2, 42);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 3,  9);

	// 11 Drones, 2 bases
	const char *build2ab[] = { "12 - Hatchery" }; // Pull 2 drones to natural
	runBuild(G_LotV, "mineralFit11", build2ab, sizeof(build2ab) / sizeof(*build2ab), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 500, 1, 13);
	fit += fits[i++] = getToMineral(bs, acc, 1000, 2, 00);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 45);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 3, 32);

	// 22 Drones, 2 bases
	const char *build2b[] = { "13 - Overlord", "17 - Hatchery, Drone to 22" };
	runBuild(G_LotV, "mineralFit22", build2b, sizeof(build2b) / sizeof(*build2b), bs);
	acc = 0;
	fit += fits[i++] = abs(diffTicks(bs.unitFinish[UT_Hatchery] - unitTime[UT_Hatchery], 0, 51));
	fit += fits[i++] = getToMineral(bs, acc, 1000, 2,  9);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 35);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 2, 57);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 3, 21);

	// 44 Drones, 2 bases
	const char *build2ba[] = { "13 - Overlord", "17 - Hatchery", "22 - Overlord", "34 - Overlord", "Drone to 44" };
	runBuild(G_LotV, "mineralFit22", build2ba, sizeof(build2ba) / sizeof(*build2ba), bs);
	acc = 0;
	fit += fits[i++] = abs(diffTicks(bs.unitFinish[UT_Hatchery] - unitTime[UT_Hatchery], 0, 52));
	fit += fits[i++] = getToMineral(bs, acc, 3000, 4, 7);
	fit += fits[i++] = getToMineral(bs, acc, 4000, 4, 36);
	fit += fits[i++] = getToMineral(bs, acc, 4500, 4, 49);

	// 24 Drones, 3 bases
	const char *build2c[] = { "13 - Overlord", "17 - Hatchery", "20 - Hatchery, Drone to 24" };
	runBuild(G_LotV, "mineralFit22", build2c, sizeof(build2c) / sizeof(*build2c), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1500, 2, 55);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 3, 17);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 3, 38);
	fit += fits[i++] = getToMineral(bs, acc, 3000, 4, 0);
	fit += fits[i++] = getToMineral(bs, acc, 3500, 4, 21);

	// 32 Drones (and yeah, not exactly the best BO for that)
	const char *build3[] = { "13 - Overlord", "16 - Spawning Pool", "17 - Hatchery", "18 - Queen, Overlord, Drone to 32" };
	runBuild(G_LotV, "mineralFit32", build3, sizeof(build3) / sizeof(*build3), bs);
	fit += fits[i++] = abs(diffTicks(bs.unitFinish[UT_SpawningPool] - unitTime[UT_SpawningPool], 0, 41));
	fit += fits[i++] = abs(diffTicks(bs.unitFinish[UT_Hatchery] - unitTime[UT_Hatchery], 1, 8));
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1000, 3, 00);
	fit += fits[i++] = getToMineral(bs, acc, 1500, 3, 16);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 3, 33);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 3, 50);

	// 48 Drones (3 hatch before pool-ish)
	const char *build4[] = { "13 - Overlord", "17 - Hatchery", "20 - Hatchery, Spawning Pool", "22 - Overlord, 2 Queen, Drone to 48" };
	runBuild(G_LotV, "mineralFit48", build4, sizeof(build4) / sizeof(*build4), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 1500, 3, 54);
	fit += fits[i++] = getToMineral(bs, acc, 2000, 4,  7);
	fit += fits[i++] = getToMineral(bs, acc, 2500, 4, 18);
	fit += fits[i++] = getToMineral(bs, acc, 3000, 4, 29);
	fit += fits[i++] = getToMineral(bs, acc, 3500, 4, 40);
 
	// 64 Drones (4 hatch before pool. Waiting for the meta to catch up!)
	const char *build5[] = { "13 - Overlord", "17 - Hatchery", "20 - Hatchery", "21 - Hatchery", "22 - Spawning Pool", "33 - 2 Queen, Drone to 64" };
	runBuild(G_LotV, "mineralFit64", build5, sizeof(build5) / sizeof(*build5), bs);
	acc = 0;
	fit += fits[i++] = getToMineral(bs, acc, 2500, 4, 40);
	fit += fits[i++] = getToMineral(bs, acc, 3000, 4, 49);
	fit += fits[i++] = getToMineral(bs, acc, 3500, 4, 57);
	fit += fits[i++] = getToMineral(bs, acc, 4000, 5,  5);

	// And the grande finale: Stephano's roach max
	// This replicates https://www.youtube.com/watch?v=01Cxaybr_Nk, where
	// Stephano maxes out in about 11:04 with basically picture-perfect macro
	// without losing a single unit. Pity there aren't more game we can
	// reference like this...
	const char *buildSteph[] =
	{
		"9 - Overlord",
		"15 - Spawning Pool",
		"16 - Overlord",
		"16 - Hatchery",
		"15 - Queen",
		"15 - 2 Zergling",
		"21 - Queen",
		"24 - Hatchery",
		"24 - Overlord",
		"28 - Overlord",
		"31 - Queen",
		"44 - Overlord",
		"6:05 - 2 Extractor",
		"54 - 2 Overlord", // Yes, this has a supply block!
		"6:40 - Extractor",
		"66 - Metabolic Boost",
		"66 - Evolution Chamber, Roach Warren",
		"7:27 - Extractor",
		"67 - Lair",
		"67 - Macro Hatchery, Missile Attacks Level 1",
		"90 - Glial Reconstitution",
		"70 - Drone to 67, 17 Zergling, Mass Roach" // And he overdrones! Buys a few seconds actually.
	};
	runBuild(G_HotS, "stephanoRoachMax", buildSteph, sizeof(buildSteph) / sizeof(*buildSteph), bs);
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_SpawningPool] - unitTime[UT_SpawningPool], 2, 01));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_Hatchery] - unitTime[UT_Hatchery], 4, 14));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_MacroHatchery] - unitTime[UT_MacroHatchery], 7, 49));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_MetabolicBoost] - unitTime[UT_MetabolicBoost], 7, 6));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_Lair] - unitTime[UT_Lair], 7, 30));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_Missile1] - unitTime[UT_Missile1], 7, 52));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_GlialReconstitution] - unitTime[UT_GlialReconstitution], 8, 50));
	fit += abs(fits[i++] = diffTicksHotS(bs.unitFinish[UT_Roach] - unitTime[UT_Roach], 11, 4));

	if (fit < 700) for (i = 0; i < fitsCount; i++) fitsSum[i] += fits[i];
	return fit;
}

const int TUNABLE_COUNT = 8;

template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
	std::hash<T> hasher;
	seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

static void writeTunables(int *tunables) {
	int i = 0;
	State_minHighEff = tunables[i++];
	State_minLowEff = tunables[i++];
	State_minSatEff = tunables[i++];
	State_workerDelayBase = tunables[i++];
	State_workerDelayAdd = tunables[i++];
	State_workerDelayAdd2 = tunables[i++];
	State_workerDelayMin = tunables[i++];
	State_hatchTransfer = tunables[i++];
}

static void readTunables(int *tunables) {
	int i = 0;
	tunables[i++] = State_minHighEff;
	tunables[i++] = State_minLowEff;
	tunables[i++] = State_minSatEff;
	tunables[i++] = State_workerDelayBase;
	tunables[i++] = State_workerDelayAdd;
	tunables[i++] = State_workerDelayAdd2;
	tunables[i++] = State_workerDelayMin;
	tunables[i++] = State_hatchTransfer;
}

static std::size_t hashTunables(int *tunables) {
	std::size_t hash = 0;
	for (int i = 0; i < TUNABLE_COUNT; i++) {
		hash_combine(hash, tunables[i]);
	}
	return hash;
}


const int HASH_TABLE_SIZE = 0x70000000 / sizeof(size_t);

size_t hashTable[HASH_TABLE_SIZE];

int runMineralFit(int range, int fit0)
{

	// Initial values
	int tunables[TUNABLE_COUNT], tunables0[TUNABLE_COUNT];
	readTunables(tunables0);
	readTunables(tunables);

	for (int tries = 0; tries < 100; tries++) {
		// Randomlt test different options in range.
		for (int i = 0; i < TUNABLE_COUNT; i++) {
			tunables[i] += rand() % (2 * range + 1) - range;
		}

		// bound hatchery transfer
		tunables[2] = MinI(tunables[0]*24/10, tunables[2]);
		tunables[4] = MaxI(-10, MinI(60, tunables[4]));
		tunables[5] = MaxI(-10, MinI(60, tunables[5]));
		tunables[6] = MaxI(tunables[6], 1);
		tunables[7] = MaxI(MinI(tunables[7], 32 - tunables[7]), -tunables[7]);

		// Hash
		size_t hash = hashTunables(tunables);
		if (hashTable[hash % HASH_TABLE_SIZE] != hash) {
			hashTable[hash % HASH_TABLE_SIZE] = hash;
			break;
		}
	}

	// Quietly determine fit
	writeTunables(tunables);
	bool oldQuiet = quiet; quiet = true;
	int fit = testMineralFit();
	quiet = oldQuiet;

	// Do some faux-simulated annealing
	int limit = 0; if (range > 1) limit = 1 << (rand() % (range * 5 / 4));
	if (fit > fit0 + limit) {
		writeTunables(tunables0);
		return fit0;
	}

	// Show progress once we reach a certain threshold
	if (fit < 530) {
		StringDump(" fit: h=" + IntToString(State_minHighEff) + ", l=" + IntToString(State_minLowEff) + ", s=" + IntToString(State_minSatEff) + ", db="
			+ IntToString(State_workerDelayBase) + ", da=" + IntToString(State_workerDelayAdd) + ", da2=" + IntToString(State_workerDelayAdd2) +
			", dm=" + IntToString(State_workerDelayMin) + ", ht=" + IntToString(State_hatchTransfer) +
			" (fit" + IntToString(fit) + ")");
	}

	return fit;
}

void runMineralTest()
{
	int fit;

#ifdef ADJUST_MINERAL_FIT
	for (int i = 0; i < fitsCount; i++)
		fitsSum[i] = 0;
	for (int j = 0; j < 1000; j++) {
		// Re-calibrate mineral fit. This takes a long time!
		int bestFit = 100 + runMineralFit(0, 100000);
		srand(unsigned(time(NULL)));
		for (int range = 4; range >= 1; range--) {
			// StringDump(" range= " + IntToString(range));
			for (int i = 0; i < 20000; i++) {
				if ((fit = runMineralFit(range, bestFit)) != bestFit) {
					bestFit = fit;
				}
			}
		}
		// Detect "outliers"
		for (int i = 0; i < fitsCount; i++)
			fitsSum[i] = 0;
	}
#endif

	// Test mineral income fit. It's a lot of individual tests, this allows about
	// +/-1.5 second average deviation on average. This is *very* important
	// to get right, as it will scale basically everything.
	fit = testMineralFit();
	if (fit > 560) {
		StringDump(" !!! Mineral income fit test failed - difference = " + IntToString(fit) + ") !!!");
		failures+=999; totalFailures+=999; // never ignore this one!
	}

	return;
}

void runTests()
{
	totalTests = totalFailures = totalUnits = 0;
	runMineralTest();
#ifdef ADJUST_MINERAL_FIT
	return;
#endif
	runBug();
	runBug2();
	runSpeedlingFlood();
	runLucadoZergHydra();
	runCheeses();
	runVortix2BaseRoach();
	runMutas();
	runTefel();
	runTrollMax();
	runRoachBane();
	// runScarlettZvT(); // disabled due to too many failures. Need to investigate...
	runHyunRoachKing();
	runPrebs3Roach();
	runLife13Banes();
	runLeenockUltra();
	runFennerBronze();
	runFennerSilver();
	runFennerGold();
	runLingQueenFlood();
	runBaneBust();
	runStephanoMaxLotV(); 
	StringDump("Testsuite finished. Total " + IntToString(totalUnits) + " units morphed, " + IntToString(totalTests) + " tests, " + IntToString(totalFailures) + " failures.");
}
