
#ifndef BUILD_H
#define BUILD_H

#include "galaxy.h"
#include "state.h"
#include "string.h"

// Here's where the actual magic happens - what we get is a build order, which
// looks roughly like follows:
//
//  9 - Overlord
//  15 - Hatchery
//  15 - Extractor
//  15 - Spawning Pool
//  17 - Overlord
//  18 - 2 Queen
//  18 - 2 Zergling
//
// Now the standard reading of this would be to do every step exactly once
// all previous ones have finished. We try to be a bit more flexible: We
// are looking for the concrete order to build things so that
//
//   1. All preconditions (e.g. supply) are met for every step
//   2. Priority inversion (steps appearing in different order) appears *only*
//      where it makes the build order execution faster
//
// Condition 2 is obviously a huge can of worms - some very intelligent people
// have built generic algorithms for that, which take forever to untie the
// knots that come from build order scheduling. We are dump, though, so what
// we try to do is micro-optimisations: Matching the first rule in the list
// is still the default, but we allow later rules to overrule *if* we are
// convinced that they neither delay the current rule (the easy part) nor any
// other rules in between (the hard part). That's still a huge pain and comes
// nowhere near being satisfying, but it's the best we can really do.

const int MAX_BUILD_STEPS = 300;
const int MAX_AUTO_STEPS = 40;

const int MAX_ACTION_LOG = 500;

struct Step {
	// How often to repeat - either by a fixed number, or until we have the
	// given number of the unit named in action
	int repeat;
	int upTo;
	// Conditions
	int supply;
	int time;
	int gas;
	// Action to take
	Action action;
	// Label (for timed directions)
	string label;
};

struct Build {
	ARR(Step, MAX_BUILD_STEPS, steps);
	int lastAutoStep;
	int stepCount;
};

// Action log - our output. Should normally be a part of BuildState,
// but SC2 is limited in the maximum size of variables, so we keep
// it separate instead.
struct LogEntry {
	Action action;
	// Applied step.
	int step;
	int repeat; // Number of repeats (mostly 1)
	int index; // Estimated index of this action being performed
	// State at time
	int supply;
	int cap;
	int mins;
	int gas;
};
struct ActionLog {
	int logPos;
	Game game;
	Race race;
	ARR(LogEntry, MAX_ACTION_LOG, log);
};

struct BuildState {
	State state;
#ifndef STATE_ROLLBACK
	State oldState;
#endif
	ARR(int, MAX_BUILD_STEPS, finished); // how much we finished of every build step
	ARR(int, UTA_First, unitCounter); // including building units
	ARR(int, UTA_First, unitFinish); // time the last unit of given type finishes

	// Number of entries in the build log
	int logPos;

	// Number of extractors that have been filled automatically. Any
	// extractors past this point will get drones associated to them
	// automatically.
	int filledGeysirs;

	// Total amount of resources gathered so far (for robustly activating
	// rules like "@100 gas").
	int totalGas;

	// (temporary fields from here)

	// Current time window. This might get reduced when we discard
	// time-limited rules. For example:
	//
	//   8:58 - Overlord
	//   107 - Roach Warren
	//   107 - Roach
	//
	// If the roach warren starts at 8:30, this means that the "overlord"
	// rule will not immediately be considered, as it is too far
	// into the future. However we do not (!) want the roach rule to
	// take over the timed rule in that kind of situation.
	int timeWindow;

	// Build log entries & number of extractors for current
	// un-committed game state
	int curLogPos; int curFilledGeysirs;
	int curTotalGas;

	// Supply offset due to failed rule matches. This is required so we can
	// say 16 Hatch, 15 Overlord without the second step firing first. We just add
	// (+1) to the OL supply in this situation until the hatchery step was applied.
	int supplyOffs;

	// Reserved resources due to failed rule matches.
	int resMins; int resGas; int resSupply; int resLarva;

	// The time the current supply block started, and whether we
	// decided that it was long enough to be a "proper" supply block.
	int supplyBlockStart; bool gotSupplyBlock;
	int supplyBlockOverlord; // time we ought to build the overlord

	// Amount we have to go over the cap in rule inversion to trigger an
	// supply block. See tryInversion.
	int supplyBlockMargin;

	// Supply we aren't allowed to fall below due to inversion.
	// Example - say we have
	//   18 - Overlord
	//   18 - Extractor
	// We might get here with 125 minerals, which means that we try for inversion.
	// However, we do not want to go below 18 due to inversion, as that would
	// defeat the point of the first rule. This might theoretically overlook
	// possible inversions, but it's better to be safe.
	int inversionSupplyLimit;

	// Number of steps that are unfinished
	int unfinishedRules;
	// Number of build order steps that are waiting for more supply
	// This causes drones to be built when as "idle" action, as it
	// might activate more rules.
	int supplyWaitRules;
};

void Build_staticInit(Game g);

void Step_copy(REF(Step) to, REF(Step) from);
void Build_copy(REF(Build) to, REF(Build) from);

void Build_init(REF(Build) th);
void Build_initLog(REF(ActionLog) log);
void Build_addStep(REF(Build) th, int supply, int time, int n, int up, ActionType t, UnitType u);
#define Build_addStep1(th, supply, t, u) Build_addStep(th, supply, 1, 0, t, u)
#define Build_addStepN(th, supply, n, t, u) Build_addStep(th, supply, n, 0, t, u)
#define Build_addStepUp(th, supply, up, t, u) Build_addStep(th, supply, 0, up, t, u)
void Build_addStepStruct(REF(Build) th, REF(Step) step);
bool Build_testActionLog(REF(ActionLog) log, REF(State) state);
void Build_dump(REF(Build) b, Game g);
string Build_show(REF(Build) b, Game g);
void Build_dumpActionLog(REF(ActionLog) log);
void Build_dumpActionLogSc2Planner(REF(ActionLog) log);
int Build_findLog(REF(ActionLog) log, int step);

INLINE bool BuildState_defaultCallback(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, int user) { return true; }

void BuildState_init(REF(BuildState) th, REF(Build) b, Race r, Game g);
bool BuildState_exec(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, int limit);
bool BuildState_exec0(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, Race r, Game g, int limit);
bool BuildState_execC(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, int limit, FREF(BuildState_defaultCallback) callback, int callbackUser);
void BuildState_setState(REF(BuildState) th, REF(Build) b, REF(State) state);

#endif