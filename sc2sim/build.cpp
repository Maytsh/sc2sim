
#include "build.h"

#ifndef GALAXY

// Dump information about decisions while executing builds. Seriously
// useful when figuring out what build order execution is up to.
#define BUILD_DEBUG

// Dump even more detailed information about why certain steps are blocked
// from executing.
#define STEP_ACTIVE_DEBUG

#endif

// Verbosity variable for the above
int Build_verbosity = 0;

// The maximum time we run the simulation until we decide that a rule
// has no actual chance of applying.
//
// We would ideally want to set this as low as possible to make things nice
// and fast, but this is actually critical for getting the "save up to
// something" phases right: If we have rules for high-priority +2 Carapace
// as well as a bunch of roaches, we need to have the (potential) start
// for the upgrade inside the time window in order to gauge whether or
// not building roaches is going to delay it. Otherwise we could run into
// a situation where we first build roaches until +2 comes into the window
// - or worse: Never start it because we keep spending gas!
//
// This also needs to be at minimum 25 seconds in order for us to detect
// supply blocks.
const int MAX_STEP_LENGTH = 60 * 32; // ticks

// Time we tolerate a supply block for until we give up and make an overlord
// (for reference: An overlord takes 25 seconds to build)
const int MAX_SUPPLY_BLOCK_LENGTH = 20 * 32; // ticks

// Idle time at which we start building "filler" units
const int MAX_IDLE_TIME = 10 * 32; // ticks

// Start all automatic overlord this much earlier, in order to account for
// pesky things like human reaction time
const int OVERLORD_OFFSET = 2 * 32; // ticks

// Number of "checkpoints" we make while executing builds in order to return to
// previous points (say, after running into a supply block)
const int UNDO_COUNT = 3;
const int UNDO_PERIOD = 32 * 30; // ticks 

// How long before a hatchery finishes morphing should we start building drones?
// We can be pretty generous here, given that we most likely don't have enough
// mins to build all in one go. Furthermore, in the early game, we will often
// build drones in anticipation of the natural finishing - and later on, we have
// quite some travel time to factor in.
const int DRONES_HATCH_DELAY = 17 * 32 + 30 * 32; // 17 seconds morph + 30 seconds travel
// (this starts making drones once the hatch is about 50% along)

// How many drones we make per a standard base / extractor. Maybe one day this will need updating.
const int DRONES_PER_HATCH = 16;
const int DRONES_PER_GAS = 3;

// Minimum assumed income for a somewhat healthy economy
// (we assume that every build has at least this amount
//  of income wherever it actually matters)
const int RES_MIN_MIN_INCOME = 43 * 6;
const int RES_MIN_GAS_INCOME = 42 * 3;

// When a rule is delayed on a hard time requirement (either by the build or
// because tech needs to come online) we deactivate it if the point of activation
// is farther away than it takes us to earn the resources to get there. Logic being that
// we do not need to start saving up for - say - an extractor one full minute in advance.
// However, this is an inexact science (as everything here): If multiple timed rules
// fire closely together, we might underestimate how early we have to activate these rules.
// This is why we increase the "time window" for timed rules a bit here.
const int TIMED_LOOKAHEAD_FACTOR = 4;

// Allow rules to be applied repeatedly? This is a bit of an advanced feature and has
// lead to all kinds of instability in the past. Can make big mistakes if it goes wrong,
// but normally leads to more natural build order execution. Right now it seems to behave.
#define ALLOW_REPEATS

// Global dummy action check result
ActionCheckResult Build_dummyActionCheckRes;

void Step_init(REF(Step) th, int supply, REF(Action) a) {
	th.repeat = 0;
	th.upTo = 0;
	th.supply = supply;
	th.time = 0;
	th.gas = 0;
	Action_copy(th.action, a);
	th.label = "";
}

void Step_copy(REF(Step) to, REF(Step) from) {
	to.repeat = from.repeat;
	to.upTo = from.upTo;
	to.supply = from.supply;
	to.time = from.time;
	to.gas = from.gas;
	Action_copy(to.action, from.action);
	to.label = from.label;
}

void Build_init(REF(Build) th) {
	th.stepCount = 0;
	th.lastAutoStep = MAX_AUTO_STEPS;
}

void Build_copy(REF(Build) to, REF(Build) from) {
	int i;
	for (i = MAX_AUTO_STEPS; i < MAX_AUTO_STEPS + from.stepCount; i += 1) {
		Step_copy(to.steps[i], from.steps[i]);
	}
	to.stepCount = from.stepCount;
}

void Build_addStep(REF(Build) th, int supply,  int time, int n, int up, ActionType t, UnitType u) {
	int step = MAX_AUTO_STEPS + th.stepCount;
	assert(step < MAX_BUILD_STEPS - 1);
	th.steps[step].repeat = n;
	th.steps[step].upTo = up;
	th.steps[step].supply = supply;
	th.steps[step].time = time;
	Action_init(th.steps[step].action, t, u);
	th.stepCount += 1;
}

void Build_addStepStruct(REF(Build) th, REF(Step) step) {
	int i = MAX_AUTO_STEPS + th.stepCount;
	assert(i < MAX_BUILD_STEPS - 1);
	Step_copy(th.steps[i], step);
	th.stepCount += 1;
}

void Build_addAutoStep(REF(Build) th, int time, ActionType t, UnitType u) {
	assert(th.lastAutoStep > 0);
	if (th.lastAutoStep <= 0) {
		return;
	}
	// Small optimisation: Check whether we can simply add a repeat
	// to the last auto step. This allows us to build lots of
	// overloads at once, if needs be.
	if(th.steps[th.lastAutoStep].time == time) {
		if(th.steps[th.lastAutoStep].action.unit == u) {
			if(th.steps[th.lastAutoStep].action.type == t) {
				th.steps[th.lastAutoStep].repeat += 1;
				return;
			}
		}
	}
	th.lastAutoStep -= 1;
	th.steps[th.lastAutoStep].repeat = 1;
	th.steps[th.lastAutoStep].upTo = 0;
	th.steps[th.lastAutoStep].supply = 0;
	th.steps[th.lastAutoStep].gas = 0;
	th.steps[th.lastAutoStep].time = time;
	Action_init(th.steps[th.lastAutoStep].action, t, u);
}

void Build_staticInit(Game g)
{
	Action_staticInit(g);
	ActionCheckResult_init(Build_dummyActionCheckRes);
}

void Build_initLog(REF(ActionLog) log)
{
	log.logPos = 0;
}

void Build_addActionLog(REF(BuildState) bs, REF(ActionLog) log, REF(State) s, ActionType a, UnitType u, int step, int repeat, int index) {
	int i = bs.curLogPos;
	if (i >= MAX_ACTION_LOG) { return; }
	log.log[i].action.type = a;
	log.log[i].action.unit = u;
	log.log[i].action.activation = s.time;
	log.log[i].supply = s.supply;
	if (a == AT_Build) {
		log.log[i].supply -= unitSupply[u] - unitSupply[unitClassBuilder[unitClass[u]]];
	}
	log.log[i].cap = s.cap;
	log.log[i].mins = s.mins;
	log.log[i].gas = s.gas;
	log.log[i].step = step;
	log.log[i].repeat = repeat;
	log.log[i].index = index;
	bs.curLogPos += 1;
	log.logPos = bs.curLogPos;
}

bool Build_testActionLog(REF(ActionLog) log, REF(State) state)
{
	int i; ActionCheckResult acr; int success;
	// Loop through action log and check that it is consistent
	
	for (i = 0; i < log.logPos; i += 1) {

		// Advance time
		while (state.time < log.log[i].action.activation) {
			State_step(state, log.log[i].action.activation);
		}

		// Make sure that state matches
		assert(log.log[i].supply == state.supply);
		assert(log.log[i].cap == state.cap);

		// Execute action
		ActionCheckResult_init(acr);
		success = State_exec(state, log.log[i].action, 0, acr);
		assert(success == 0);
		if (success != 0) {
			return false;
		}

		assert(log.log[i].mins == state.mins);
		assert(log.log[i].gas == state.gas);
	}
	return true;
}

string Step_show(REF(Step) step, Game g)
{
	string dump;
	if (step.repeat == 0) {
		return "";
	}
	if (step.supply > 0) {
		dump += IntToString(step.supply) + " - ";
	}
	if (step.time > 0) {
		dump += FormatTickSec(g, step.time) + " - ";
	}
	if (step.action.type == AT_Noop) {
		return dump + "\"" + step.label + "\"";
	}
	if (step.repeat > 1) {
		dump += IntToString(step.repeat) + " ";
	}
	else if (step.repeat == -1) {
		dump += "Mass ";
	}
	dump += Action_show(step.action);
	if (step.upTo > 0) {
		dump += " to " + IntToString(step.upTo);
	}
	return dump;
}

void Build_dump(REF(Build) b, Game g)
{
	int i;
	for (i = MAX_AUTO_STEPS; i < MAX_AUTO_STEPS + b.stepCount; i+= 1) {
		if (b.steps[i].repeat == 0) {
			continue;
		}
		StringDump(Step_show(b.steps[i], g));
	}
}

string Build_show(REF(Build) b, Game g)
{
	int i; string dump = "";
	for (i = MAX_AUTO_STEPS; i < MAX_AUTO_STEPS + b.stepCount; i += 1) {
		if (b.steps[i].repeat == 0) {
			continue;
		}
		if (dump != "") dump += " | ";
		dump += Step_show(b.steps[i], g);
	}
	return dump;
}

void Build_dumpActionLog(REF(ActionLog) log)
{
	int i;
	for (i = 0; i < log.logPos; i+=1) {
		StringDump(
			StringAlign(4, IntToString(log.log[i].mins)) + "m " +
			StringAlign(4, IntToString(log.log[i].gas)) + "g " +
			StringAlign(3, IntToString(log.log[i].supply)) + "/" +
			StringAlign(3, IntToString(log.log[i].cap)) + " (" +
			FormatTick(log.game, log.log[i].action.activation) +
			 ") - " + Action_show(log.log[i].action) + " [" + IntToString(log.log[i].step) + " " + IntToString(log.log[i].index) + "]");
	}
}

void Build_dumpActionLogSc2Planner(REF(ActionLog) log)
{
	int i; string s = "http://sc2planner.com/";
	if (log.game == G_HotS) {
		s = s + "?v=hots";
	} else if (log.game == G_LotV) {
		s = s + "?v=lotv";
	}
	if (log.race == R_Zerg) {
		s = s + "#Z";
	}
	for (i = 0; i < log.logPos; i+=1) {
		s = s + Action_toSc2Planner(log.log[i].action);
	}
	StringDump(s);
}

int Build_findLog(REF(ActionLog) log, int step)
{				
	// Find the first log entry for the given build order step
	int j;
	for (j = 0; j < log.logPos; j += 1) {
		if (log.log[j].step == step) {
			return j;
		}
	}
	return -1;
}

// This callback will be called right before time advances.
bool Build_callback(REF(BuildState) bstate, REF(ActionLog) log, REF(Action) a, int dt, REF(ActionCheckResult) acr)
{
	REF(State) state = bstate.state;
	// Got an idle hatchery and a queen with energy to inject?
	while (state.units[UTA_HatcheryInject] > 0 && state.units[UTA_QueenEnergy] > 0) {
		Build_addActionLog(bstate, log, state, AT_Inject, UT_None, -1, 0, 0);
		State_scheduleNew(state, AT_Inject, UT_None);
	}
	// Got fresh extractors to fill?
	while (3 * state.units[UT_Extractor] > bstate.curFilledGeysirs) {
		if (state.units[UTA_MinWorker] > 0) {
			Build_addActionLog(bstate, log, state, AT_GasWorker, UT_None, -1, 1, bstate.curFilledGeysirs);
			State_scheduleNew(state, AT_GasWorker, UT_None);
		}
		bstate.curFilledGeysirs += 1;
	}
	// Or extractors to empty? This can happen if we start in a state where
	// more workers are on gas than expected.
	while (3 * state.units[UT_Extractor] < bstate.curFilledGeysirs) {
		if (state.units[UTA_GasWorker] > 0) {
			Build_addActionLog(bstate, log, state, AT_MinWorker, UT_None, -1, 1, bstate.curFilledGeysirs);
			State_scheduleNew(state, AT_MinWorker, UT_None);
		}
		bstate.curFilledGeysirs -= 1;
	}
	// Got a supply block?
	if (acr.reqSupply > 0 && state.supply + acr.reqSupply > state.cap) {
		// Set supply block start, if necessary
		if (bstate.supplyBlockStart > state.time) {
			bstate.supplyBlockStart = state.time;
		}
		// Check whether supply block length is over limit
		if (state.time + dt >= bstate.supplyBlockStart + MAX_SUPPLY_BLOCK_LENGTH) {
#ifdef BUILD_DEBUG
			if (Build_verbosity > 1) {
				StringDump("Supply block started at " + FormatTick(bstate.state.game, bstate.supplyBlockStart)
					+ ", bailing out at " + FormatTick(bstate.state.game, state.time + dt));
			}
#endif
			// Cancel current execution, we need to take care of this
			bstate.gotSupplyBlock = true;
			return false;
		}
	}
	return true;
}

bool BuildState_execAction(REF(BuildState) th, REF(ActionLog) log, REF(Action) a, int dt) {
	ActionCheckResult acr;
	int limit = th.state.time + dt;
	int newlimit;
	int gas;

	// Prepare for attempting to execute an action...
	ActionCheckResult_init(acr);
	th.supplyBlockStart = NEVER; 

	while(true) {

		// Try to execute
		newlimit = State_exec(th.state, a, limit, acr);
		if (newlimit <= 0) { return newlimit == 0; }

		// Delay - but call callback first
		if (!Build_callback(th, log, a, newlimit - th.state.time, acr)) { return false; }

		// Move time forward
		gas = th.state.gas;
		State_step(th.state, newlimit);
		th.curTotalGas += th.state.gas - gas;

	}
	return false; // unreachable
}

bool BuildState_stepState(REF(BuildState) th, REF(ActionLog) log, REF(Action) a, int limit) {
	int newlimit; int gas;
	th.supplyBlockStart = NEVER;

	assert(limit > th.state.time);
	
	// Calculate how long we are going to delay
	newlimit = MinI(limit, State_getNextActivation(th.state));
	if (!Build_callback(th, log, a, newlimit - th.state.time, Build_dummyActionCheckRes)) { return false; }

	// Move time forward
	gas = th.state.gas;
	State_step(th.state, newlimit);
	th.curTotalGas += th.state.gas - gas;
	return true;
}

void BuildState_init(REF(BuildState) th, REF(Build) b, Race r, Game g)
{
	int i;
	// Initialize state
	State_init(th.state, r, g);
	th.curLogPos = 0; th.logPos = 0;
	th.curFilledGeysirs = 0; th.filledGeysirs = 0;
	th.curTotalGas = 0; th.totalGas = 0;
	// Initialize build steps
	for (i = 0; i < MAX_AUTO_STEPS + b.stepCount + 1; i+=1) {
		th.finished[i] = 0;
	}
	for (i = 0; i < UTA_First; i+=1) {
		th.unitCounter[i] = th.state.units[i];
		if (th.state.units[i] > 0) {
			th.unitFinish[i] = 0;
		} else {
			th.unitFinish[i] = NEVER;
		}
	}
	b.lastAutoStep = MAX_AUTO_STEPS;
}

bool BuildState_stepActive(REF(BuildState) th, REF(Build) b, int iStep, int iLookahead, int moreSupplyOffs, int stepsLeft);

void BuildState_setState(REF(BuildState) th, REF(Build) b, REF(State) state)
{
	ARR(int, UT_Count, units);
	int i; int j; int unit;
	UnitType builder;
	int builderCount; int cnt;
	int pulledFromGas = 0;

	// Copy state
	State_copy(th.state, state);

	// Reset unit finish timing
	for (i = 0; i < UTA_First; i+=1) {
		th.unitFinish[i] = NEVER;
	}

	// Make a copy of the unit count and add building units
	UnitType_copyArray(units, state.units);
	for (i = 0; i < th.state.heapSize; i+=1) {
		if (th.state.actions[i].type == AT_Build) {
			j = th.state.actions[i].unit;
			units[j] += unitMult[j];
			// Also update finish timing
			if (th.unitFinish[j] < NEVER) {
				th.unitFinish[j] = MaxI(th.unitFinish[j], th.state.actions[i].activation);
			} else {
				th.unitFinish[j] = th.state.actions[i].activation;
			}
		}
	}

	// Save back the unit counter
	for (i = 0; i < UTA_First; i += 1) {
		th.unitCounter[i] = units[i];
	}

	// Remove all units we start with
	if (state.game == G_HotS) {
		units[UT_Hatchery] -= 1;
		units[UT_Overlord] -= 1;
		units[UT_Drone] -= 6;
	} else if (state.game == G_LotV) {
		units[UT_Hatchery] -= 1;
		units[UT_Overlord] -= 1;
		units[UT_Drone] -= 12;
	} else {
		assert(false);
	}

	// Clear auto steps. Note that it's important to inialize *all* of them
	// to zero. We might later on make copies of the finished array and *then*
	// increase the number of auto rules, at which point we might otherwise
	// think that we already did the step in question. See BuildState_copy.
	for (i = 0; i < MAX_AUTO_STEPS; i+=1) {
		th.finished[i] = 0;
	}
	b.lastAutoStep = MAX_AUTO_STEPS;

	// Initialize gas count. We will add the cost of all past
	// investments (that are relevant to the build order) below.
	th.curTotalGas = th.state.gas;

	// Initialize number of finished steps
	for (i = MAX_AUTO_STEPS; i < MAX_AUTO_STEPS + b.stepCount; i+=1) {
		th.finished[i] = 0; // Default

		// Build step?
		if (b.steps[i].action.type == AT_Build) {

			// Repeated step?
			if (b.steps[i].repeat > 0) {

				// Calculcate how many repeats we can infer from existing units.
				// Keep in mind that zerglings are annoying rascals.
				unit = b.steps[i].action.unit;
				if (unit == UT_MacroHatchery) {
					unit = UT_Hatchery;
				}
				cnt = units[unit] / unitMult[unit];

				// Check how many we can plausably blame on this rule
				th.finished[i] = MinI(b.steps[i].repeat, cnt);
				th.curTotalGas += unitGas[unit] * th.finished[i];
				units[unit] -= unitMult[unit] * th.finished[i];
				if (th.unitFinish[unit] >= NEVER) {
					th.unitFinish[unit] = 0;
				}

				// Morph order?
				if (unitClass[unit] < UC_FirstBuild) {
					builder = unitClassBuilder[unitClass[unit]];
					if (builder != UT_Larva) {

						// There might be build commands for this unit earlier in the list.
						// For example:
						//
						//  10 Zergling
						//  10 Baneling
						//
						// An end state with 10 zerglings and 10 banes should be acceptable here.
						builderCount = th.finished[i] * unitMult[unit] / unitMult[builder];
						for (j = MAX_AUTO_STEPS; j < i; j += 1) {
							if (b.steps[j].repeat > 0) {
								if (b.steps[j].action.type == AT_Build && b.steps[j].action.unit == builder) {
									cnt = MinI(builderCount, b.steps[j].repeat - th.finished[j]);
									if (cnt > 0) {
										th.finished[j] += cnt;
										builderCount -= cnt;
										if (builderCount <= 0) { break; }
									}
								}
							}
						}
					}
				}

			}
		}

	}

	// We are now going to make a second pass - this time it will matter whether
	// rules are active or not, so we got to adjust some state to make sure
	// that check has everything it needs. One of those is the supply offset:
	// After all, if we have
	//
	//  17 - Hatchery, "Send drone to make Hatchery!"
	//
	// The label will have supply requirement 16, but we should count is as
	// done only if either we are at 17 supply, or we are at 16 supply *and*
	// the hatchery has been built.
	th.supplyOffs = 0;

	// Determine number of drones pulled from gas. We need to loop through the
	// rules again for this, as we need to check rules for whether they're active,
	// and that might well depend on curTotalGas.
	// 
	// Note that we mark all active rules as completely finished, even if there's
	// a discrepancy between the number of actual and expected gas workers. This
	// is because it's easier and faster to manipulate curFilledGeysirs (the
	// mechanism for filling fresh guysers) than to go back and re-activate the
	// exact number of rule repeats we need to reach the correct gas worker count.
	pulledFromGas = 0;
	for (i = MAX_AUTO_STEPS; i < MAX_AUTO_STEPS + b.stepCount; i += 1) {
		if (b.steps[i].action.type == AT_MinWorker) {
			if (BuildState_stepActive(th, b, i, 0, 0, 1)) {
				pulledFromGas += b.steps[i].repeat;
				th.finished[i] = b.steps[i].repeat;
			}
		}
		if (b.steps[i].action.type == AT_GasWorker) {
			if (BuildState_stepActive(th, b, i, 0, 0, 1)) {
				pulledFromGas -= b.steps[i].repeat;
				th.finished[i] = b.steps[i].repeat;
			}
		}

		// Noop? Count as done if it's active
		if (b.steps[i].action.type == AT_Noop) {
			if (BuildState_stepActive(th, b, i, 0, 0, 1)) {
				th.finished[i] = b.steps[i].repeat;
			}
		}

		// Adjust supply offset
		if (b.steps[i].action.type == AT_Build && b.steps[i].repeat > 0) {
			if (unitClass[b.steps[i].action.unit] == UC_MorphDrone) {
				th.supplyOffs += b.steps[i].repeat - th.finished[i];
			}
		}
	}

	// Finalize gas worker count
	th.curFilledGeysirs = state.units[UTA_GasWorker] + pulledFromGas;
	for (i = 0; i < state.heapSize; i += 1) {
		if (state.actions[i].type == AT_GasWorker) {
			th.curFilledGeysirs += 1;
		}
	}

}

void BuildState_copy(REF(BuildState) to, REF(BuildState) from, REF(Build) b)
{
	int i;
	State_copy(to.state, from.state);
	to.curLogPos = from.logPos;
	to.logPos = from.logPos;
	to.curFilledGeysirs = from.filledGeysirs;
	to.filledGeysirs = from.filledGeysirs;
	to.totalGas = from.totalGas;
	to.curTotalGas = from.curTotalGas;
	for (i = 0; i < MAX_AUTO_STEPS + b.stepCount + 1; i+=1) {
		to.finished[i] = from.finished[i];
	}
	for (i = 0; i < UTA_First; i+=1) {
		to.unitCounter[i] = from.unitCounter[i];
		to.unitFinish[i] = from.unitFinish[i];
	}
}

void BuildState_commit(REF(BuildState) th) {
	// Reset state
#ifdef STATE_ROLLBACK
	State_commit(th.state);
#else
	State_copy(th.oldState, th.state);
#endif
	th.logPos = th.curLogPos;
	th.filledGeysirs = th.curFilledGeysirs;
	th.totalGas = th.curTotalGas;
}

void BuildState_rollback(REF(BuildState) th) {
	// Reset state
#ifdef STATE_ROLLBACK
	State_rollback(th.state);
#else
	State_copy(th.state, th.oldState);
#endif
	// Automatic callback actions might have added extra
	// build log entries and extractors. Reset.
	th.curLogPos = th.logPos;
	th.curFilledGeysirs = th.filledGeysirs;
	th.curTotalGas = th.totalGas;
}

#ifndef GALAXY
int BuildState_stepsLeft(REF(BuildState) th, REF(Build) b, int iStep)
{
	if (b.steps[iStep].repeat < 0) {
		return 1;
	}
	return MaxI(b.steps[iStep].repeat - th.finished[iStep],
		        b.steps[iStep].upTo - th.unitCounter[MaxI(0, b.steps[iStep].action.unit)]);
}
#else
#define BuildState_stepsLeft(th, b, iStep) \
	MaxI(-b.steps[iStep].repeat, \
         MaxI(b.steps[iStep].repeat - th.finished[iStep], \
		      b.steps[iStep].upTo - th.unitCounter[MaxI(0, b.steps[iStep].action.unit)]))
#endif

bool BuildState_stepActive(REF(BuildState) th, REF(Build) b, int iStep, int iLookahead, int moreSupplyOffs, int stepsLeft)
{
	REF(Step) step = b.steps[iStep];
	int stepTime = step.time;
	// Own supply offset - this is making things complicated again:
	// If we have a rule like "47 - 3xExtractor", we want it to match 
	// at supply 46 and 45 if it was applied once or twice already
	// respectively.
	int ownSupplyOffs = 0; int mins; int gas;
	if (th.finished[iStep] > 0 && step.action.type == AT_Build && unitClass[step.action.unit] == UC_MorphDrone) {
		ownSupplyOffs -= th.finished[iStep];
	}
	// Check requirements. This check has a rather disorienting number of
	// modifiers on it. A quick overview:
	//  L1) step.supply - the minimum supply coming from the rule
	//  L2) ownSupplyOffs - see above, for repeated rules where every application consumes supply
	//  L3) th.supplyOffs - for rules above us in the build order that consume supply (13 - Extractor | 12 - Spawning Pool)
	//  R1) th.state.supply - the current supply in our state
	//  R3) moreSupplyOffs - supply consumed by the rule we're checking inversion for, as well as all reservations so far
	//        So e.g. for "22 - Queen | 24 - Extractor" the extractor should never be prioritised in front of the queen,
	//        even though after applying the queen rule the extractor rule is applicable instantly.
	if (step.supply > 0 && step.supply + ownSupplyOffs + th.supplyOffs > th.state.supply - moreSupplyOffs) {
#ifdef STEP_ACTIVE_DEBUG
		if (Build_verbosity > 2) {
			StringDump("[" + Action_show(step.action) + " not active, " +
				IntToString(th.state.supply) + " (-" + IntToString(th.resSupply) + "-" + IntToString(moreSupplyOffs) + ") < " +
				IntToString(step.supply) + " (+" + IntToString(ownSupplyOffs) + "+" + IntToString(th.supplyOffs) + ")]");
		}
#endif
		// Mark that we have a rule waiting for supply. This might cause
		// drones to be built automatically.
		th.supplyWaitRules += 1;
		return false;
	}
	// Check tech
	if (step.action.type == AT_Build) {
		// Share variable space
#define tech mins
		tech = unitTech[step.action.unit];
		if (tech != UT_None) {
			if (th.state.units[tech] == 0) {
				// We are asked to build something that we don't have the tech for.
				// Is it at least on the way?
				if (th.unitCounter[tech] > 0) {
					// Then we can use the tech finish time as a new "timer" for
					// when the rule comes online. Note that this is slightly
					// inaccurate, as currently the "finish" time is the *last* time
					// the unit in question gets completed, so if for some reason
					// we make a tech requirement more than once this might be
					// a bit inaccurate.
					stepTime = MaxI(stepTime, th.unitFinish[tech]);
				} else {
					// We assume by default that tech that hasn't even started yet is
					// always beyond the lookahead
#ifdef STEP_ACTIVE_DEBUG
					if (Build_verbosity > 2) {
						StringDump("[" + Action_show(step.action) + " not active, tech " + unitName[tech] + " not available]");
					}
#endif
					return false;
				}
			}
		}
		// The same for second tech
		tech = unitTech2[step.action.unit];
		if (tech != UT_None) {
			if (th.state.units[tech] == 0) {
				if (th.unitCounter[tech] > 0) {
					stepTime = MaxI(stepTime, th.unitFinish[tech]);
				}
				else {
#ifdef STEP_ACTIVE_DEBUG
					if (Build_verbosity > 2) {
						StringDump("[" + Action_show(step.action) + " not active, tech " + unitName[tech] + " does not finish until " +
							FormatTick(th.state.game, th.unitFinish[tech]) + "]");
					}
#endif
					return false;
				}
			}
		}
		#undef tech
	}
	if (step.gas > th.curTotalGas) {
		if (th.state.gasWorkers <= 0) {
#ifdef STEP_ACTIVE_DEBUG
			if (Build_verbosity > 2) {
				StringDump("[" + Action_show(step.action) + " not active, need " + IntToString(step.gas) + " gas]");
			}
#endif
			return false;
		}
		stepTime = MaxI(stepTime, th.state.time + (step.gas - th.curTotalGas) * 32 * 60 / MaxI(RES_MIN_GAS_INCOME, State_gasIncome(th.state)));
	}
	if (stepTime > 0) {
		if (stepTime > th.state.time + iLookahead) {
#ifdef STEP_ACTIVE_DEBUG
			if (Build_verbosity > 2) {
				StringDump("[" + Action_show(step.action) + " not active, " + FormatTick(th.state.game, stepTime) + " beyond lookahead]");
			}
#endif
			return false;
		}
		// Reduce lookahead, taking cost into account
		// (no need to consider making an extractor 1:00 in advance)
		if (step.action.type == AT_Build && iLookahead > 0) {
			mins = stepsLeft * unitMins[step.action.unit];
			gas = stepsLeft * unitGas[step.action.unit];
			if (mins > 0) {
				iLookahead = MinI(iLookahead, TIMED_LOOKAHEAD_FACTOR * mins * 32 * 60 / MaxI(RES_MIN_MIN_INCOME, State_minIncome(th.state)));
			}
			if (gas > 0) {
				iLookahead = MinI(iLookahead, TIMED_LOOKAHEAD_FACTOR * gas * 32 * 60 / MaxI(RES_MIN_GAS_INCOME, State_gasIncome(th.state)));
			}
			if (iLookahead < 0) {
				return false;
			}
		}
		if (stepTime > th.state.time + iLookahead) {
			// Now that we have discarded a rule using a tighter lookahead,
			// we have to make sure that the same low lookahead is going
			// to get used to assess future rules. Otherwise another rule
			// might "overtake" us here!
			th.timeWindow = stepTime - iLookahead;
#ifdef STEP_ACTIVE_DEBUG
			if (Build_verbosity > 2) {
				StringDump("[" + Action_show(step.action) + " not active, " +
					FormatTick(th.state.game, stepTime) +
					" beyond reduced lookahead of " +
					FormatTick(th.state.game, th.timeWindow) + "]");
			}
#endif
			return false;
		}
	}
	return true;
}

bool BuildState_handleSupplyBlock(REF(BuildState) th, REF(ActionLog) log, REF(Action) a)
{
	int ovTime = unitTime[UT_Overlord]; Action aov;
	int blockStart = th.supplyBlockStart; // will get overwritten by BuildState_execAction

	// Okay, we have run into a supply block trying to apply the given action.
	// Now we are trying to find out when we would like to see an overlord.
	// To do this, we have to find out at what time we would apply the action if
	// we *had* in fact built an overlord. Unluckily, this is quite a circular
	// question, therefore we hack our way around it: We temporarily reduce
	// the overlord build time to 0, then simulate building the overlord and
	// then applying the action. Checking that time should give us a pretty good
	// estimate.

	// First, restore start state
	BuildState_rollback(th);

	// Now build our hacked overlord
	unitTime[UT_Overlord] = 0;
	Action_init(aov, AT_Build, UT_Overlord);
	if (BuildState_execAction(th, log, aov, MAX_STEP_LENGTH)) {

		// Retry executing the original action
		if (!BuildState_execAction(th, log, a, MAX_STEP_LENGTH)) {

			// This can absolutely happen - most notably if besides
			// the supply there is another reason we can't execute the
			// action (say, tech). We better ignore the supply block
			// if that's the case.
			unitTime[UT_Overlord] = ovTime;
			th.gotSupplyBlock = false;
			return false;
		}

	}

	// Reset overlord time
	unitTime[UT_Overlord] = ovTime;

	// Set time for overlord. Take care to never add a rule to start an
	// overlord in the "future": That rule would never fire, getting us
	// stuck in an endless loop.
	th.supplyBlockOverlord = MinI(th.supplyBlockOverlord,
		MinI(th.oldState.time, th.state.time - ovTime) - OVERLORD_OFFSET);
	th.supplyBlockStart = blockStart;
	BuildState_rollback(th);
	assert(th.gotSupplyBlock);
	return true;
}

void BuildState_reserve(REF(BuildState) th, int repeat, int mins, int gas, int supply, int larvae) {
	int incGas = 1; int incMin = 1; int incLarvae = 1; int tgas = 0; int tmin = 0; int tlarvae = 0;

	// This is another tricky spot - we want to remove the given
	// amount of resources from our pool, so we can safely check
	// other rules for inversion without them taking our resources.

	// Sounds easy - just remove the resources, right? But there
	// is actually no need to be that conservative. Suppose we
	// have something like follows:
	//
	//   50 - 10 Hydralisk
	//   50 - 10 Drones
	//
	// Now unless David Kim goes absolutely bonkers, the hydras represent
	// signifant gas pressure, so we should have no trouble squeezing in the
	// drones somewhere in the middle. But if we just remove the full mineral
	// cost of the hydras (750) from the pool, we will never get enough
	// un-reserved minerals to build them.
	//
	// So here's what we do: If we derive that we run short of one resource
	// right now (say, gas bank negative) we estimate how much of the other
	// resources we'll get until we get there - and proceed to not remove that
	// cost from the pool. I have a feeling that this is really thin ice we're
	// walking here, but that's what you get for messing with heuristics...

	if (mins == 0 && gas == 0 && supply == 0) { return; }

	mins = mins * repeat;
	gas = gas * repeat;
	supply = supply * repeat;
	larvae = larvae * repeat;

	// Get income
	incGas = MaxI(RES_MIN_GAS_INCOME, State_gasIncome(th.state));
	incLarvae = 4 * MaxI(1, th.state.units[UT_Hatchery]) + 9 * MinI(th.state.units[UT_Queen], th.state.units[UT_Hatchery]) / 2;

	// Determine time equivalent for all resources
	if (mins > th.state.mins) {
		incMin = MaxI(RES_MIN_MIN_INCOME, State_minIncome(th.state));
		tmin = (mins - th.state.mins) * 32 * 60 / incMin;
	}
	if (gas > th.state.gas) {
		tgas = (gas - th.state.gas) * 32 * 60 / incGas;
	}
	if (larvae > th.state.units[UT_Larva]) {
		// Approximate larvae spawn rate:
		// - 4 per minute per hatchery
		// - 4.5 per minute per hatchery/queen pair
		//   (3 / 40 * 60 - assuming LotV)
		tlarvae = (larvae - th.state.units[UT_Larva]) * 32 * 60 / incLarvae;
	}
	
	// Determine what will take the longest to reach
	if (tgas >= tmin) {
		if (tgas >= tlarvae) { // tgas >= { tlarvae, tmin }
			if (tgas > 0) {
				mins = MaxI(0, mins - (tgas - tmin) * incMin / 32 / 60);
				larvae = MaxI(0, larvae - (tgas - tlarvae) * incLarvae / 32 / 60);
			}
		} else { // tlarvae >= { tgas, tmin }
			mins = MaxI(0, mins - (tlarvae - tmin) * incMin / 32 / 60);
			gas = MaxI(0, gas - (tlarvae - tgas) * incGas / 32 / 60);
		}
	} else { // tgas >= tmin
		if (tmin >= tlarvae) { // tmin >= { tlarvae, tgas }
			gas = MaxI(0, gas - (tmin - tgas) * incGas / 32 / 60);
			larvae = MaxI(0, larvae - (tmin - tlarvae) * incLarvae / 32 / 60);
		} else { // tlarvae >= { tgas, tmin }
			mins = MaxI(0, mins - (tlarvae - tmin) * incMin / 32 / 60);
			gas = MaxI(0, gas - (tlarvae - tgas) * incGas / 32 / 60);
		}
	}

	// Never take more away than we have!
	mins = MinI(mins, MaxI(0, th.state.mins));
	gas = MinI(gas, MaxI(0, th.state.gas));
	supply = MinI(supply, MaxI(0, th.state.cap - th.state.supply));
	larvae = MinI(larvae, MaxI(0, th.state.units[UT_Larva]));

	th.resMins += mins; th.state.mins -= mins;
	th.resGas += gas; th.state.gas -= gas;
	th.resSupply += supply; th.state.supply += supply;
	th.resLarva += larvae; th.state.units[UT_Larva] -= larvae;

}

void BuildState_reserveRestore(REF(BuildState) th)
{
	th.state.mins = MaxI(0, th.state.mins - th.resMins);
	th.state.gas = MaxI(0, th.state.gas - th.resGas);
	th.state.supply += th.resSupply;
	th.state.units[UT_Larva] = MaxI(0, th.state.units[UT_Larva] - th.resLarva);
}

void BuildState_reserveClear(REF(BuildState) th)
{
	th.resMins = 0;
	th.resGas = 0;
	th.resSupply = 0;
	th.resLarva = 0;
}

bool BuildState_inversionSupplyBlock(REF(BuildState) th, REF(Action) a, REF(ActionCheckResult) acr, int repeatsProjected);

int BuildState_tryInversion(REF(BuildState) th, REF(ActionLog) log, REF(Build) b, int ruleSupply, int firstStep)
{
	int i; ActionCheck ac; ActionCheckResult acr;
	REFP(Action) pact;
	bool allowInversion = false;
	int repeatsLeft;
	// Now do the check for priority inversion - if we find a rule
	// with lower priority that would fire *instantly*, that one
	// can probably be applied before us *without* delaying us.
	// Note that this might go wrong if we have multiple rules that
	// happen to have their criterion met at the same time.
	for (i = firstStep; i < MAX_AUTO_STEPS + b.stepCount + 1; i+=1) {
		// Any rule repeats left?
		repeatsLeft = BuildState_stepsLeft(th, b, i);
		if (repeatsLeft <= 0) {
			// Special case for last rule: If there is no non-automatic rule
			// active, the drone rule is allowed to priority-inverse. This is
			// so that overlord rules don't block us from building drones in
			// order to get more rules with a supply limit active.
			if (i < MAX_AUTO_STEPS + b.stepCount) { continue; }
			if (th.supplyWaitRules == 0) { continue; }
			repeatsLeft = 1;
		}
		// We even count non-active rules towards supply block margin,
		// as long as they have higher priority. The basic assumption
		// here is that the build order author ought to have known
		// what he/she was doing, so we will not jump to conclusions
		// on supply blocks until we have proven one.
		if (b.steps[i].action.type == AT_Build && i >= MAX_AUTO_STEPS) {
			th.supplyBlockMargin += repeatsLeft * unitCap[b.steps[i].action.unit];
		}
		// Now check whether the rule is actually active
		if (!BuildState_stepActive(th, b, i, th.timeWindow - th.state.time, 0, repeatsLeft)) { continue; }
		// Check condition, but do not actually execute
		SET_REFP(pact, b.steps[i].action);
		ActionCheckResult_init(acr);
		ac = Action_start(GET_REFP(pact), th.state, AM_Check, acr);
		if (ac == AC_Done || ac == AC_Delay) {
			// Special inversion action checks. Note that we still let it test the action
			// so we have reservation and don't spend resources on something further down
			// the line. We just block the action against actually getting executed here.
			allowInversion = true;
			if (!BuildState_stepActive(th, b, i, 0, th.resSupply + MaxI(0, ruleSupply), repeatsLeft)) {
				allowInversion = false;
			} else {
				if (GET_REFP(pact).type == AT_Build) {
					// Don't allow inversion to invalidate supply limit on our primary rule.
					// See documentation for inversionSupplyLimit.
					if (unitClass[GET_REFP(pact).unit] < UC_FirstBuild &&
						th.state.supply - unitSupply[unitClassBuilder[unitClass[GET_REFP(pact).unit]]] < th.inversionSupplyLimit) {

						allowInversion = false;
					}
				}
			}
			if (allowInversion) {
				// Inversion is allowed: Then we should do that one instead right now!
#ifdef BUILD_DEBUG
				if (Build_verbosity > 1) {
					StringDump(" ... overruled by " + Action_show(GET_REFP(pact)) + "!");
					StringDump(" ... state after: " + State_showOverview(th.state, 0) +
						", reserved: " + IntToString(th.resMins) + "m " +
							IntToString(th.resGas) + "g " +
							IntToString(th.resSupply) + "s " +
							IntToString(th.resLarva) + "l ");
				}
#endif
				return i;
			}
		}
		if (ac == AC_MoreCap && GET_REFP(pact).type == AT_Build) {
			if (BuildState_inversionSupplyBlock(th, b.steps[i].action, acr, repeatsLeft)) {
				if (BuildState_handleSupplyBlock(th, log, GET_REFP(pact))) {
					return -1;
				}
			}
		}
		BuildState_reserve(th, repeatsLeft, acr.reqMins, acr.reqGas, acr.reqSupply, acr.reqLarvae);
	}
	// Undo reservation
	return 0;
}

bool BuildState_inversionSupplyBlock(REF(BuildState) th, REF(Action) a, REF(ActionCheckResult) acr, int repeatsProjected)
{
	// Supply block in inversion? This is a bit of a hairy case, but we
	// must take it seriously. After all, we can have something like
	//
	//  55 - Missile Attacks Level 1, Ground Carapace Level 1
	//  55 - 20 Roach
	//
	// And unless we have a supply block check here, we will stop making
	// overlords from the moment we start saving up for the first upgrade.
	// The tricky part is that we don't really know at what point the
	// supply block "started", so we just guess that it's the time the
	// first rule took to get matched.
	//
	// Also note that we have to account for supply block margins.
	// This is for when we skipped a rule that would have given us
	// supply, for example:
	//
	//	  18 - Overlord
	//    18 - Queen
	// 
	// Now if we get at this point with 250 minerals, we will attempt
	// to inverse the order by building the queen first, running
	// into a supply block. However, it is obviously lunacy to insert
	// another overlord: We already have one!

	// First determine the supply need of the action per repeat.
	// If it's zero, we cannot get a supply block from this rule
	// (noteably: morphing banelings, but not morphing ravagers!)
	int supplyNeed = unitSupply[a.unit] - unitSupply[unitClassBuilder[unitClass[a.unit]]];
	if (supplyNeed == 0) {
		return false;
	}

	// Estimate how often we can repeat the rule right away
	if (acr.reqMins > 0) { repeatsProjected = MinI(repeatsProjected, th.state.mins / acr.reqMins); }
	if (acr.reqGas > 0) { repeatsProjected = MinI(repeatsProjected, th.state.gas / acr.reqGas); }
	if (acr.reqLarvae > 0) { repeatsProjected = MinI(repeatsProjected, th.state.units[UT_Larva] / acr.reqLarvae); }

	// Now check for supply block
	if (th.state.supply + repeatsProjected * supplyNeed <= th.state.cap + th.supplyBlockMargin) {
		return false;
	}

#ifdef BUILD_DEBUG
	if (Build_verbosity > 1) {
		StringDump("Inversion supply block: " +
			Action_show(a) + " needs " +
			IntToString(repeatsProjected) + "*" + IntToString(supplyNeed) +
			" supply, have " + IntToString(th.state.cap) + " (+" + IntToString(th.supplyBlockMargin) + ")");
	}
#endif

	// All checks passed, we officially have a supply block in
	// rule inversion!
	th.supplyBlockStart = th.oldState.time; // guessed
	th.gotSupplyBlock = true;
	return true;
}

// Try applying the given step
// -1 = can't match
//  0 = matched
// >0 = matched, but priority-inversion selected the given rule instead
int BuildState_try(REF(BuildState) th, REF(ActionLog) log, REF(Build) b, int iStep)
{
	int i;
	REF(Action) a = b.steps[iStep].action;
	int stepsLeft = BuildState_stepsLeft(th, b, iStep);
	int repeats = 1;
	int unitCounter;
	int ruleSupply = 0;
	int startMins; int startGas; int startSupply; int startLarvae;
	ActionCheckResult acr;
	// Step not active?
	if (!BuildState_stepActive(th, b, iStep, th.timeWindow - th.state.time, 0, stepsLeft)) { return -1; }
	if (a.type == AT_Build) {
		// Active overlord rule? Add some margin to supply blocks
		th.supplyBlockMargin += unitCap[a.unit];
	}
	// Set supply block limit
	th.inversionSupplyLimit = MaxI(th.inversionSupplyLimit, b.steps[iStep].supply);
	// Timed rule? Wait until we are allowed to apply the rule.
	// The above call should have made sure we won't wait too long.
	while (b.steps[iStep].time > th.state.time) {
		BuildState_stepState(th, log, a, b.steps[iStep].time);
	}
	while (b.steps[iStep].gas > th.curTotalGas) {
		if (th.state.gasWorkers <= 0) { return -1; }
		BuildState_stepState(th, log, a, th.state.time + State_timeToGas(th.state, th.state.gas + b.steps[iStep].gas - th.curTotalGas));
	}
	// Apply the rule!
	if (!BuildState_execAction(th, log, a, th.timeWindow - th.state.time)) {
		// Supply capped?
		if (a.type == AT_Build && th.gotSupplyBlock) {
			// Kerrigan is urging us to spawn more overlordssssss
			if (BuildState_handleSupplyBlock(th, log, a)) {
				return -1;
			}
		}
		BuildState_rollback(th);
		return -1;
	}

#ifdef BUILD_DEBUG
	if (Build_verbosity > 1) {
		if (th.finished[iStep] + stepsLeft > 1) {
			StringDump(State_showOverview(th.state, 4) +
					" -- " + Action_show(a) +
					" (" + IntToString(th.finished[iStep] + 1) +
					" of " + IntToString(th.finished[iStep] + stepsLeft) + ")");
		}
		else {
			StringDump(State_showOverview(th.state, 4) +
				" -- " + Action_show(a));
		}
	}
#endif

	// Remove earned resources from reservation
	//th.resMins = MaxI(0, th.resMins - State_minIncomeUntil(th.oldState, th.state.time));
	//th.resGas = MaxI(0, th.resGas - State_gasIncomeUntil(th.oldState, th.state.time));

	// Save resources we had before we started messing around with reservations
	startMins = th.state.mins; startGas = th.state.gas;
	startSupply = th.state.supply; startLarvae = th.state.units[UT_Larva];

	// Restore reservations at this point. This will only do something
	// if this is an inversion, and then we absolutely need to do it
	// before checking repeats (because the inversion only checks the
	// first repeat for safety).
	BuildState_reserveRestore(th);

	// Determine cost for this action
	ActionCheckResult_init(acr);
	Action_start(a, th.state, AM_Check, acr);

	// Approximate number of repeats using the cost. We might find later
	// that we cannot actually execute this number of repeats, but this is
	// what we are going to use for reasoning about inversion.
	repeats = MaxI(1, stepsLeft);
	if (b.steps[iStep].repeat < 0) {
		repeats = 32; // ought to be enough to consider at one time
	}
	if (repeats > 1) {
		if (acr.reqMins > 0) {
			repeats = MinI(repeats, 1 + th.state.mins / acr.reqMins);
		}
		if (acr.reqGas > 0) {
			repeats = MinI(repeats, 1 + th.state.gas / acr.reqGas);
		}
		if (acr.reqSupply > 0) {
			repeats = MinI(repeats, 1 + MaxI(0, th.state.cap - th.state.supply) / acr.reqSupply);
		}
		if (acr.reqLarvae > 0) {
			repeats = MinI(repeats, 1 + th.state.units[UT_Larva] / acr.reqLarvae);
		}
		if (acr.reqUnit != UT_None) {
			repeats = MinI(repeats, 1 + th.state.units[acr.reqUnit]);
		}

#ifdef BUILD_DEBUG
		if (Build_verbosity > 1 && repeats > 1) {
			StringDump("  (repeated x" + IntToString(repeats) + ")");
		}
#endif

		if (b.steps[iStep].repeat >= 0) {

			// Reserve resources for all remaining repeats of this rule..
			BuildState_reserve(th, stepsLeft-1, acr.reqMins, acr.reqGas, acr.reqSupply, acr.reqLarvae);

		} 
		else {

			// For mass rules, just reserve how many repeats we can actually do, but...
			BuildState_reserve(th, repeats-1, acr.reqMins, acr.reqGas, acr.reqSupply, acr.reqLarvae);

			// "Mass" rules are special: We want to reserve some resource so that we do
			// not get priority inversion by other rules that are simply cheaper - e.g.
			// massing roaches might often leave a residual of >50 minerals around, which
			// might get filled with a lone zergling if there's a rule for that down
			// the line. To prevent that, we use the compromise here to reserve *half* of
			// what another unit would cost. Ultimate non-committal "we might want to 
			// build another one of those" kind of thing.
			BuildState_reserve(th, 1, acr.reqMins/2, acr.reqGas/2, acr.reqSupply/2, acr.reqLarvae/2);
		}
	}


	// Check for inversion. We can skip this for the last rule (small optimisation).
	if (iStep < MAX_AUTO_STEPS + b.stepCount) {

		// Reserve the supply freed up by the current step to prevent
		// oscillation - e.g. a rule overruling this one just to find out
		// that undoing this one moved us into a supply block.
		if (a.type == AT_Build) {
			// Calculate supply stat
			ruleSupply = unitSupply[a.unit];
			if (unitClass[a.unit] < UC_FirstBuild) {
				ruleSupply -= unitSupply[unitClassBuilder[unitClass[a.unit]]];
			}
			// Reserve if negative
			if (ruleSupply < 0) {
				BuildState_reserve(th, 1, 0, 0, -ruleSupply, 0);
				th.supplyBlockMargin -= ruleSupply;
			}
		}
		// Now that we have reserved all appropriate resources - try to
		// find a rule that still fires! Note that this will add
		// additional reservations. It's pretty much expected we will
		// have no unreserved resources left in the state after this.
		i = BuildState_tryInversion(th, log, b, ruleSupply, iStep+1);
		// Supply block in a later rule cancels out this one. Oh well.
		if (i < 0) { return -1; }
		// We got overruled! Roll everything back, let the upper loop
		// jump to this new opportunity.
		if (i > 0) {
			// Update reservations: As we are about to reset the
			// state, we ought to reserve the resources spent on
			// the actual action execution above. Note what we should
			// not go through BuildState_reserve here, as these resources
			// have already been consumed. This also means that we know
			// them to be available again after the rollback, so we can
			// just add them to the reservations without further checks.
			th.resMins += acr.reqMins;
			th.resGas += acr.reqGas;
			th.resSupply += acr.reqSupply;
			th.resLarva += acr.reqLarvae;
			// Now rollback. Reservations will remain, so the inversion
			// check for *that* rule will re-apply it using reserveRestore()
			// above. Thus we carry on exactly where we were in terms
			// of reservations, just with a different action actually
			// applied.
			BuildState_rollback(th);
			return i;
		}

	}

	// Okay, now we have decided that this is the rule we are going to
	// apply. We can clear reservations!
	th.state.mins = startMins; th.state.gas = startGas;
	th.state.supply = startSupply; th.state.units[UT_Larva] = startLarvae;
	BuildState_reserveClear(th);

	assert(repeats >= 1);
	for (i = 0; i < repeats; i += 1) {

		// Only the first repeat was actually executed, so add the rest.
		// Note that we do not need a supply block check here, I think.
		if (i > 0) {
			if (!BuildState_execAction(th, log, a, 0)) {
				break;
			}
		}
		// We are building something? Update the stats.
		if (a.type == AT_Build) {
			th.unitCounter[a.unit] += unitMult[a.unit];
			// Special rule for morphing from drones
			if (unitClass[a.unit] == UC_MorphDrone) {
				th.unitCounter[UT_Drone] -= 1;
			}
			// Save time we finish building
			th.unitFinish[a.unit] = th.state.time + unitTime[a.unit];
		}
		// Determine unit counter (if applicable)
		unitCounter = 0;
		if (a.type == AT_Build) { unitCounter = th.unitCounter[a.unit]; }
		// Log it
		th.finished[iStep] += 1;
		Build_addActionLog(th, log, th.state, a.type, a.unit, iStep, repeats, unitCounter);
	}
	return 0;
}

bool BuildState_step(REF(BuildState) th, REF(ActionLog) log, REF(Build) b)
{
	int i = b.lastAutoStep; int iLeft; int ret;
	// Initialize temporaries
	th.timeWindow = th.state.time + MAX_STEP_LENGTH;
	th.supplyOffs = 0;
	th.gotSupplyBlock = false;
	th.supplyBlockOverlord = NEVER;
	BuildState_reserveClear(th);
	// Determine how much additional supply is about to come online
	// (unitCounter includes building units)
	th.supplyBlockMargin = th.unitCounter[UT_Hatchery] * unitCap[UT_Hatchery];
	th.supplyBlockMargin += th.unitCounter[UT_Overlord] * unitCap[UT_Overlord];
	th.supplyBlockMargin -= th.state.cap;
	th.inversionSupplyLimit = 0;
	th.unfinishedRules = 0;
	th.supplyWaitRules = 0;
	// Find a step to apply
	assert(i >= 0);
	while (i < MAX_AUTO_STEPS + b.stepCount + 1) {
		// Early check for finished steps...
		assert(i >= 0);
		iLeft = BuildState_stepsLeft(th, b, i);
		if (iLeft <= 0) {
			// Never skip the last rule if there are other unfished rules left
			// This is most likely because other rules are waiting for supply
			// to get high enough or research to finish - and if the build order
			// doesn't specify, building drones probably is as good an idea as
			// anything.
			if (i < MAX_AUTO_STEPS + b.stepCount || th.unfinishedRules == 0 || th.supplyWaitRules == 0) {
				i += 1;
				continue;
			}
		}
		if (i < MAX_AUTO_STEPS + b.stepCount) {
			th.unfinishedRules += iLeft;
		} else if (th.unfinishedRules == 0) {
			return false;
		}
		// Okay, let's try to do this one!
		ret = BuildState_try(th, log, b, i);
		if (ret == 0) { return true; }
		// Failed morph-from-drone build step: Apply supply offset to later steps
		if (b.steps[i].action.type == AT_Build &&
			unitClass[b.steps[i].action.unit] == UC_MorphDrone) {
			th.supplyOffs += iLeft;
		}
		// Didn't match - select next one
		if (ret > 0) { i = ret; } else { i += 1; }
		// Supply block? Stop trying - even if we have lower-priority rules
		// that can still be applied, it's unlikely that this will improve
		// the situation.
		if (th.gotSupplyBlock) {
			return false;
		}

	}

	return false;
}

int BuildState_targetDroneCount(REF(BuildState) th) {
	// We don't care about build state for gas - it builds so fast we can start
	// making drones right away.
	int gasDrones = DRONES_PER_GAS * th.unitCounter[UT_Extractor];
	// However for hatches, we have to discount building ones until we're close
	// enough to the finishing line. Note that we only know the time the *last*
	// hatch started, so this might underestimate the drone count if there are
	// two hatches building at the same time.
	if (th.state.time + DRONES_HATCH_DELAY >= th.unitFinish[UT_Hatchery]) {
		return gasDrones + DRONES_PER_HATCH * th.unitCounter[UT_Hatchery];
	} else {
		return gasDrones + DRONES_PER_HATCH * th.state.units[UT_Hatchery];
	}
}

bool BuildState_exec(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, int limit)
{
	return BuildState_execC(th, b, log, limit, BuildState_defaultCallback, 0);
}

bool BuildState_exec0(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, Race r, Game g, int limit)
{
	Build_initLog(log);
	BuildState_init(th, b, r, g);
	return BuildState_exec(th, b, log, limit);
}

bool BuildState_execC(REF(BuildState) th, REF(Build) b, REF(ActionLog) log, int limit, FREF(BuildState_defaultCallback) callback, int callbackUser)
{
	ARR(BuildState, UNDO_COUNT, undos);
	int undoPtr = 0;
	int ovTime;
	int startTime = th.state.time;
	int droneRule = MAX_AUTO_STEPS+b.stepCount;
	Action a;
	// Set up fallthru rule: Drone
	b.steps[droneRule].repeat = 0;
	b.steps[droneRule].time = 0;
	b.steps[droneRule].supply = 0;
	b.steps[droneRule].gas = 0;
	Action_init(b.steps[droneRule].action, AT_Build, UT_Drone);
	b.steps[droneRule].upTo = BuildState_targetDroneCount(th);
	th.finished[droneRule] = 0;
	// Initialise state
	th.curLogPos = log.logPos;
	log.game = th.state.game;
	log.race = th.state.race;
	BuildState_commit(th); // copies the above to filledGeysirs/logPos
	// Initial undo step (probably unneeded, mainly safety)
	BuildState_copy(undos[0], th, b);
	// Try stepping until something happens
	while (th.state.supply < 200 && th.state.time < limit) {
		// Commit!
		BuildState_commit(th);
		// Set undo step?
		if (undos[undoPtr].state.time + UNDO_PERIOD <= th.state.time) {
			undoPtr = (undoPtr + 1) % UNDO_COUNT;
			BuildState_copy(undos[undoPtr], th, b);
			// do a callback
			if (!callback(th, b, log, callbackUser)) { return false; }
		}

		// Perform step!
		if (BuildState_step(th, log, b)) {
			// Update drone rule to saturate all bases 
			b.steps[droneRule].upTo = BuildState_targetDroneCount(th);
			continue;
		}

		// Run out of stuff to do?
		if (th.unfinishedRules == 0) {
			return true; // Great, we're done here!
		}

		// Supply block?
		if (th.gotSupplyBlock) {
			// Add the new overlord as a high-priority build order step
			if (b.lastAutoStep == 0) { return false; }
			ovTime = MaxI(startTime, th.supplyBlockOverlord);
			Build_addAutoStep(b, ovTime, AT_Build, UT_Overlord);
			th.finished[b.lastAutoStep] = 0;
#ifdef BUILD_DEBUG
			if (Build_verbosity > 1) {
				StringDump("Supply block at " + FormatTick(th.state.game, th.supplyBlockStart) + ", trying to build overlord at " + FormatTick(th.state.game, ovTime));
			}
#endif
			// Undo to some point before that
			while (ovTime < undos[undoPtr].state.time) {
				undoPtr = (undoPtr + UNDO_COUNT - 1) % UNDO_COUNT;
				ovTime += 1; // Safety
			}
			BuildState_copy(th, undos[undoPtr], b);
#ifdef BUILD_DEBUG
			if (Build_verbosity > 1) {
				StringDump(" == rewound state to " + FormatTick(th.state.game, th.state.time) + " ==");
			}
#endif
			continue;
		}

		if (th.state.time + MAX_IDLE_TIME < th.timeWindow) {
			// Safety check. If we don't have enough mineral income, adding
			// rules will get us nowhere either.
			if (th.state.minWorkers < 6) { return false; }
			// No auto steps left we could add? Abort.
			if (b.lastAutoStep == 0) { return false; }
			// Okay, let's just build more drones, or overlords. Just anything.
			if (th.state.supply >= th.state.cap && th.unitCounter[UT_Overlord] == th.state.units[UT_Overlord]) {
				Build_addAutoStep(b, th.state.time, AT_Build, UT_Overlord);
			} else if (b.steps[b.lastAutoStep].action.type != AT_Build ||
				b.steps[b.lastAutoStep].action.unit != UT_Drone) {
				Build_addAutoStep(b, th.state.time, AT_Build, UT_Drone);
			} else {
				// Can't build anything? Last resort: Wait. This should only happen
				// in extreme border cases where we have so few drones that even building
				// a drone takes forever.
				State_step(th.state, th.state.time+100);
			}
		} else {
			// Seems there is a timed rule that has limited our window to the point
			// where we can't do anything right now. Delay until then...
#ifdef BUILD_DEBUG
			if (Build_verbosity > 1) {
				StringDump(" ... delay " + FormatTick(th.state.game, th.timeWindow - th.state.time) + " for timed rules");
			}
#endif
			Action_init0(a, AT_Build);
			while (th.state.time < th.timeWindow) {
				BuildState_stepState(th, log, a, th.timeWindow);
			}
		}
	}
	return true;
}
