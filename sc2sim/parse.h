
#ifndef PARSE_H
#define PARSE_H

#include "build.h"

// Attempt to read build oders from strings. Given the very
// limited string capabilities we have to work with, we will
// not manage anything intelligent, but it will be a significant
// time saver anyhow.

void Build_staticInitParse(Game g);
int Build_parse(REF(Build) th, string s, Game g);
int Build_simplify(REF(Build) build, Race r, Game g, int iterations, int limit, int override_supply);

#endif // PARSE_H