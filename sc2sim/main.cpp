
#ifdef WINDOWS
#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#endif

#include "build.h"
#include "parse.h"

#include <string>
#include <list>

void runTests();
int scrape();

FILE *log_file = NULL;

extern int Build_verbosity;

int main(int argc, const char *argv[]) {

	// Static initialization
	Build_staticInitParse(G_LotV);

	// "Parse" command line
	int simplify = 0; int override_supply = 0;
	int limit = 60 * 60 * 32;
	for (int i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--help")) {
			std::cout << "Fun command line arguments:" << std::endl << std::endl;
			std::cout << " --limit [time]           Run builds for the given amount of time maximum" << std::endl;
			std::cout << " -v [x], --verbose [x]    Increase overall chattiness" << std::endl;
			std::cout << " -s [i], --simplify [i]   Attempt to simplify the build. Default 10 maximum iterations" << std::endl;
			std::cout << " --override-supply [t]    Let time stamps override supply for simplification. Default one second." << std::endl;
			return 0;
		}
		if (!strcmp(argv[i], "-s") || !strcmp(argv[i], "--simplify")) {
			if (argv[i + 1] && isdigit(*argv[i + 1])) {
				simplify = atoi(argv[i + 1]);
				i++;
			} else {
				simplify = 10;
			}
			continue;
		}
		if (!strcmp(argv[i], "--override-supply")) {
			if (argv[i + 1] && isdigit(*argv[i + 1])) {
				override_supply = atoi(argv[i + 1]);
				i++;
			}
			else {
				override_supply = 32;
			}
			continue;
		}
		if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--verbose")) {
			if (argv[i + 1] && isdigit(*argv[i + 1])) {
				Build_verbosity = atoi(argv[i + 1]);
				i++;
			}
			else {
				Build_verbosity = 1;
			}
			continue;
		}
		std::cerr << " Unknown command line: " << argv[i] << std::endl;
	}

	// Get custom build & state
	std::string line, build, state;
	std::cout << "Hello, this is Hero Core " HERO_VERSION ". Please tell me a build." << std::endl;
	do {
		std::getline(std::cin, line);
		if (build != "") build += " | ";
		build += line;
	} while (line != "");

	// No input? Run test suite
	if (build == "") {
		std::cout << "Fine, I'll pass the time by running the test suite then..." << std::endl << std::endl;

		// Run testsuite
		log_file = fopen("test.log", "w");
		runTests();
		fclose(log_file);
		log_file = NULL;
		return 0;
	}

	// Check if it's a dump from the game
	const string build_tag = "Build = ";
	size_t build_pos = build.find(build_tag);
	if (build_pos != std::string::npos) {
		// Then we *should* find the state in it as well
		const string state_tag = "Start = ";
		size_t state_pos = build.find(state_tag);
		if (state_pos != std::string::npos) {
			state = build.substr(state_pos + state_tag.length(), build.find(";", state_pos) - state_pos - state_tag.length());
		}
		build = build.substr(build_pos + build_tag.length(), build.find(";", build_pos) - build_pos - build_tag.length());
	}
	
	// Load build
	Build b;
	Build_init(b);
	int errors = Build_parse(b, build, G_LotV);
	std::cout << " ... " << b.stepCount << " steps, " << errors << " errors." << std::endl;
	if (b.stepCount == 0) {
		return 1;
	}

	// Get start state
	if (state == "") {
		std::cout << "Set the start state. Default is start state:" << std::endl; std::getline(std::cin, state);
	}

	// Simplify, if requested
	if (simplify) {
		int simps = Build_simplify(b, R_Zerg, G_LotV, simplify, limit, override_supply);
		std::cout << " ... " << simps << " simplifications applied." << std::endl;

		// Dump the build back
		std::cout << "Build after simplification:" << std::endl;
		Build_dump(b, G_LotV);
	}

	BuildState bs; ActionLog al;
	BuildState_init(bs, b, R_Zerg, G_LotV);
	if (state != "") {
		State_readCompact(bs.state, state);
		BuildState_setState(bs, b, bs.state);

		// Run
		Build_initLog(al);
		State_dump(bs.state, 2);
		BuildState_exec(bs, b, al, limit);

		// Dump
		Build_dumpActionLog(al);
		StringDump(" ... end state " + State_showOverview(bs.state, 0));
		//Build_dumpActionLogSc2Planner(al);
	}

	// Run from scratch
	std::cout << std::endl << "From scratch: " << std::endl;
	State_init(bs.state, R_Zerg, G_LotV);
	BuildState_setState(bs, b, bs.state);
	Build_initLog(al);
	BuildState_exec0(bs, b, al, R_Zerg, G_LotV, limit);

	// Dump again
	Build_dumpActionLog(al);
	StringDump(" ... end state " + State_showOverview(bs.state, 0));
	//Build_dumpActionLogSc2Planner(al);

	return 0;
}
