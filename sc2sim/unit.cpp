
#include "unit.h"

ARR(string,UT_Count,unitName);
ARR(string,UT_Count,unitAbbrevs);
ARR(int,UT_Count,unitMins);
ARR(int,UT_Count,unitGas);
ARR(int,UT_Count,unitSupply);
ARR(int,UT_Count,unitCap);
ARR(int,UT_Count,unitClass);
ARR(int,UT_Count,unitTime);
ARR(UnitType,UT_Count,unitTech);
ARR(UnitType,UT_Count,unitTech2);
ARR(int,UT_Count,unitMult);

ARR(UnitType,UC_Count,unitClassBuilder);

void Unit_staticInit(Game g) {
	int i;

	for (i = 0; i < UT_Count; i+=1) {
		unitName[i] = "*** Unnamed ***";
		unitAbbrevs[i] = "";
		unitMins[i] = 0;
		unitGas[i] = 0;
		unitSupply[i] = 0;
		unitCap[i] = 0;
		unitClass[i] = 0;
		unitTime[i] = 0;
		unitTech[i] = UT_None;
		unitTech2[i] = UT_None;
		unitMult[i] = 1;
	}
	for (i = 0; i < UC_Count; i+=1) {
		unitClassBuilder[i] = UT_None;
	}

	unitClassBuilder[UC_MorphLarva] = UT_Larva;
	unitClassBuilder[UC_MorphDrone] = UT_Drone;
	unitClassBuilder[UC_MorphLing] = UT_Zergling;
	unitClassBuilder[UC_MorphOverlord] = UT_Overlord;
	unitClassBuilder[UC_MorphRoach] = UT_Roach;
	unitClassBuilder[UC_MorphHydralisk] = UT_Hydralisk;
	unitClassBuilder[UC_MorphCorruptor] = UT_Corruptor;

	unitName[UT_Hatchery] = "Hatchery";
	unitAbbrevs[UT_Hatchery] = "Hatch,Expand"; // Make expand! Defense not implemented.
	unitMins[UT_Hatchery] = 300; // extra cost for drone travel time
	if (g == G_HotS) {
		unitCap[UT_Hatchery] = 2;
	} else if (g == G_LotV) {
		unitCap[UT_Hatchery] = 6;
	}
	unitClass[UT_Hatchery] = UC_MorphDrone;
	unitTime[UT_Hatchery] = 100 * 32;
	unitClassBuilder[UC_BuildHatchery] = UTA_HatcheryBuild;

	unitName[UT_MacroHatchery] = "Macro Hatchery";
	unitAbbrevs[UT_MacroHatchery] = "Macro Hatch";
	unitMins[UT_MacroHatchery] = 300;
	if (g == G_HotS) {
		unitCap[UT_MacroHatchery] = 2;
	} else if (g == G_LotV) {
		unitCap[UT_MacroHatchery] = 6;
	}
	unitClass[UT_MacroHatchery] = UC_MorphDrone;
	unitTime[UT_MacroHatchery] = 100 * 32;

	unitName[UT_Lair] = "Lair";
	unitMins[UT_Lair] = 150;
	unitGas[UT_Lair] = 100;
	unitClass[UT_Lair] = UC_BuildHatchery;
	unitTime[UT_Lair] = 80 * 32;
	unitTech[UT_Lair] = UT_SpawningPool;

	unitName[UT_Hive] = "Hive";
	unitMins[UT_Hive] = 200;
	unitGas[UT_Hive] = 150;
	unitClass[UT_Hive] = UC_BuildHatchery;
	unitTime[UT_Hive] = 100 * 32;
	unitTech[UT_Hive] = UT_InfestationPit;
	unitTech2[UT_Hive] = UT_Lair;

	unitName[UT_Larva] = "Larva";

	unitName[UT_Extractor] = "Extractor";
	unitMins[UT_Extractor] = 25;
	unitClass[UT_Extractor] = UC_MorphDrone;
	unitTime[UT_Extractor] = 30 * 32;

	unitName[UT_SpawningPool] = "Spawning Pool";
	unitAbbrevs[UT_SpawningPool] = "Pool";
	unitMins[UT_SpawningPool] = 200;
	unitClass[UT_SpawningPool] = UC_MorphDrone;
	unitTime[UT_SpawningPool] = 65 * 32;
	unitClassBuilder[UC_ResearchSpawningPool] = UTA_SpawningPoolResearch;

	unitName[UT_BanelingNest] = "Baneling Nest";
	unitAbbrevs[UT_BanelingNest] = "Bane Nest";
	unitMins[UT_BanelingNest] = 100;
	unitGas[UT_BanelingNest] = 50;
	unitClass[UT_BanelingNest] = UC_MorphDrone;
	unitTime[UT_BanelingNest] = 60 * 32;
	unitClassBuilder[UC_ResearchBanelingNest] = UTA_BanelingNestResearch;

	unitName[UT_RoachWarren] = "Roach Warren";
	unitAbbrevs[UT_RoachWarren] = "Warren";
	unitMins[UT_RoachWarren] = 150;
	unitClass[UT_RoachWarren] = UC_MorphDrone;
	unitTime[UT_RoachWarren] = 55 * 32;
	unitTech[UT_RoachWarren] = UT_SpawningPool;
	unitClassBuilder[UC_ResearchRoachWarren] = UTA_RoachWarrenResearch;

	unitName[UT_EvolutionChamber] = "Evolution Chamber";
	unitAbbrevs[UT_EvolutionChamber] = "Evo Chamber,Evo";
	unitMins[UT_EvolutionChamber] = 75;
	unitClass[UT_EvolutionChamber] = UC_MorphDrone;
	unitTime[UT_EvolutionChamber] = 35 * 32;
	unitClassBuilder[UC_ResearchEvolutionChamber] = UTA_EvolutionChamberResearch;

	unitName[UT_HydraliskDen] = "Hydralisk Den";
	unitAbbrevs[UT_HydraliskDen] = "Hydra Den";
	unitMins[UT_HydraliskDen] = 100;
	unitGas[UT_HydraliskDen] = 100;
	unitClass[UT_HydraliskDen] = UC_MorphDrone;
	unitTime[UT_HydraliskDen] = 40 * 32;
	unitTech[UT_HydraliskDen] = UT_Lair;
	unitClassBuilder[UC_ResearchHydraliskDen] = UTA_HydraliskDenResearch;

	unitName[UT_Spire] = "Spire";
	unitMins[UT_Spire] = 200;
	unitGas[UT_Spire] = 200;
	unitClass[UT_Spire] = UC_MorphDrone;
	unitTime[UT_Spire] = 100 * 32;
	unitTech[UT_Spire] = UT_Lair;
	unitClassBuilder[UC_ResearchSpire] = UTA_SpireResearch;

	unitName[UT_InfestationPit] = "Infestation Pit";
	unitMins[UT_InfestationPit] = 100;
	unitGas[UT_InfestationPit] = 100;
	unitClass[UT_InfestationPit] = UC_MorphDrone;
	unitTime[UT_InfestationPit] = 50 * 32;
	unitTech[UT_InfestationPit] = UT_Lair;
	unitClassBuilder[UC_ResearchInfestationPit] = UTA_InfestationPitResearch;

	unitName[UT_UltraliskCavern] = "Ultralisk Cavern";
	unitAbbrevs[UT_UltraliskCavern] = "Ultra Cavern";
	unitMins[UT_UltraliskCavern] = 150;
	unitGas[UT_UltraliskCavern] = 200;
	unitClass[UT_UltraliskCavern] = UC_MorphDrone;
	unitTime[UT_UltraliskCavern] = 65 * 32;
	unitTech[UT_UltraliskCavern] = UT_Hive;

	unitName[UT_SpineCrawler] = "Spine Crawler";
	unitAbbrevs[UT_SpineCrawler] = "Spine";
	unitMins[UT_SpineCrawler] = 100;
	unitClass[UT_SpineCrawler] = UC_MorphDrone;
	unitTime[UT_SpineCrawler] = 50 * 32;
	unitTech[UT_SpineCrawler] = UT_SpawningPool;
	
	unitName[UT_SporeCrawler] = "Spore Crawler";
	unitAbbrevs[UT_SporeCrawler] = "Spore";
	unitMins[UT_SporeCrawler] = 75;
	unitClass[UT_SporeCrawler] = UC_MorphDrone;
	unitTime[UT_SporeCrawler] = 30 * 32;
	if (g >= G_LotV) {
		unitTech[UT_SporeCrawler] = UT_SpawningPool;
	} else {
		unitTech[UT_SporeCrawler] = UT_EvolutionChamber;
	}
	
	unitName[UT_Overlord] = "Overlord";
	unitAbbrevs[UT_Overlord] = "OL";
	unitMins[UT_Overlord] = 100;
	unitCap[UT_Overlord] = 8;
	unitClass[UT_Overlord] = UC_MorphLarva;
	unitTime[UT_Overlord] = 25 * 32;

	unitName[UT_Overseer] = "Overseer";
	unitMins[UT_Overseer] = 50;
	unitGas[UT_Overseer] = 50;
	unitCap[UT_Overseer] = 8;
	unitClass[UT_Overseer] = UC_MorphOverlord;
	unitTime[UT_Overseer] = 533; // 16.6665 ?!

	unitName[UT_OverlordTransport] = "Overlord Transport"; // Really needs a better name
	unitAbbrevs[UT_OverlordTransport] = "Drop Overlord,TLOL"; // how about this one?
	unitMins[UT_OverlordTransport] = 25;
	unitGas[UT_OverlordTransport] = 25;
	unitCap[UT_OverlordTransport] = 8;
	unitClass[UT_OverlordTransport] = UC_MorphOverlord;
	unitTime[UT_OverlordTransport] = 533; // 16.6665 !?
	unitTech[UT_OverlordTransport] = UT_EvolutionChamber;

	unitName[UT_Drone] = "Drone";
	unitMins[UT_Drone] = 50;
	unitSupply[UT_Drone] = 1;
	unitClass[UT_Drone] = UC_MorphLarva;
	unitTime[UT_Drone] = 17 * 32;

	unitName[UT_Queen] = "Queen";
	unitMins[UT_Queen] = 150;
	unitSupply[UT_Queen] = 2;
	unitClass[UT_Queen] = UC_BuildHatchery;
	unitTime[UT_Queen] = 50 * 32;
	unitTech[UT_Queen] = UT_SpawningPool;

	unitName[UT_Zergling] = "Zergling";
	unitAbbrevs[UT_Zergling] = "Ling,Ling Pair";
	unitMins[UT_Zergling] = 50;
	unitSupply[UT_Zergling] = 1;
	unitClass[UT_Zergling] = UC_MorphLarva;
	unitTime[UT_Zergling] = 24 * 32;
	unitTech[UT_Zergling] = UT_SpawningPool;
	unitMult[UT_Zergling] = 2; // Yep, only reason this exists.

	unitName[UT_Baneling] = "Baneling";
	unitAbbrevs[UT_Baneling] = "Bane";
	unitMins[UT_Baneling] = 25;
	unitGas[UT_Baneling] = 25;
	unitSupply[UT_Baneling] = 1;
	unitClass[UT_Baneling] = UC_MorphLing;
	unitTime[UT_Baneling] = 20 * 32;
	unitTech[UT_Baneling] = UT_BanelingNest;

	unitName[UT_Roach] = "Roach";
	unitMins[UT_Roach] = 75;
	unitGas[UT_Roach] = 25;
	unitSupply[UT_Roach] = 2;
	unitClass[UT_Roach] = UC_MorphLarva;
	unitTime[UT_Roach] = 27 * 32;
	unitTech[UT_Roach] = UT_RoachWarren;

	unitName[UT_Ravager] = "Ravager";
	unitMins[UT_Ravager] = 25;
	unitGas[UT_Ravager] = 75;
	unitSupply[UT_Ravager] = 3;
	unitClass[UT_Ravager] = UC_MorphRoach;
	unitTime[UT_Ravager] = 12 * 32;
	unitTech[UT_Ravager] = UT_RoachWarren;

	unitName[UT_Hydralisk] = "Hydralisk";
	unitAbbrevs[UT_Hydralisk] = "Hydra";
	unitMins[UT_Hydralisk] = 100;
	unitGas[UT_Hydralisk] = 50;
	unitSupply[UT_Hydralisk] = 2;
	unitClass[UT_Hydralisk] = UC_MorphLarva;
	unitTime[UT_Hydralisk] = 33 * 32;
	unitTech[UT_Hydralisk] = UT_HydraliskDen;

	unitName[UT_Lurker] = "Lurker";
	unitMins[UT_Lurker] = 50;
	unitGas[UT_Lurker] = 100;
	unitSupply[UT_Lurker] = 3;
	unitClass[UT_Lurker] = UC_MorphHydralisk;
	unitTime[UT_Lurker] = 25 * 32;
	unitTech[UT_Lurker] = UT_LurkerDen;

	unitName[UT_Mutalisk] = "Mutalisk";
	unitAbbrevs[UT_Mutalisk] = "Muta";
	unitMins[UT_Mutalisk] = 100;
	unitGas[UT_Mutalisk] = 100;
	unitSupply[UT_Mutalisk] = 2;
	unitClass[UT_Mutalisk] = UC_MorphLarva;
	unitTime[UT_Mutalisk] = 33 * 32;
	unitTech[UT_Mutalisk] = UT_Spire;

	unitName[UT_Corruptor] = "Corruptor";
	unitMins[UT_Corruptor] = 150;
	unitGas[UT_Corruptor] = 100;
	unitSupply[UT_Corruptor] = 2;
	unitClass[UT_Corruptor] = UC_MorphLarva;
	unitTime[UT_Corruptor] = 40 * 32;
	unitTech[UT_Corruptor] = UT_Spire;

	unitName[UT_Broodlord] = "Brood Lord";
	unitAbbrevs[UT_Broodlord] = "Broodlord, Brood";
	unitMins[UT_Broodlord] = 150;
	unitGas[UT_Broodlord] = 150;
	unitSupply[UT_Broodlord] = 4;
	unitClass[UT_Broodlord] = UC_MorphCorruptor;
	unitTime[UT_Broodlord] = 1082; // 33.8332 ?!
	unitTech[UT_Broodlord] = UT_GreaterSpire;
	
	unitName[UT_Ultralisk] = "Ultralisk";
	unitAbbrevs[UT_Ultralisk] = "Ultra";
	unitMins[UT_Ultralisk] = 300;
	unitGas[UT_Ultralisk] = 200;
	unitSupply[UT_Ultralisk] = 6;
	unitClass[UT_Ultralisk] = UC_MorphLarva;
	if (g == G_LotV) {
		unitTime[UT_Ultralisk] = 55 * 32;
	} else {
		unitTime[UT_Ultralisk] = 70 * 32;
	}
	unitTech[UT_Ultralisk] = UT_UltraliskCavern;

	unitName[UT_Viper] = "Viper";
	unitMins[UT_Viper] = 100;
	unitGas[UT_Viper] = 200;
	unitSupply[UT_Viper] = 3;
	unitClass[UT_Viper] = UC_MorphLarva;
	unitTime[UT_Viper] = 40 * 32;
	unitTech[UT_Viper] = UT_Hive;

	unitName[UT_Infestor] = "Infestor";
	unitMins[UT_Infestor] = 100;
	unitGas[UT_Infestor] = 150;
	unitSupply[UT_Infestor] = 2;
	unitClass[UT_Infestor] = UC_MorphLarva;
	unitTime[UT_Infestor] = 50 * 32;
	unitTech[UT_Infestor] = UT_InfestationPit;

	unitName[UT_Burrow] = "Burrow";
	unitMins[UT_Burrow] = 100;
	unitGas[UT_Burrow] = 100;
	unitClass[UT_Burrow] = UC_BuildHatchery;
	unitTime[UT_Burrow] = 100 * 32;
	unitTech[UT_Burrow] = UT_SpawningPool;

	unitName[UT_MetabolicBoost] = "Metabolic Boost";
	unitAbbrevs[UT_MetabolicBoost] = "Ling Speed,Speed";
	unitMins[UT_MetabolicBoost] = 100;
	unitGas[UT_MetabolicBoost] = 100;
	unitClass[UT_MetabolicBoost] = UC_ResearchSpawningPool;
	unitTime[UT_MetabolicBoost] = 110 * 32;
	unitTech[UT_MetabolicBoost] = UT_SpawningPool;

	unitName[UT_CentrifugalHooks] = "Centrifugal Hooks";
	unitAbbrevs[UT_CentrifugalHooks] = "Baneling Speed, Bane Speed, Centrifical Hooks";
	unitMins[UT_CentrifugalHooks] = 150;
	unitGas[UT_CentrifugalHooks] = 150;
	unitClass[UT_CentrifugalHooks] = UC_ResearchBanelingNest;
	unitTime[UT_CentrifugalHooks] = 110 * 32;
	unitTech[UT_CentrifugalHooks] = UT_Lair;

	unitName[UT_Melee1] = "Melee Attacks Level 1";
	unitAbbrevs[UT_Melee1] = "Melee Attacks 1, +1 melee, +1 attack, Zerg Melee Weapons Level 1";
	unitMins[UT_Melee1] = 100;
	unitGas[UT_Melee1] = 100;
	unitClass[UT_Melee1] = UC_ResearchEvolutionChamber;
	unitTime[UT_Melee1] = 160 * 32;

	unitName[UT_Melee2] = "Melee Attacks Level 2";
	unitAbbrevs[UT_Melee2] = "Melee Attacks 2, +2 melee, +2 attack, Zerg Melee Weapons Level 2";
	unitMins[UT_Melee2] = 150;
	unitGas[UT_Melee2] = 150;
	unitClass[UT_Melee2] = UC_ResearchEvolutionChamber;
	unitTime[UT_Melee2] = 190 * 32;
	unitTech[UT_Melee2] = UT_Lair;
	unitTech2[UT_Melee2] = UT_Melee1;

	unitName[UT_Melee3] = "Melee Attacks Level 3";
	unitAbbrevs[UT_Melee3] = "Melee Attacks 3, +3 melee, +3 attack, Zerg Melee Weapons Level 3";
	unitMins[UT_Melee3] = 200;
	unitGas[UT_Melee3] = 200;
	unitClass[UT_Melee3] = UC_ResearchEvolutionChamber;
	unitTime[UT_Melee3] = 220 * 32;
	unitTech[UT_Melee3] = UT_Hive;
	unitTech2[UT_Melee3] = UT_Melee2;

	unitName[UT_Missile1] = "Missile Attacks Level 1";
	unitAbbrevs[UT_Missile1] = "Missile Attacks 1, +1 missile, +1 ranged, Zerg Missile Weapons Level 1";
	unitMins[UT_Missile1] = 100;
	unitGas[UT_Missile1] = 100;
	unitClass[UT_Missile1] = UC_ResearchEvolutionChamber;
	unitTime[UT_Missile1] = 160 * 32;

	unitName[UT_Missile2] = "Missile Attacks Level 2";
	unitAbbrevs[UT_Missile2] = "Missile Attacks 2, +2 missile, +2 ranged, Zerg Missile Weapons Level 2";
	unitMins[UT_Missile2] = 150;
	unitGas[UT_Missile2] = 150;
	unitClass[UT_Missile2] = UC_ResearchEvolutionChamber;
	unitTime[UT_Missile2] = 190 * 32;
	unitTech[UT_Missile2] = UT_Lair;
	unitTech2[UT_Missile2] = UT_Missile1;

	unitName[UT_Missile3] = "Missile Attacks Level 3";
	unitAbbrevs[UT_Missile3] = "Missile Attacks 3, +3 missile, +3 ranged, Zerg Missile Weapons Level 3";
	unitMins[UT_Missile3] = 200;
	unitGas[UT_Missile3] = 200;
	unitClass[UT_Missile3] = UC_ResearchEvolutionChamber;
	unitTime[UT_Missile3] = 220 * 32;
	unitTech[UT_Missile3] = UT_Hive;
	unitTech2[UT_Missile3] = UT_Missile2;

	unitName[UT_Carapace1] = "Ground Carapace Level 1";
	unitAbbrevs[UT_Carapace1] = "Ground Carapace 1, +1 carapace, +1 armor,  Zerg Ground Armor Level 1";
	unitMins[UT_Carapace1] = 150;
	unitGas[UT_Carapace1] = 150;
	unitClass[UT_Carapace1] = UC_ResearchEvolutionChamber;
	unitTime[UT_Carapace1] = 160 * 32;

	unitName[UT_Carapace2] = "Ground Carapace Level 2";
	unitAbbrevs[UT_Carapace2] = "Ground Carapace 2, +2 carapace, +2 armor,  Zerg Ground Armor Level 2";
	unitMins[UT_Carapace2] = 225;
	unitGas[UT_Carapace2] = 225;
	unitClass[UT_Carapace2] = UC_ResearchEvolutionChamber;
	unitTime[UT_Carapace2] = 190 * 32;
	unitTech[UT_Carapace2] = UT_Lair;
	unitTech[UT_Carapace2] = UT_Carapace1;

	unitName[UT_Carapace3] = "Ground Carapace Level 3";
	unitAbbrevs[UT_Carapace3] = "Ground Carapace 3, +3 carapace, +3 armor,  Zerg Ground Armor Level 3";
	unitMins[UT_Carapace3] = 300;
	unitGas[UT_Carapace3] = 300;
	unitClass[UT_Carapace3] = UC_ResearchEvolutionChamber;
	unitTime[UT_Carapace3] = 220 * 32;
	unitTech[UT_Carapace3] = UT_Hive;
	unitTech2[UT_Carapace3] = UT_Carapace2;

	unitName[UT_GlialReconstitution] = "Glial Reconstitution";
	unitAbbrevs[UT_GlialReconstitution] = "Roach Speed";
	unitMins[UT_GlialReconstitution] = 100;
	unitGas[UT_GlialReconstitution] = 100;
	unitClass[UT_GlialReconstitution] = UC_ResearchRoachWarren;
	unitTime[UT_GlialReconstitution] = 110 * 32;
	unitTech[UT_GlialReconstitution] = UT_Lair;

	unitName[UT_TunnelingClaws] = "Tunneling Claws";
	unitAbbrevs[UT_TunnelingClaws] = "Roach Burrow";
	unitMins[UT_TunnelingClaws] = 100;
	unitGas[UT_TunnelingClaws] = 100;
	unitClass[UT_TunnelingClaws] = UC_ResearchRoachWarren;
	unitTime[UT_TunnelingClaws] = 110 * 32;
	unitTech[UT_TunnelingClaws] = UT_Lair;

	unitName[UT_GroovedSpines] = "Grooved Spines";
	unitAbbrevs[UT_GroovedSpines] = "Hydralisk Range, Hydra Range";
	unitMins[UT_GroovedSpines] = 150;
	unitGas[UT_GroovedSpines] = 150;
	unitClass[UT_GroovedSpines] = UC_ResearchHydraliskDen;
	unitTime[UT_GroovedSpines] = 80 * 32;
	unitTech[UT_GroovedSpines] = UT_HydraliskDen;

	unitName[UT_MuscularAugments] = "Muscular Augments";
	unitAbbrevs[UT_MuscularAugments] = "Hydralisk Speed, Hydra Speed";
	unitMins[UT_MuscularAugments] = 150;
	unitGas[UT_MuscularAugments] = 150;
	unitClass[UT_MuscularAugments] = UC_ResearchHydraliskDen;
	unitTime[UT_MuscularAugments] = 100 * 32;
	unitTech[UT_MuscularAugments] = UT_HydraliskDen;

	unitName[UT_AdrenalGlands] = "Adrenal Glands";
	unitAbbrevs[UT_AdrenalGlands] = "Cracklings";
	unitMins[UT_AdrenalGlands] = 200;
	unitGas[UT_AdrenalGlands] = 200;
	unitClass[UT_AdrenalGlands] = UC_ResearchSpawningPool;
	unitTime[UT_AdrenalGlands] = 130 * 32;
	unitTech[UT_AdrenalGlands] = UT_Hive;

	unitName[UT_PathogenGlands] = "Pathogen Glands";
	unitMins[UT_PathogenGlands] = 150;
	unitGas[UT_PathogenGlands] = 150;
	unitClass[UT_PathogenGlands] = UC_ResearchInfestationPit;
	unitTime[UT_PathogenGlands] = 80 * 32;
	unitTech[UT_PathogenGlands] = UT_InfestationPit;

	unitName[UT_PneumatizedCarapace] = "Pneumatized Carapace";
	unitAbbrevs[UT_PneumatizedCarapace] = "Overlord Speed, OL Speed";
	unitMins[UT_PneumatizedCarapace] = 100;
	unitGas[UT_PneumatizedCarapace] = 100;
	unitClass[UT_PneumatizedCarapace] = UC_BuildHatchery;
	unitTime[UT_PneumatizedCarapace] = 60 * 32;
	if (g == G_LotV) {
		unitTech[UT_PneumatizedCarapace] = UT_SpawningPool;
	} else {
		unitTech[UT_PneumatizedCarapace] = UT_Lair;
	}

	unitName[UT_VentralSacs] = "Ventral Sacs";
	unitAbbrevs[UT_VentralSacs] = "Overlord Drop, OL Drop";
	unitMins[UT_VentralSacs] = 200;
	unitGas[UT_VentralSacs] = 200;
	unitClass[UT_VentralSacs] = UC_BuildHatchery;
	unitTime[UT_VentralSacs] = 130 * 32;
	unitTech[UT_VentralSacs] = UT_Lair;

	unitName[UT_FlyerAttack1] = "Flyer Attacks Level 1";
	unitAbbrevs[UT_FlyerAttack1] = "Flyer Attacks 1, +1 flyer attack, +1 air attack, Zerg Flyer Weapons Level 1";
	unitMins[UT_FlyerAttack1] = 100;
	unitGas[UT_FlyerAttack1] = 100;
	unitClass[UT_FlyerAttack1] = UC_ResearchSpire;
	unitTime[UT_FlyerAttack1] = 160 * 32;
	unitTech[UT_FlyerAttack1] = UT_Spire;

	unitName[UT_FlyerAttack2] = "Flyer Attacks Level 2";
	unitAbbrevs[UT_FlyerAttack2] = "Flyer Attacks 2, +2 flyer attack, +2 air attack, Zerg Flyer Weapons Level 2";
	unitMins[UT_FlyerAttack2] = 175;
	unitGas[UT_FlyerAttack2] = 175;
	unitClass[UT_FlyerAttack2] = UC_ResearchSpire;
	unitTime[UT_FlyerAttack2] = 190 * 32;
	unitTech[UT_FlyerAttack2] = UT_FlyerAttack1;

	unitName[UT_FlyerAttack3] = "Flyer Attacks Level 3";
	unitAbbrevs[UT_FlyerAttack3] = "Flyer Attacks 3, +3 flyer attack, +3 air attack, Zerg Flyer Weapons Level 3";
	unitMins[UT_FlyerAttack3] = 250;
	unitGas[UT_FlyerAttack3] = 250;
	unitClass[UT_FlyerAttack3] = UC_ResearchSpire;
	unitTime[UT_FlyerAttack3] = 220 * 32;
	unitTech[UT_FlyerAttack3] = UT_FlyerAttack2;
	unitTech2[UT_FlyerAttack3] = UT_Hive;

	unitName[UT_FlyerCarapace1] = "Flyer Carapace Level 1";
	unitAbbrevs[UT_FlyerCarapace1] = "Flyer Carapace 1, +1 flyer Carapace, +1 air armor";
	unitMins[UT_FlyerCarapace1] = 150;
	unitGas[UT_FlyerCarapace1] = 150;
	unitClass[UT_FlyerCarapace1] = UC_ResearchSpire;
	unitTime[UT_FlyerCarapace1] = 160 * 32;
	unitTech[UT_FlyerCarapace1] = UT_Spire;

	unitName[UT_FlyerCarapace2] = "Flyer Carapace Level 2";
	unitAbbrevs[UT_FlyerCarapace2] = "Flyer Carapace 2, +2 flyer Carapace, +2 air armor";
	unitMins[UT_FlyerCarapace2] = 225;
	unitGas[UT_FlyerCarapace2] = 225;
	unitClass[UT_FlyerCarapace2] = UC_ResearchSpire;
	unitTime[UT_FlyerCarapace2] = 190 * 32;
	unitTech[UT_FlyerCarapace2] = UT_FlyerCarapace1;

	unitName[UT_FlyerCarapace3] = "Flyer Carapace Level 3";
	unitAbbrevs[UT_FlyerCarapace3] = "Flyer Carapace 3, +3 flyer Carapace, +3 air armor";
	unitMins[UT_FlyerCarapace3] = 300;
	unitGas[UT_FlyerCarapace3] = 300;
	unitClass[UT_FlyerCarapace3] = UC_ResearchSpire;
	unitTime[UT_FlyerCarapace3] = 220 * 32;
	unitTech[UT_FlyerCarapace3] = UT_FlyerCarapace2;
	unitTech2[UT_FlyerCarapace3] = UT_Hive;

	unitName[UT_LurkerDen] = "Lurker Den";
	unitMins[UT_LurkerDen] = 150;
	unitGas[UT_LurkerDen] = 150;
	unitClass[UT_LurkerDen] = UC_ResearchHydraliskDen;
	unitTime[UT_LurkerDen] = 3840;

	unitName[UT_GreaterSpire] = "Greater Spire";
	unitMins[UT_GreaterSpire] = 100;
	unitGas[UT_GreaterSpire] = 150;
	unitClass[UT_GreaterSpire] = UC_ResearchSpire;
	unitTime[UT_GreaterSpire] = 100 * 32;
	unitTech[UT_GreaterSpire] = UT_Hive;

	// Pseudo units - names for debugging
	unitName[UTA_Patch] = "Mineral Patches";
	unitName[UTA_Geysir] = "Geysirs";
	unitName[UTA_Gas] = "Gasses";
	unitName[UTA_MinWorker] = "Mineral Workers";
	unitName[UTA_GasWorker] = "Gas Workers";
	unitName[UTA_HatcheryBuild] = "Idle Hatcheries";
	unitName[UTA_HatcheryInject] = "Injectable Hatcheries";
	unitName[UTA_QueenEnergy] = "Queen Casts Available";
	unitName[UTA_SpawningPoolResearch] = "Idle Spawning Pool";
	unitName[UTA_BanelingNestResearch] = "Idle Baneling Nest";
	unitName[UTA_EvolutionChamberResearch] = "Idle Evolution Chambers";
	unitName[UTA_RoachWarrenResearch] = "Idle Roach Warren";
	unitName[UTA_HydraliskDenResearch] = "Idle Hydralisk Den";
	unitName[UTA_SpireResearch] = "Idle Spire";

}
