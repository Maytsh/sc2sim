
SIM_SRCS = \
  sc2sim/action.cpp sc2sim/build.cpp sc2sim/main.cpp \
  sc2sim/parse.cpp sc2sim/state.cpp sc2sim/unit.cpp \
  sc2sim/tests.cpp

CXXFLAGS += --std=c++0x -DUNIX

sim : $(SIM_SRCS:cpp=o)
	$(CXX) -o $@ $^ -lrt

strip : galaxy_postprocess/main.o
	$(CXX) -o $@ $^

out.galaxy : strip $(SIM_SRCS) sc2sim/all.cpp
	$(CPP) -o temp.galaxy sc2sim/all.cpp -DGALAXY -P
	./strip temp.galaxy out.galaxy
	rm temp.galaxy

clean :
	rm sim strip out.galaxy
	rm $(SIM_SRCS:cpp=o)
