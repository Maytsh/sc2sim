<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Desc>
    <Frame type="Label" name="ModeLabel">
        <Style val="@Arcade_Subtitle"/>
        <Text val="@UI/BattleMapProfilePanel/Mode"/>
        <CollapseLayout val="true"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="Label" name="CategoryTitleTemplate">
        <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
        <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
        <Style val="@@HelpControlsPanelSection"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="Label" name="CategoryTextTemplate">
        <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
        <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
        <Style val="@@HelpControlsPanelText"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="Frame" name="Padding">
        <Anchor side="Left" relative="$parent" pos="Min" offset="0"/>
        <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="Label" name="ControlTitleTemplate">
        <Anchor side="Left" relative="$parent" pos="Min" offset="10"/>
        <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
        <Style val="@@HelpControlsPanelHeader"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="Label" name="ControlTextTemplate">
        <Anchor side="Left" relative="$parent" pos="Min" offset="10"/>
        <Anchor side="Right" relative="$parent" pos="Max" offset="0"/>
        <Style val="@@HelpControlsPanelText"/>
        <AcceptsMouse val="false"/>
    </Frame>

    <Frame type="TextListBoxItem" name="HeroListItem">
        <AcceptsMouse val="false"/>

        <Frame type="Label" name="label0" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent" pos="Min" offset="10"/>
            <Text val="This mod is for having fun with macro - develop your economy and try to reach a set goal as fast as possible. Build StarCraft &quot;muscle memory&quot; so you can focus more on other parts of the game!"/>
        </Frame>

        <Frame type="Label" name="label1" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label0" pos="Max" offset="10"/>
            <Text val="Saving &amp; Loading"/>
        </Frame>

        <Frame type="Label" name="label2" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label1" pos="Max" offset="10"/>
            <Text val="This mod saves automatically every few seconds, allowing you to jump back. For example &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l 0&lt;/s&gt; will start again from the beginning, and &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l -60&lt;/s&gt; will move back one minute. You can soft-pause the game with &lt;s val=&quot;MonospaceTemplate&quot;&gt;-p&lt;/s&gt;."/>
        </Frame>

        <Frame type="Label" name="label3" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label2" pos="Max" offset="10"/>
            <Text val="This uses a modified version of the SALT mod by Turtles. Read on below for more information."/>
        </Frame>

        <Frame type="Label" name="label4" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label3" pos="Max" offset="10"/>
            <Text val="Build Order Simulation (Zerg only)"/>
        </Frame>

        <Frame type="Label" name="label5" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label4" pos="Max" offset="10"/>
            <Text val="To evaluate and guide your play, Macro Hero can simulate how a build order would play out in an ideal world. This means you can test build order ideas in an isolated space, but also use it as a blueprint for your own play."/>
        </Frame>

        <Frame type="Label" name="label6" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label5" pos="Max" offset="10"/>
            <Text val="All you need is a build as text, which Macro Hero parses and simulates. The heuristic will execute the build order realistically, for example automatically building overlords or drones and saving up resources when it makes sense."/>
        </Frame>

        <Frame type="Label" name="label7" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label6" pos="Max" offset="10"/>
            <Text val="Build Order Guide (Zerg only)"/>
        </Frame>

        <Frame type="Label" name="label8" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label7" pos="Max" offset="10"/>
            <Text val="This mod tracks your current game state and can predict what your next actions should be. The interface is inspired by Guitar Hero[tm]: The actions to be performed will be listed in the order that you have to perform them, spaced according to the approximate time you have left."/>
        </Frame>

        <Frame type="Label" name="label9" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label8" pos="Max" offset="10"/>
            <Text val="Furthermore, this mode will monitor your performance by running the build order simulation to completion every few seconds. The projected finish time will be shown so you always know how well you are doing."/>
        </Frame>

        <Frame type="Label" name="label10" template="HeroHelpPanel/CategoryTitleTemplate">
            <Anchor side="Top" relative="$parent/label9" pos="Max" offset="10"/>
            <Text val="Command Reference"/>
        </Frame>

        <Frame type="Label" name="label11" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label10" pos="Max" offset="10"/>
            <Text val="Outside of this shiny user interface, this mod is controlled using chat commands. Here is a list:"/>
        </Frame>

        <Frame type="Label" name="label12" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label11" pos="Max" offset="0"/>
            <Text val="&lt;ul indent=&quot;2&quot; text=&quot;•&quot;&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-g&lt;/s&gt;: Start guided play&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-h&lt;/s&gt;: Shows a helpful wall of text&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-l[time]&lt;/s&gt;: Load the save closest to the given time. So &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l0&lt;/s&gt; starts over, or &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l5:00&lt;/s&gt; takes you to the state 5 minutes in.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-l[+-][offset]&lt;/s&gt;: If you add &lt;s val=&quot;MonospaceTemplate&quot;&gt;+&lt;/s&gt; or &lt;s val=&quot;MonospaceTemplate&quot;&gt;-&lt;/s&gt; in front, the time is relative to the current time. &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l-60&lt;/s&gt; takes you back one minute, and &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l+60&lt;/s&gt; jumps forward if possible.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-l[name]&lt;/s&gt;: Loads a named save&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-o&lt;/s&gt;: Removes all UI elements&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-p&lt;/s&gt;: Pause or resume the game&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-s[name]&lt;/s&gt;: Makes a named save. In contrast to auto-saves, these will not get deleted.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;-w[seconds]&lt;/s&gt;: Time window to show. Use shorter windows to increase difficulty.&lt;/li&gt;&lt;n/&gt;&lt;/ul&gt;"/>
        </Frame>

        <Frame type="Label" name="label13" template="HeroHelpPanel/CategoryTitleTemplate">
            <Anchor side="Top" relative="$parent/label12" pos="Max" offset="10"/>
            <Text val="Saving &amp; Loading"/>
        </Frame>

        <Frame type="Label" name="label14" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label13" pos="Max" offset="10"/>
            <Text val="Game loading in Macro Hero is primarily based on automatic saving: The game will create a snapshot every few seconds, which you can load."/>
        </Frame>

        <Frame type="Label" name="label15" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label14" pos="Max" offset="10"/>
            <Text val="Storage capacity is limited, so auto saves will be incrementally thinned out as we run into said limits. After jumping backwards, this mechanism will start cleaning up all game states you jumped past, so &quot;future&quot; snapshots degrade even more quickly."/>
        </Frame>

        <Frame type="Label" name="label16" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label15" pos="Max" offset="10"/>
            <Text val="If you want to ensure that some save does not get discarded, name it using &lt;s val=&quot;MonospaceTemplate&quot;&gt;-s [name]&lt;/s&gt;. Note that the start state never gets discarded, so &lt;s val=&quot;MonospaceTemplate&quot;&gt;-l 0&lt;/s&gt; is always guaranteed to work."/>
        </Frame>

        <Frame type="Label" name="label17" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label16" pos="Max" offset="10"/>
            <Text val="Restrictions"/>
        </Frame>

        <Frame type="Label" name="label18" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label17" pos="Max" offset="10"/>
            <Text val="Saving and loading the game sounds like a trivial matter, but is surprisingly tricky to get right. The engine is rather... selective in what it shares with mods. Furtunately though, we stand on the shoulders of Turtles, who figured out workarounds for most of these problems."/>
        </Frame>

        <Frame type="Label" name="label19" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label18" pos="Max" offset="10"/>
            <Text val="Honorable mention goes to &lt;s val=&quot;ModLeftSize16Bold&quot;&gt;control groups&lt;/s&gt;: Ironically, mods can set them, but not read them. This is why we might ask you to cycle through your control groups at the start: We need to figure out what keys change control groups so we can &quot;infer&quot; what their current state is."/>
        </Frame>

        <Frame type="Label" name="label20" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label19" pos="Max" offset="10"/>
            <Text val="Apart from that, there are still a few things that don&apos;t update correctly when loading - for example camera hotkeys or rallies to objects in the fog of war. Apologies for the inconvenience."/>
        </Frame>

        <Frame type="Label" name="label21" template="HeroHelpPanel/CategoryTitleTemplate">
            <Anchor side="Top" relative="$parent/label20" pos="Max" offset="10"/>
            <Text val="Build Order Simulation"/>
        </Frame>

        <Frame type="Label" name="label22" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label21" pos="Max" offset="10"/>
            <Text val="Format"/>
        </Frame>

        <Frame type="Label" name="label23" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label22" pos="Max" offset="10"/>
            <Text val="The build order format should look familiar:"/>
        </Frame>

        <Frame type="Label" name="label24" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label23" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;MonospaceTemplate&quot;&gt;    13 - Overlord&lt;n/&gt;    17 - Hatchery&lt;n/&gt;    19 - Hatchery&lt;n/&gt;    18 - Spawning Pool&lt;n/&gt;    23 - 2 Queen, 2 Zergling&lt;n/&gt;    30 - 1 Queen&lt;n/&gt;    2:50 - 3 Extractor&lt;n/&gt;    3:15 - Evolution Chamber, Roach Warren&lt;n/&gt;    @100 gas - Lair&lt;n/&gt;    @100 gas - Missile Attacks Level 1&lt;n/&gt;    @100 gas - Metabolic Boost&lt;n/&gt;    @100 gas - Roach Speed&lt;n/&gt;    67 - Mass Roach, Mass Zergling&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="label25" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label24" pos="Max" offset="10"/>
            <Text val="Every line defines one or multiple &lt;s val=&quot;ModLeftSize16Bold&quot;&gt;rules&lt;/s&gt;, each with &lt;s val=&quot;ModLeftSize16Bold&quot;&gt;requirements&lt;/s&gt; and an &lt;s val=&quot;ModLeftSize16Bold&quot;&gt;action&lt;/s&gt;."/>
        </Frame>

        <Frame type="Label" name="label26" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label25" pos="Max" offset="10"/>
            <Text val="Requirements"/>
        </Frame>

        <Frame type="Label" name="label27" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label26" pos="Max" offset="10"/>
            <Text val="There are three types of requirements:"/>
        </Frame>

        <Frame type="Label" name="label28" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label27" pos="Max" offset="0"/>
            <Text val="&lt;ul indent=&quot;2&quot; text=&quot;•&quot;&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;[mm]:[ss]&lt;/s&gt; - Rule gets activated once time is reached.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;[supply]&lt;/s&gt; - Rule is active once supply is reached. If a previous rule frees supply, the supply limit is increased until that rule gets applied. So for example &lt;s val=&quot;MonospaceTemplate&quot;&gt;13 - Extractor | 12 - Spawning Pool&lt;/s&gt; allows for the pool to be built at 13 supply if the extractor isn&apos;t started yet.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;@[gas] gas&lt;/s&gt; or &lt;s val=&quot;MonospaceTemplate&quot;&gt;@[gas]g&lt;/s&gt; - Rule is active once gas has been earned. Does not count gas reserved for previous rules. So &lt;s val=&quot;MonospaceTemplate&quot;&gt;16 - Metabolic Boost, @50gas - Baneling Nest&lt;/s&gt; means that the baneling nest rule will only fire if a total of 150 gas have been mined.&lt;/li&gt;&lt;n/&gt;&lt;/ul&gt;"/>
        </Frame>

        <Frame type="Label" name="label29" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label28" pos="Max" offset="10"/>
            <Text val="You can combine them if you want."/>
        </Frame>

        <Frame type="Label" name="label30" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label29" pos="Max" offset="10"/>
            <Text val="Actions"/>
        </Frame>

        <Frame type="Label" name="label31" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label30" pos="Max" offset="10"/>
            <Text val="There are a bunch of different actions we can simulate:"/>
        </Frame>

        <Frame type="Label" name="label32" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label31" pos="Max" offset="0"/>
            <Text val="&lt;ul indent=&quot;2&quot; text=&quot;•&quot;&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;Build [n] [unit]&lt;/s&gt; or &lt;s val=&quot;MonospaceTemplate&quot;&gt;Morph [n] [unit]&lt;/s&gt; or just &lt;s val=&quot;MonospaceTemplate&quot;&gt;[n] [unit]&lt;/s&gt; - makes this number of units.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;[unit] to [n]&lt;/s&gt; -- make unit until we reach a total count. For example &lt;s val=&quot;MonospaceTemplate&quot;&gt;Drone to 80&lt;/s&gt;.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;Mass [unit]&lt;/s&gt; -- make unit until supply limit is reached.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;Research [tech]&lt;/s&gt; or just &lt;s val=&quot;MonospaceTemplate&quot;&gt;[tech]&lt;/s&gt; - starts the given research.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;[n] mineral worker&lt;/s&gt; or &lt;s val=&quot;MonospaceTemplate&quot;&gt;[n] mineral drone&lt;/s&gt; - move said number of workers to minerals. For robustness, these rules should often have an &lt;s val=&quot;MonospaceTemplate&quot;&gt;@[gas] g&lt;/s&gt; requirement.&lt;/li&gt;&lt;li&gt;&lt;s val=&quot;MonospaceTemplate&quot;&gt;[n] gas worker&lt;/s&gt; or &lt;s val=&quot;MonospaceTemplate&quot;&gt;[n] gas drone&lt;/s&gt; - undo mineral workers. Note that extractors are automatically filled, so this is only makes sense after &quot;mineral worker&quot;.&lt;/li&gt;&lt;n/&gt;&lt;/ul&gt;"/>
        </Frame>

        <Frame type="Label" name="label33" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label32" pos="Max" offset="10"/>
            <Text val="Note that we often use unit / research names, which can be hard to remember. In fact, we support a few more or less common abbreviations. Here is a handy list:"/>
        </Frame>

        <Frame type="Label" name="label34" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label33" pos="Max" offset="0"/>
            <Text val="&lt;ul indent=&quot;2&quot; text=&quot;•&quot;&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-hatchery.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Hatchery, Hatch, Expand&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-lair.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Lair&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-hive.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Hive&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-hatchery.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Macro Hatchery, Macro Hatch&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-extractor.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Extractor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-spawningpool.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Spawning Pool, Pool&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-evolutionchamber.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Evolution Chamber, Evo Chamber, Evo&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-banelingnest.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Baneling Nest, Bane Nest&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-roachwarren.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Roach Warren, Warren&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-hydraliskden.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Hydralisk Den, Hydra Den&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-lurkerden.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Lurker Den&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-infestationpit.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Infestation Pit&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-spire.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Spire&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-greaterspire.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Greater Spire&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-building-zerg-ultraliskcavern.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ultralisk Cavern, Ultra Cavern&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-overlord.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Overlord, OL&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-overlordtransport.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Overlord Transport, TLOL&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-overseer.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Overseer&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-drone.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Drone&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-queen.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Queen&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-zergling.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Zergling, Ling&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-baneling.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Baneling, Bane&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-roach.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Roach&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-ravager.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ravager&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-hydralisk.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Hydralisk, Hydra&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-lurker.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Lurker&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-mutalisk.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Mutalisk, Muta&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-corruptor.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Corruptor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-broodlord.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Brood Lord, Broodlord, Brood&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-ultralisk.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ultralisk, Ultra&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-unit-zerg-viper.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Viper&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-ability-zerg-burrow-color.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Burrow&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-meleeattacks-level1.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Melee Attacks Level 1, Melee Attacks 1, +1 melee, +1 attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-meleeattacks-level2.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Melee Attacks Level 2, Melee Attacks 2, +2 melee, +2 attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-meleeattacks-level3.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Melee Attacks Level 3, Melee Attacks 3, +3 melee, +3 attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-missileattacks-level1.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Missile Attacks Level 1, Missile Attacks 1, +1 missile, +1 ranged&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-missileattacks-level2.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Missile Attacks Level 2, Missile Attacks 2, +2 missile, +2 ranged&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-missileattacks-level3.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Missile Attacks Level 3, Missile Attacks 3, +3 missile, +3 ranged&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-groundcarapace-level1.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ground Carapace Level 1, Ground Carapace 1, +1 carapace, +1 armor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-groundcarapace-level2.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ground Carapace Level 2, Ground Carapace 2, +2 carapace, +2 armor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-groundcarapace-level3.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Ground Carapace Level 3, Ground Carapace 3, +3 carapace, +3 armor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-metabolicboost.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Metabolic Boost, Ling Speed, Speed&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-glialreconstitution.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Glial Reconstitution, Roach Speed&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-groovedspines.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Grooved Spines, Hydralisk Range, Hydra Range&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-evolvemuscularaugments.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Mscular Augments, Hydralisk Speed, Hydra Speed&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-centrifugalhooks.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Centrifugal Hooks, Baneling Speed, Bane Speed&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-pneumatizedcarapace.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Pneumatized Carapace, Overlord Speed, OL Speed&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-adrenalglands.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Adrenal Glands, Cracklings&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-airattacks-level1.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Attacks Level 1, Flyer Attacks 1, +1 flyer attack, +1 air attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-airattacks-level2.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Attacks Level 2, Flyer Attacks 2, +2 flyer attack, +2 air attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-airattacks-level3.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Attacks Level 3, Flyer Attacks 3, +3 flyer attack, +3 air attack&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-flyercarapace-level1.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Carapace Level 1, Flyer Carapace 1, +1 flyer Carapace, +1 air armor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-flyercarapace-level2.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Carapace Level 2, Flyer Carapace 2, +2 flyer Carapace, +2 air armor&lt;/li&gt;&lt;li&gt;&lt;IMG path=&quot;Assets\Textures\btn-upgrade-zerg-flyercarapace-level3.dds&quot; height=&quot;32&quot; width=&quot;32&quot; alignment=&quot;absolutemiddle&quot; /&gt;: Flyer Carapace Level 3, Flyer Carapace 3, +3 flyer Carapace, +3 air armor&lt;/li&gt;&lt;n/&gt;&lt;/ul&gt;"/>
        </Frame>

        <Frame type="Label" name="label35" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label34" pos="Max" offset="10"/>
            <Text val="Matching Rules"/>
        </Frame>

        <Frame type="Label" name="label36" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label35" pos="Max" offset="10"/>
            <Text val="Generally, a rule fires if"/>
        </Frame>

        <Frame type="Label" name="label37" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label36" pos="Max" offset="0"/>
            <Text val="&lt;ul indent=&quot;2&quot; text=&quot;•&quot;&gt;&lt;li&gt;All required resources and technology are available,&lt;/li&gt;&lt;li&gt;the requirements are met and&lt;/li&gt;&lt;li&gt;applying it does not delay application of a previous rule.&lt;/li&gt;&lt;n/&gt;&lt;/ul&gt;"/>
        </Frame>

        <Frame type="Label" name="label38" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label37" pos="Max" offset="10"/>
            <Text val="Note that we do not say that rules need to be applied in the order written - the last point allows some flexibility in that regard. For example:"/>
        </Frame>

        <Frame type="Label" name="label39" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label38" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;MonospaceTemplate&quot;&gt;  30 - Roach Warren, 10 roaches&lt;n/&gt;  22 - Mass Zergling&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="label40" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label39" pos="Max" offset="10"/>
            <Text val="These rules will produce zerglings until 30 supply is hit, at which point the roach warren and roaches take priority. This rule matching makes it easier to naturally mix different kinds of actions in a build order."/>
        </Frame>

        <Frame type="Label" name="label41" template="HeroHelpPanel/ControlTitleTemplate">
            <Anchor side="Top" relative="$parent/label40" pos="Max" offset="10"/>
            <Text val="Writing Build Orders"/>
        </Frame>

        <Frame type="Label" name="label42" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label41" pos="Max" offset="10"/>
            <Text val="Build order simulation is hard - probably even NP-hard with reasonable extensions. This mod attemps to solve it anyway using an &quot;intelligent&quot; heuristic. These quotation marks are important. Do not be surprised when (not if!) reasonable-looking build orders will not quite execute as planned."/>
        </Frame>

        <Frame type="Label" name="label43" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label42" pos="Max" offset="10"/>
            <Text val="Piece of advice: When in doubt, Use specific requirements instead of giving the simulation room to make &quot;intelligent&quot; choices."/>
        </Frame>

        <Frame type="Label" name="label44" template="HeroHelpPanel/CategoryTitleTemplate">
            <Anchor side="Top" relative="$parent/label43" pos="Max" offset="10"/>
            <Text val="FAQ"/>
        </Frame>

        <Frame type="Label" name="label45" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label44" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;ModLeftSize16Bold&quot;&gt;This mod is dumb. Play real StarCraft!&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="label46" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label45" pos="Max" offset="10"/>
            <Text val="Yes and yes. Just like hitting a punching bag is dump, and should never be confused with an actual fight. But still, sooner or later, being able to hit hard (timings) makes your life easier."/>
        </Frame>

        <Frame type="Label" name="label47" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label46" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;ModLeftSize16Bold&quot;&gt;Why can&apos;t I save build orders? Why aren&apos;t control group layouts remembered?&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="label48" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label47" pos="Max" offset="10"/>
            <Text val="Funny story, actually. If you happen to have surplus pitchforks lying around, head to &lt;a name=&quot;WebsiteUrl&quot; href=&quot;http://us.battle.net/forums/en/sc2/topic/11884478517&quot;&gt;the appropriate battle.net thread&lt;/a&gt;. If that link does not work, do a web search for &quot;extension mod bank&quot;."/>
        </Frame>

        <Frame type="Label" name="label49" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label48" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;ModLeftSize16Bold&quot;&gt;I found a bug! Let me tell you how stupid you are!&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="label50" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label49" pos="Max" offset="10"/>
            <Text val="Impotent rage is the highest form of flattery. I will probably set up a GitHub issue tracker, use your favourite search engine to track it down."/>
        </Frame>

        <Frame type="Label" name="label51" template="HeroHelpPanel/CategoryTextTemplate">
            <Anchor side="Top" relative="$parent/label50" pos="Max" offset="10"/>
            <Text val="&lt;s val=&quot;MonospaceTemplate&quot;&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;n/&gt;&lt;/s&gt;"/>
        </Frame>

        <Frame type="Label" name="ItemLabel">
            <Anchor side="Top" relative="$parent/label51" pos="Max" offset="40"/>
            <Anchor side="Left" relative="$parent" pos="Min" offset="10"/>
            <Anchor side="Right" relative="$parent" pos="Max" offset="-10"/>
            <Height val="10"/>
        </Frame>
    </Frame>

    <Frame type="ListBox" name="HeroHelpListBoxTemplate">
        <ScrollBar val="ScrollBar"/>
        <ItemDesc val="HeroHelpPanel/HeroListItem"/>
        <DescFlags val="Internal"/>
        <AcceptsMouse val="false"/>

        <Frame type="ScrollBar" name="ScrollBar" template="StandardTemplates/StandardScrollBarTemplate">
        </Frame>
    </Frame>
</Desc>
