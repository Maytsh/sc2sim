
// This is just a simple program to strip empty lines. Shame
// on Windows batch for not being able to perform this task
// in a reliable fashion.

#include <stdio.h>
#include <string.h>
#include <cctype>

#pragma warning (disable: 4996)

int main(int argc, const char *argv[]) {

	if (argc < 2) { fprintf(stderr, "Usage: strip [in] [out]\n"); return 1; }

	FILE *in = fopen(argv[1], "r");
	FILE *out = fopen(argv[2], "w");
	if (!in || !out) { fprintf(stderr, "Could not open files!\n"); return 1; }

	fputs("// This is a generated file! Editing it would be plain stupid!\n", out);
	fputs("// Unless you want a minor case of serious brain damage, I suggest\n", out);
	fputs("// you look around for the original files. They might make a lot more\n", out);
	fputs("// sense *and* have enlightening comments!\n\n", out);

	const int BUF_SIZE = 2048 * 16;
	char buf[BUF_SIZE];
	while(!feof(in) && fgets(buf, BUF_SIZE, in)) {
		// Check
		if (strlen(buf) + 1 >= BUF_SIZE) {
			fclose(in); fclose(out);
			fprintf(stderr, "Line too long!\n");
			return 2;
		}
		// Check 2
		char *p = buf;
		while (isspace(*p) || *p == ';') p++;
		if (*p == '#') continue;
		if (*p) {
			if (strlen(buf) < 80) {
				fputs(buf, out);
			} else {
				// Make an effort to beautify long lines
				int nest = 0, nest2 = 0;
				char *start = buf;
				p = buf;
				for (p = buf; *p; p++) {
					putc(*p,out);
					if (*p == '(') nest++;
					if (*p == '(') nest--;
					if (*p == '{') nest2++;
					if (*p == '}') nest2--;
					if ((*p == ';' || *p == '{' || *p == '}') && nest == 0 && p - start > 80) {
						putc('\n', out);
						// Auto-indent. Still somewhat funky, but at this point
						// optimizing further would probably be *really* silly :D
						for (int i = 0; i < nest2; i++) {
							putc(' ', out); putc(' ', out);
						}
					}
				}
			}
		}
	}
	fclose(in);
	fclose(out);
	return 0;
}
