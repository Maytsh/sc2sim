
module Main where

import Text.Pandoc
import Text.Pandoc.Definition
import Text.Pandoc.Error (handleError)

import Control.Monad.State

import Data.String.Utils
import Data.List

-- Padding amounts
categoryPadding = 30
headingPadding = 20
paragraphPadding = 10

-- | Writer for Starcraft 2 UI definition format
uiWriter :: Pandoc -> String
uiWriter pand = evalState (uiWritePandoc pand) 0

-- | UI Writer state. We need to carry an integer state around in order
-- to enumerate labels.
type UIWrite = State Int

uiLabelName :: Int -> String
uiLabelName i = "label" ++ show i

-- | Make a label for the given (UI-formatted) text.
uiInsertLabel :: String -> Int -> String -> UIWrite String
uiInsertLabel template padding text = do

  -- Generate ID
  i <- get
  put (i+1)
  
  -- Make name and determine our anchor (either parent or last label)
  let name = uiLabelName i
      relative | i > 0     = "$parent/" ++ uiLabelName (i-1)
               | otherwise = "$parent"
      pos | i > 0 = "Max"
          | otherwise = "Min"
  
  -- Escape text
  let escaped = replace "%" "&#37;" $
                replace "•" "&#8226;" $
                replace "<" "&lt;" $
                replace ">" "&gt;" $
                replace "\"" "&quot;" $
                replace "&" "&amp;" $
                text

  return $ intercalate "\n" [ 
    "<Frame type=\"Label\" name=\""++name++"\" template=\""++template++"\">",
    "  <Anchor side=\"Top\" relative=\""++relative++"\" pos=\""++pos++"\" offset=\""++show padding++"\"/>",
    "  <Text val=\""++escaped++"\"/>",
    "</Frame>"
    ]

-- | Generate style tag
style :: String -> String -> String
style s str = concat ["<s val=\"", s, "\">", str, "</s>"]
 
-- | Convert the given inline text into a format string
uiInline :: Inline -> String
uiInline (Str str)                 = str -- TODO: escape?
uiInline (Emph inls)               = style "ModLeftSize16Bold" (concatMap uiInline inls)
uiInline (Strong inls)             = style "ModLeftSize16Bold" (concatMap uiInline inls)
uiInline (Strikeout inls)          = concatMap uiInline inls
uiInline (Superscript inls)        = concatMap uiInline inls
uiInline (Subscript inls)          = style "ModLeftSize12" (concatMap uiInline inls)
uiInline (SmallCaps inls)          = style "ModLeftSize12" (concatMap uiInline inls)
uiInline (Quoted SingleQuote inls) = concat ["'", concatMap uiInline inls, "'"]
uiInline (Quoted DoubleQuote inls) = concat ["\"", concatMap uiInline inls, "\""]
uiInline (Cite _citations inls)    = concatMap uiInline inls
uiInline (Code _attr str)          = style "MonospaceTemplate" str
uiInline Space                     = " "
uiInline SoftBreak                 = " "
uiInline LineBreak                 = "<n/>"
uiInline (Math _type str)          = str
uiInline (RawInline _fmt str)      = str
uiInline (Link _attr inls (target, _))  = concat [ "<a name=\"WebsiteUrl\" href=\"", target, "\">", concatMap uiInline inls, "</a>" ]
uiInline (Image _attr _inls (target, _))= concat [ "<IMG path=\"", target, "\" height=\"32\" width=\"32\" alignment=\"absolutemiddle\" />" ]
uiInline other                     = error "unsupported uiInline: " ++ show other
 
uiWritePandoc :: Pandoc -> UIWrite String
uiWritePandoc (Pandoc _meta blocks) =
  intercalate "\n\n" <$> mapM uiWriteBlock blocks
 
uiWriteBlockInline :: Block -> String
uiWriteBlockInline (Plain inls) = concatMap uiInline inls
uiWriteBlockInline (Para inls)  = concatMap uiInline inls
uiWriteBlockInline (CodeBlock attr str) = style "MonospaceTemplate" $ replace "\n" "<n/>" str
uiWriteBlockInline other        = error "unsupported uiWriteBlockInline: " ++ show other

uiWriteList :: String -> [[Block]] -> UIWrite String
uiWriteList bullet blocks =
  let blocks_fmt = map (intercalate "<n/><n/>" . map uiWriteBlockInline) blocks
      to_list_entry text = "<li>" ++ text ++ "</li>"
  in uiInsertLabel "HeroHelpPanel/CategoryTextTemplate" 0 $
     concat [ "<ul indent=\"2\" text=\"" ++ bullet ++ "\">"
            , concatMap to_list_entry blocks_fmt ++ "<n/>"
            , "</ul>" ]

uiWriteBlock :: Block -> UIWrite String
uiWriteBlock (Plain inls)          = uiInsertLabel "HeroHelpPanel/CategoryTextTemplate" 0 (concatMap uiInline inls)
uiWriteBlock (Para inls)           = uiInsertLabel "HeroHelpPanel/CategoryTextTemplate" paragraphPadding (concatMap uiInline inls)
uiWriteBlock (Header 1 _attr inls) = uiInsertLabel "HeroHelpPanel/CategoryTitleTemplate" paragraphPadding (concatMap uiInline inls)
uiWriteBlock (Header n _attr inls) = uiInsertLabel "HeroHelpPanel/ControlTitleTemplate" paragraphPadding (replicate (n*2-4)' ' ++ concatMap uiInline inls)
uiWriteBlock (CodeBlock attr str)  = uiInsertLabel "HeroHelpPanel/CategoryTextTemplate" paragraphPadding $ style "MonospaceTemplate" $ replace "\n" "<n/>" str
uiWriteBlock (BulletList blocks)   = uiWriteList "•" blocks
uiWriteBlock (OrderedList _a blocks) = uiWriteList "%number%. " blocks
uiWriteBlock other                 = error $ "unsupported uiWriteBlock: " ++ show other

markdownToUI :: String -> String
markdownToUI = uiWriter .  handleError . readMarkdown def

addIndent :: Int -> String -> String
addIndent i = unlines . map add . lines
  where add ""   = ""
        add line = indent ++ line
        indent = replicate i ' '

main :: IO ()
main = readFile "help.md" >>= writeFile "help.SC2Layout" . addIndent (length "        ") . markdownToUI
    

