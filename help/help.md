
This mod is for having fun with macro - develop your economy and try to reach a set goal as fast as possible. Build StarCraft "muscle memory" so you can focus more on other parts of the game!

Saving & Loading
----------------

This mod saves automatically every few seconds, allowing you to jump back. For example `-l 0` will start again from the beginning, and `-l -60` will move back one minute. You can soft-pause the game with `-p`.

This uses a modified version of the SALT mod by Turtles. Read on below for more information. 

Build Order Simulation (Zerg only)
----------------------------------

To evaluate and guide your play, Macro Hero can simulate how a build order would play out in an ideal world. This means you can test build order ideas in an isolated space, but also use it as a blueprint for your own play.

All you need is a build as text, which Macro Hero parses and simulates. The heuristic will execute the build order realistically, for example automatically building overlords or drones and saving up resources when it makes sense.

Build Order Guide (Zerg only)
-----------------------------

This mod tracks your current game state and can predict what your next actions should be. The interface is inspired by Guitar Hero[tm]: The actions to be performed will be listed in the order that you have to perform them, spaced according to the approximate time you have left.

Furthermore, this mode will monitor your performance by running the build order simulation to completion every few seconds. The projected finish time will be shown so you always know how well you are doing.

Command Reference
=================

Outside of this shiny user interface, this mod is controlled using chat commands. Here is a list:

 * `-g`: Start guided play
 * `-h`: Shows a helpful wall of text
 * `-l[time]`: Load the save closest to the given time. So `-l0` starts over, or `-l5:00` takes you to the state 5 minutes in.
 * `-l[+-][offset]`: If you add `+` or `-` in front, the time is relative to the current time. `-l-60` takes you back one minute, and `-l+60` jumps forward if possible.
 * `-l[name]`: Loads a named save
 * `-o`: Removes all UI elements
 * `-p`: Pause or resume the game
 * `-s[name]`: Makes a named save. In contrast to auto-saves, these will not get deleted.
 * `-w[seconds]`: Time window to show. Use shorter windows to increase difficulty.

Saving & Loading
================

Game loading in Macro Hero is primarily based on automatic saving: The game will create a snapshot every few seconds, which you can load.

Storage capacity is limited, so auto saves will be incrementally thinned out as we run into said limits. After jumping backwards, this mechanism will start cleaning up all game states you jumped past, so "future" snapshots degrade even more quickly.

If you want to ensure that some save does not get discarded, name it using `-s [name]`. Note that the start state never gets discarded, so `-l 0` is always guaranteed to work.

Restrictions
------------

Saving and loading the game sounds like a trivial matter, but is surprisingly tricky to get right. The engine is rather... selective in what it shares with mods. Furtunately though, we stand on the shoulders of Turtles, who figured out workarounds for most of these problems.

Honorable mention goes to *control groups*: Ironically, mods can set them, but not read them. This is why we might ask you to cycle through your control groups at the start: We need to figure out what keys change control groups so we can "infer" what their current state is.

Apart from that, there are still a few things that don't update correctly when loading - for example camera hotkeys or rallies to objects in the fog of war. Apologies for the inconvenience.

Build Order Simulation
======================

Format
------

The build order format should look familiar:

```
    13 - Overlord
    17 - Hatchery
    19 - Hatchery
    18 - Spawning Pool
    23 - 2 Queen, 2 Zergling
    30 - 1 Queen
    2:50 - 3 Extractor
    3:15 - Evolution Chamber, Roach Warren
    @100 gas - Lair
    @100 gas - Missile Attacks Level 1
    @100 gas - Metabolic Boost
    @100 gas - Roach Speed
    67 - Mass Roach, Mass Zergling
```

Every line defines one or multiple *rules*, each with *requirements* and an *action*.

Requirements
------------

There are three types of requirements:


 * `[mm]:[ss]` - Rule gets activated once time is reached.
 * `[supply]` - Rule is active once supply is reached.
    If a previous rule frees supply, the supply limit is increased until that rule gets applied.
    So for example `13 - Extractor | 12 - Spawning Pool` allows for the pool to be built at 13
    supply if the extractor isn't started yet.

 * `@[gas] gas` or `@[gas]g` - Rule is active once gas has been earned.
    Does not count gas reserved for previous rules. So
	`16 - Metabolic Boost, @50gas - Baneling Nest` means that the
	baneling nest rule will only fire if a total of 150 gas have been mined.

You can combine them if you want.

Actions
-------

There are a bunch of different actions we can simulate:

 * `Build [n] [unit]` or `Morph [n] [unit]` or just `[n] [unit]` - makes this number of units.
 * `[unit] to [n]` -- make unit until we reach a total count. For example `Drone to 80`.
 * `Mass [unit]` -- make unit until supply limit is reached.
 * `Research [tech]` or just `[tech]` - starts the given research.
 * `[n] mineral worker` or `[n] mineral drone` - move said number of workers to minerals. For robustness, these rules should often have an `@[gas] g` requirement.
 * `[n] gas worker` or `[n] gas drone` - undo mineral workers. Note that extractors are automatically filled, so this is only makes sense after "mineral worker".

Note that we often use unit / research names, which can be hard to remember. In fact, we support a few more or less common abbreviations. Here is a handy list:
 
 * ![Hatchery](Assets\Textures\btn-building-zerg-hatchery.dds): Hatchery, Hatch, Expand
 * ![Lair](Assets\Textures\btn-building-zerg-lair.dds): Lair
 * ![Hive](Assets\Textures\btn-building-zerg-hive.dds): Hive
 * ![Macro Hatchery](Assets\Textures\btn-building-zerg-hatchery.dds): Macro Hatchery, Macro Hatch
 * ![Extractor](Assets\Textures\btn-building-zerg-extractor.dds): Extractor
 * ![Spawning Pool](Assets\Textures\btn-building-zerg-spawningpool.dds): Spawning Pool, Pool
 * ![Evolution Chamber](Assets\Textures\btn-building-zerg-evolutionchamber.dds): Evolution Chamber, Evo Chamber, Evo
 * ![Baneling Nest](Assets\Textures\btn-building-zerg-banelingnest.dds): Baneling Nest, Bane Nest
 * ![Roach Warren](Assets\Textures\btn-building-zerg-roachwarren.dds): Roach Warren, Warren
 * ![Hydralisk Den](Assets\Textures\btn-building-zerg-hydraliskden.dds): Hydralisk Den, Hydra Den
 * ![Lurker Den](Assets\Textures\btn-building-zerg-lurkerden.dds): Lurker Den
 * ![Infestation Pit](Assets\Textures\btn-building-zerg-infestationpit.dds): Infestation Pit
 * ![Spire](Assets\Textures\btn-building-zerg-spire.dds): Spire
 * ![Greater Spire](Assets\Textures\btn-building-zerg-greaterspire.dds): Greater Spire
 * ![Ultralisk Cavern](Assets\Textures\btn-building-zerg-ultraliskcavern.dds): Ultralisk Cavern, Ultra Cavern
 * ![Overlord](Assets\Textures\btn-unit-zerg-overlord.dds): Overlord, OL
 * ![Overlord Transport](Assets\Textures\btn-unit-zerg-overlordtransport.dds): Overlord Transport, TLOL
 * ![Overseer](Assets\Textures\btn-unit-zerg-overseer.dds): Overseer
 * ![Drone](Assets\Textures\btn-unit-zerg-drone.dds): Drone
 * ![Queen](Assets\Textures\btn-unit-zerg-queen.dds): Queen
 * ![Zergling](Assets\Textures\btn-unit-zerg-zergling.dds): Zergling, Ling
 * ![Baneling](Assets\Textures\btn-unit-zerg-baneling.dds): Baneling, Bane
 * ![Roach](Assets\Textures\btn-unit-zerg-roach.dds): Roach
 * ![Ravager](Assets\Textures\btn-unit-zerg-ravager.dds): Ravager
 * ![Hydralisk](Assets\Textures\btn-unit-zerg-hydralisk.dds): Hydralisk, Hydra
 * ![Lurker](Assets\Textures\btn-unit-zerg-lurker.dds): Lurker
 * ![Mutalisk](Assets\Textures\btn-unit-zerg-mutalisk.dds): Mutalisk, Muta
 * ![Corruptor](Assets\Textures\btn-unit-zerg-corruptor.dds): Corruptor
 * ![Brood Lord](Assets\Textures\btn-unit-zerg-broodlord.dds): Brood Lord, Broodlord, Brood
 * ![Ultralisk](Assets\Textures\btn-unit-zerg-ultralisk.dds): Ultralisk, Ultra
 * ![Viper](Assets\Textures\btn-unit-zerg-viper.dds): Viper
 * ![Burrow](Assets\Textures\btn-ability-zerg-burrow-color.dds): Burrow
 * ![Melee Attacks Level 1](Assets\Textures\btn-upgrade-zerg-meleeattacks-level1.dds): Melee Attacks Level 1, Melee Attacks 1, +1 melee, +1 attack
 * ![Melee Attacks Level 2](Assets\Textures\btn-upgrade-zerg-meleeattacks-level2.dds): Melee Attacks Level 2, Melee Attacks 2, +2 melee, +2 attack
 * ![Melee Attacks Level 3](Assets\Textures\btn-upgrade-zerg-meleeattacks-level3.dds): Melee Attacks Level 3, Melee Attacks 3, +3 melee, +3 attack
 * ![Missile Attacks Level 1](Assets\Textures\btn-upgrade-zerg-missileattacks-level1.dds): Missile Attacks Level 1, Missile Attacks 1, +1 missile, +1 ranged
 * ![Missile Attacks Level 2](Assets\Textures\btn-upgrade-zerg-missileattacks-level2.dds): Missile Attacks Level 2, Missile Attacks 2, +2 missile, +2 ranged
 * ![Missile Attacks Level 3](Assets\Textures\btn-upgrade-zerg-missileattacks-level3.dds): Missile Attacks Level 3, Missile Attacks 3, +3 missile, +3 ranged
 * ![Ground Carapace Level 1](Assets\Textures\btn-upgrade-zerg-groundcarapace-level1.dds): Ground Carapace Level 1, Ground Carapace 1, +1 carapace, +1 armor
 * ![Ground Carapace Level 2](Assets\Textures\btn-upgrade-zerg-groundcarapace-level2.dds): Ground Carapace Level 2, Ground Carapace 2, +2 carapace, +2 armor
 * ![Ground Carapace Level 3](Assets\Textures\btn-upgrade-zerg-groundcarapace-level3.dds): Ground Carapace Level 3, Ground Carapace 3, +3 carapace, +3 armor
 * ![Metabolic Boost](Assets\Textures\btn-upgrade-zerg-metabolicboost.dds): Metabolic Boost, Ling Speed, Speed
 * ![Glial Reconstitution](Assets\Textures\btn-upgrade-zerg-glialreconstitution.dds): Glial Reconstitution, Roach Speed
 * ![Grooved Spines](Assets\Textures\btn-upgrade-zerg-groovedspines.dds): Grooved Spines, Hydralisk Range, Hydra Range
 * ![Mscular Augments](Assets\Textures\btn-upgrade-zerg-evolvemuscularaugments.dds): Mscular Augments, Hydralisk Speed, Hydra Speed
 * ![Centrifugal Hooks](Assets\Textures\btn-upgrade-zerg-centrifugalhooks.dds): Centrifugal Hooks, Baneling Speed, Bane Speed
 * ![Pneumatized Carapace](Assets\Textures\btn-upgrade-zerg-pneumatizedcarapace.dds): Pneumatized Carapace, Overlord Speed, OL Speed
 * ![Adrenal Glands](Assets\Textures\btn-upgrade-zerg-adrenalglands.dds): Adrenal Glands, Cracklings
 * ![Flyer Attacks Level 1](Assets\Textures\btn-upgrade-zerg-airattacks-level1.dds): Flyer Attacks Level 1, Flyer Attacks 1, +1 flyer attack, +1 air attack
 * ![Flyer Attacks Level 2](Assets\Textures\btn-upgrade-zerg-airattacks-level2.dds): Flyer Attacks Level 2, Flyer Attacks 2, +2 flyer attack, +2 air attack
 * ![Flyer Attacks Level 3](Assets\Textures\btn-upgrade-zerg-airattacks-level3.dds): Flyer Attacks Level 3, Flyer Attacks 3, +3 flyer attack, +3 air attack
 * ![Flyer Carapace Level 1](Assets\Textures\btn-upgrade-zerg-flyercarapace-level1.dds): Flyer Carapace Level 1, Flyer Carapace 1, +1 flyer Carapace, +1 air armor
 * ![Flyer Carapace Level 2](Assets\Textures\btn-upgrade-zerg-flyercarapace-level2.dds): Flyer Carapace Level 2, Flyer Carapace 2, +2 flyer Carapace, +2 air armor
 * ![Flyer Carapace Level 3](Assets\Textures\btn-upgrade-zerg-flyercarapace-level3.dds): Flyer Carapace Level 3, Flyer Carapace 3, +3 flyer Carapace, +3 air armor

Matching Rules
--------------

Generally, a rule fires if

 * All required resources and technology are available,
 * the requirements are met and
 * applying it does not delay application of a previous rule.

Note that we do not say that rules need to be applied in the order written - the last point allows some flexibility in that regard. For example:

```
  30 - Roach Warren, 10 roaches
  22 - Mass Zergling
```

These rules will produce zerglings until 30 supply is hit, at which point the roach warren and roaches take priority. This rule matching makes it easier to naturally mix different kinds of actions in a build order.

Writing Build Orders
--------------------

Build order simulation is hard - probably even NP-hard with reasonable extensions. This mod attemps to solve it anyway using an "intelligent" heuristic. These quotation marks are important. Do not be surprised when (not if!) reasonable-looking build orders will not quite execute as planned.

Piece of advice: When in doubt, Use specific requirements instead of giving the simulation room to make "intelligent" choices.

FAQ
===

*This mod is dumb. Play real StarCraft!*

Yes and yes. Just like hitting a punching bag is dump, and should never be confused with an actual fight. But still, sooner or later, being able to hit hard (timings) makes your life easier.

*Why can't I save build orders? Why aren't control group layouts remembered?*

Funny story, actually. If you happen to have surplus pitchforks lying around, head to [the appropriate battle.net thread](http://us.battle.net/forums/en/sc2/topic/11884478517). If that link does not work, do a web search for "extension mod bank".

*I found a bug! Let me tell you how stupid you are!*

Impotent rage is the highest form of flattery. I will probably set up a GitHub issue tracker, use your favourite search engine to track it down.


```












```